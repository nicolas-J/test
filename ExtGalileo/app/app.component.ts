import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { Cookie } from "ng2-cookies";

import { AuthService } from './services/auth.service';

@Component({
    selector: 'aw-galileo',
    templateUrl: '/app/app.component.html'
})
export class AppComponent {
    showNav: boolean = true;

    constructor(private authService: AuthService, private router: Router) {
        this.showNav = true;
        let browserLang = Cookie.get('_culture');

    }

    ngOnInit() {
        let routeLink = window.location.pathname;
        //if (this.authService.getToken() == null) {
        //    this.showNav = false;
        //    if (routeLink == "/Account/Register")
        //        this.router.navigate(['/Account/Register']);
        //    else
        //this.router.navigate([routeLink]);
        //}
    }

    loaderOff() {
        $(".loaderBox").on("click", function () {
            $(this).css("display", "none");
        });
    }

}