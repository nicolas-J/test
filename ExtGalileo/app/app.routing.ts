﻿import { Component, ModuleWithProviders, Input } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from './services/auth-guard.service';

//Main
import { AppComponent } from './app.component';
import { NavComponent } from './items/nav.component';
import { HeaderComponent } from './items/header.component';
import { FooterComponent } from './items/footer.component';

//Home 
import { IndexComponent } from './home/index.component';

//Account
import { AccountComponent } from './account/account.component';
import { LoginComponent } from './account/login.component';
import { RegisterComponent } from './account/register.component';
import { ForgotPasswordComponent } from './account/forgotPassword.component';
import { ResetPasswordComponent } from './account/resetPassword.component';

//Dashboard
import { DashboardComponent } from './dashboard/dashboard.component';
import { TarificationComponent } from './dashboard/tarification.component';
import { ProjectsComponent } from './dashboard/projects.component';
import { DocumentsComponent } from './dashboard/documents.component';
import { PortefeuilleComponent } from './dashboard/portefeuille.component';

//Products
import { TestModuleComponent } from './test/testModule.component';
import { SanteIndividuelComponent } from './products/sante-individuel/santeIndividuel.component';
import { ProductRcComponent } from './products/rc-autoentrepreneur/product-rc.component';
import { RcProComponent } from './products/rc-pro/rc-pro.component';
import { PaymentCallbackComponent } from './onlinePayment/paymentCallback.component';
import { OnlinePaymentComponent } from './onlinePayment/onlinePayment.component';
import { PaymentListComponent } from './onlinePayment/payment-list.component';

//Manage
import { CollaboratorDetailComponent } from './manage/collaboratorDetail.component';

const appRoutes: Routes = [
    {
        path: 'Account',
        children: [
            { path: 'Login', component: LoginComponent },
            { path: 'Register', canActivate: [AuthGuardService], component: RegisterComponent },
            { path: 'ForgotPassword', component: ForgotPasswordComponent },
            { path: 'ResetPassword', component: ResetPasswordComponent }
        ]
    },
    {
        path: '', pathMatch: 'full', redirectTo: 'Home/Index'
    },
    {
        path: 'Products',
        children: [
            { path: 'RcPro/:id', component: RcProComponent },
            { path: 'RcPro', component: RcProComponent }
        ]
    },
    {
        path: 'Home',
        children: [
            { path: 'Index', component: IndexComponent },
            { path: 'Account', canActivate: [AuthGuardService], component: AccountComponent },
            { path: 'NosProduits', component: TarificationComponent },
            { path: 'Portefeuille', component: PortefeuilleComponent },
            { path: 'Portefeuille/:id', component: PortefeuilleComponent },
            { path: 'Documents', component: DocumentsComponent },
            { path: 'Projects', canActivate: [AuthGuardService], component: ProjectsComponent },
            { path: 'TestModule', component: TestModuleComponent },
            { path: 'OnlinePayment', component: OnlinePaymentComponent }
        ]
    },
    {
        path: 'Manage',
        canActivate: [AuthGuardService],
        children: [
            { path: 'CollaboratorDetail', component: CollaboratorDetailComponent },
            { path: 'CollaboratorDetail/:id', component: CollaboratorDetailComponent }
        ]
    },
    {
        path: 'Payment',
        canActivate: [AuthGuardService],
        children: [
            { path: 'PaymentCallback', component: PaymentCallbackComponent },
            { path: 'PaymentCallback/:id', component: PaymentCallbackComponent },
            { path: 'PaymentList', canActivate: [AuthGuardService], component: PaymentListComponent }
        ]
    }
];

export const routing = RouterModule.forRoot(appRoutes);
