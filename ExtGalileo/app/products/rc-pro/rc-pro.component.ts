﻿import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { slideToLeft } from '../../tools/router.animations';

import { RcProChubb } from "../../models/rcProChubb";
import { Product } from "../../models/Product";
import { ClientCompany } from "../../models/ClientCompany";

import { SelectItem } from "primeng/primeng";
import { MultiSelectModule } from 'primeng/primeng';
import { SelectButtonModule } from 'primeng/primeng';

import { AppService } from '../../services/app.service';

@Component({
    selector: 'aw-rc-pro',
    templateUrl: '/app/products/rc-pro/rc-pro.component.html',
    animations: [slideToLeft()],
    host: { '[@slideToLeft]': '' }
})

export class RcProComponent {
    model = new RcProChubb();

    activitiesList: SelectItem[] = [];

    allPrimaryActivitiesList: SelectItem[];
    otherPrimary: string = "";

    secondaryActivity: SelectItem[];
    otherSecondary: string = "";

    allSecondaryActivitiesList: SelectItem[];

    paiementList: SelectItem[];

    alertPrimary: string;
    alertSecondary: string;

    cities: string[];

    minDate: Date;
    display: boolean = false;
    isPaymentPopUp: boolean = false;
    stopProd: boolean = false;

    questionMediaList: boolean[] = [];
    questionInfoList: boolean[] = [];
    priceLoad: boolean = false;
    // errors of five first fields before of displaying the price
    errors: boolean[] = [true, true, true, false, true];

    seal: string;
    stringData: string;

    isWaiting: boolean = false;

    constructor(private router: Router, private appService: AppService, private route: ActivatedRoute) {
    }

    ngOnInit() {

        this.paiementList = [];
        this.paiementList.push({ label: 'Prélèvement', value: 'P' });
        this.paiementList.push({ label: 'CB', value: 'CB' });

        this.model.EffectDate = this.minDate = new Date();
        this.model.Product.Family = "IARD";
        this.model.Product.Target = "TNS/TPE/PME/Start-up";
        this.model.Product.Name = "RC Pro";

        this.route.queryParams.subscribe(params => {
            if (params["id"] && params["severity"] && params["summary"] && params["detail"]) {
                this.appService.setEmittedValue({ severity: params["severity"], summary: params["summary"], detail: params["detail"] });
                this.appService.sendString("/Products/GetRcPro", params["id"]).subscribe(response => {
                    this.model = response;

                    //Fix bad returning date format
                    this.model.EffectDate = new Date(parseInt(this.model.EffectDate.toString().substr(6)));
                    this.model.DueDate = new Date(parseInt(this.model.DueDate.toString().substr(6)));

                    //Restore price
                    if (this.model.Base.Price && this.model.DueDate) {
                        this.errors = [true, false, false, false, false];
                        this.displayPrice();
                    }

                    //Restore list of activities
                    this.listFamilyActivity(this.model.PrimaryActivity, false);
                    if (this.model.SecondaryActivities)
                        if (this.model.SecondaryActivities.length > 0)
                            this.getSecondaryList(this.model.SecondaryActivities, false);

                    //Restore client city
                    if (this.model.Client.Address.City)
                        this.getCities(false);

                    //Download signed pdf
                    if (this.model.PdfSignUrl)
                        window.location.assign(this.model.PdfSignUrl);
                });
            }
        });

        this.route.params.subscribe(params => {
            if (params.id) {
                this.appService.sendString("/Products/GetRcPro", params.id).subscribe(response => {
                    this.model = response;

                    //Fix bad returning date format
                    this.model.EffectDate = new Date(parseInt(this.model.EffectDate.toString().substr(6)));
                    this.model.DueDate = new Date(parseInt(this.model.DueDate.toString().substr(6)));

                    //Restore list of activities
                    this.listFamilyActivity(this.model.PrimaryActivity, false);
                    if (this.model.SecondaryActivities)
                        if (this.model.SecondaryActivities.length > 0)
                            this.getSecondaryList(this.model.SecondaryActivities, false);

                    //Restore client city
                    if (this.model.Client.Address.City)
                        this.getCities(false);

                    //Restore price
                    if (this.model.Base.Price && this.model.DueDate) {
                        this.errors = [false, false, false, false, false];
                        this.displayPrice();
                    }
                });
            }
        });
    }

    onSubmit() {
        this.appService.sendModel("/Payment/GetSeal", this.model).subscribe(callback => {
            this.seal = callback.Seal;
            this.stringData = callback.StringData;
            this.isPaymentPopUp = true;
        });
    }

    listFamilyActivity(event: any, onChange: boolean = true) {

        this.allPrimaryActivitiesList = [];
        this.secondaryActivity = [];
        this.allPrimaryActivitiesList = [];
        this.allSecondaryActivitiesList = [];

        if (!this.model.PrimaryActivitiesList || onChange)
            this.model.PrimaryActivitiesList = [];

        if (!this.model.SecondaryActivities || onChange)
            this.model.SecondaryActivities = [];

        if (!this.model.SecondaryActivitiesList || onChange)
            this.model.SecondaryActivitiesList = [];
        this.activitiesList = [];
        this.alertPrimary = "";

        this.model.OptionAp = false;
        this.model.OptionMd = false;
        if (event) {
            this.errors[0] = false;
        } else {
            this.errors[0] = true;
        }
        this.displayPrice();

        if (event == "Conseil") {
            this.allPrimaryActivitiesList = this.getConseilList();

            this.alertPrimary = "IL EST PRECISE QUE : TOUTE ACTIVITE DE FORMATION EN ATELIER, USINE, INDUSTRIE, CHANTIER, TOUTE ACTIVITE D’ENSEIGNEMENT DE LA CONDUITE (AUTO - ECOLE), TOUTE ACTIVITE DE CONSEIL EN INVESTISSMENTS FINANCIERS (CIF), D’INTERMEDIATION EN OPERATIONS DE BANQUE (IOB), DE CONSEIL EN GESTION DE PATRIMOINE (CGP), ET D’INTERMEDIATION EN ASSURANCE, SONT EXPRESSEMENT EXCLUES.";

            this.secondaryActivity.push({ label: "Aucune", value: "" });
            this.secondaryActivity.push({ label: "Marketing & Media", value: "Marketing & Media" });
            this.secondaryActivity.push({ label: "Informatique", value: "Informatique" });
        }
        else if (event == "Marketing & Media") {

            this.allPrimaryActivitiesList = this.getMarketMediaList();

            this.alertPrimary = "DISPOSITIONS COMMUNES A TOUTES LES ACTIVITES : IL EST PRECISE QUE TOUTE EDITION DE CHAINES, DE REVUES OU PERIODIQUES, DE SITES INTERNET, DE JEUX VIDEO OU TOUTE AUTRE ACTIVITE, RESERVEE AUX ADULTES ET/ OU EROTIQUES ET/ OU PORNOGRAPHIQUES SONT EXPRESSEMENT EXCLUS";

            this.secondaryActivity.push({ label: "Aucune", value: "" });
            this.secondaryActivity.push({ label: "Conseil", value: "Conseil" });
            this.secondaryActivity.push({ label: "Informatique", value: "Informatique" });

        }
        else if (event == "Informatique") {
            this.allPrimaryActivitiesList = this.getItList();

            this.alertPrimary = "";

            this.secondaryActivity.push({ label: "Aucune", value: "" });
            this.secondaryActivity.push({ label: "Conseil/Marketing & Media", value: "Conseil/Marketing & Media" });
        }

        if (this.model.PrimaryActivitiesList.length == 0) {

            for (let activity of this.allPrimaryActivitiesList) {
                this.model.PrimaryActivitiesList.push(activity.value);
            }
        }
    }

    getSecondaryList(event: any, onChange: boolean = true) {
        this.allSecondaryActivitiesList = [];
        if (!this.model.SecondaryActivitiesList || onChange)
            this.model.SecondaryActivitiesList = [];
        this.activitiesList = [];
        if (event.length == 0)
            this.listFamilyActivity(this.model.PrimaryActivity);

        if (event.indexOf('') >= 0) {

            this.secondaryActivity = [{ label: "Aucune", value: "" }];
            this.model.SecondaryActivities = [""];
        }
        else {
            if (event.indexOf('Conseil') >= 0)
                this.allSecondaryActivitiesList = this.getConseilList();
            if (event.indexOf('Marketing & Media') >= 0)
                this.allSecondaryActivitiesList = this.getMarketMediaList();
            if (event.indexOf('Informatique') >= 0)
                this.allSecondaryActivitiesList = this.getItList();
            if (event.indexOf('Conseil/Marketing & Media') >= 0)
                this.allSecondaryActivitiesList = this.getConMarkMediList();

            if (this.model.SecondaryActivitiesList.length == 0) {

                for (let activity of this.allSecondaryActivitiesList) {

                    this.model.SecondaryActivitiesList.push(activity.value);
                }
            }
        }
    }

    getConseilList() {
        this.activitiesList.push({ label: "Cabinet / conseil en Recrutement", value: "Cabinet / conseil en Recrutement" });
        this.activitiesList.push({ label: "Centre d'appels", value: "Centre d'appels" });
        this.activitiesList.push({ label: "Conseil en Crédit Impôt Recherche (exclusion des IOB)", value: "Conseil en Crédit Impôt Recherche (exclusion des IOB)" });
        this.activitiesList.push({ label: "Conseil en Développement personnel / coaching", value: "Conseil en Développement personnel / coaching" });
        this.activitiesList.push({ label: "Conseil en Marketing", value: "Conseil en Marketing" });
        this.activitiesList.push({ label: "Conseil en Réduction de Coûts / costs Killer", value: "Conseil en Réduction de Coûts / costs Killer" });
        this.activitiesList.push({ label: "Conseil en stratégie", value: "Conseil en stratégie" });
        this.activitiesList.push({ label: "Consultant en Management", value: "Consultant en Management" });
        this.activitiesList.push({ label: "Consultant en Organisation et Développement", value: "Consultant en Organisation et Développement" });
        this.activitiesList.push({ label: "Consultant en Ressources Humaines", value: "Consultant en Ressources Humaines" });
        this.activitiesList.push({ label: "Formation*", value: "Formation*" });
        this.activitiesList.push({ label: "Gestion pour compte de tiers de la comptabilité hors activité d'expertise comptable", value: "Gestion pour compte de tiers de la comptabilité hors activité d'expertise comptable" });
        this.activitiesList.push({ label: "Gestion pour compte de tiers de tâches administratives", value: "Gestion pour compte de tiers de tâches administratives" });
        this.activitiesList.push({ label: "Gestion pour le compte de tiers de paie et gestion administrative du personnel", value: "Gestion pour le compte de tiers de paie et gestion administrative du personnel" });
        this.activitiesList.push({ label: "Recouvrement de créances", value: "Recouvrement de créances" });
        this.activitiesList.push({ label: "Traduction et Interprétation", value: "Traduction et Interprétation" });

        return this.activitiesList;
    }

    getMarketMediaList() {
        this.activitiesList.push({ label: "Animation réseaux sociaux pour le compte de tiers", value: "Animation réseaux sociaux pour le compte de tiers" });
        this.activitiesList.push({ label: "Banques de données images", value: "Banques de données images" });
        this.activitiesList.push({ label: "Centre d'appels", value: "Centre d'appels" });
        this.activitiesList.push({ label: "Conception / réalisation d'études de marché, de sondages", value: "Conception / réalisation d'études de marché, de sondages" });
        this.activitiesList.push({ label: "Conception de spots et de films publicitaires et bandes annonces (autres que TV)", value: "Conception de spots et de films publicitaires et bandes annonces (autres que TV)" });
        this.activitiesList.push({ label: "Conseil en communication", value: "Conseil en communication" });
        this.activitiesList.push({ label: "Conseil en marketing", value: "Conseil en marketing" });
        this.activitiesList.push({ label: "Conseil en publicité", value: "Conseil en publicité" });
        this.activitiesList.push({ label: "Création de campagnes publicitaires", value: "Création de campagnes publicitaires" });
        this.activitiesList.push({ label: "Création de sites internet", value: "Création de sites internet" });
        this.activitiesList.push({ label: "Edition d'enregistrements sonores", value: "Edition d'enregistrements sonores" });
        this.activitiesList.push({ label: "Édition de chaînes généralistes", value: "Édition de chaînes généralistes" });
        this.activitiesList.push({ label: "Édition de chaînes thématiques", value: "Édition de chaînes thématiques" });
        this.activitiesList.push({ label: "Édition de journaux", value: "Édition de journaux" });
        this.activitiesList.push({ label: "Édition de livres", value: "Édition de livres" });
        this.activitiesList.push({ label: "Édition de répertoires et de fichiers d'adresses", value: "Édition de répertoires et de fichiers d'adresses" });
        this.activitiesList.push({ label: "Édition de revues et périodiques", value: "Édition de revues et périodiques" });
        this.activitiesList.push({ label: "Édition et diffusion de programmes radio", value: "Édition et diffusion de programmes radio" });
        this.activitiesList.push({ label: "Fabrication de tout support publicitaire", value: "Fabrication de tout support publicitaire" });
        this.activitiesList.push({ label: "Gestion de relations publiques", value: "Gestion de relations publiques" });
        this.activitiesList.push({ label: "Marketing Direct", value: "Marketing Direct" });
        this.activitiesList.push({ label: "Réalisation et conception graphique", value: "Réalisation et conception graphique" });
        this.activitiesList.push({ label: "Reproduction d'enregistrements sonores", value: "Reproduction d'enregistrements sonores" });
        this.activitiesList.push({ label: "Studio et autres activités photographiques", value: "Studio et autres activités photographiques" });

        return this.activitiesList;
    }

    getItList() {
        this.activitiesList.push({ label: "Activité de banque de données", value: "Activité de banque de données" });
        this.activitiesList.push({ label: "Conseil en systèmes et logiciels informatiques", value: "Conseil en systèmes et logiciels informatiques" });
        this.activitiesList.push({ label: "Conseil en systèmes informatiques", value: "Conseil en systèmes informatiques" });
        this.activitiesList.push({ label: "Conseil en télécommunication", value: "Conseil en télécommunication" });
        this.activitiesList.push({ label: "Création de sites internet", value: "Création de sites internet" });
        this.activitiesList.push({ label: "Développement d'applications spécifiques / à façon", value: "Développement d'applications spécifiques / à façon" });
        this.activitiesList.push({ label: "Edition de logiciels (non personnalisés)", value: "Edition de logiciels (non personnalisés)" });
        this.activitiesList.push({ label: "Edition de logiciels outils de développement et de langages", value: "Edition de logiciels outils de développement et de langages" });
        this.activitiesList.push({ label: "Edition de logiciels système et de réseau", value: "Edition de logiciels système et de réseau" });
        this.activitiesList.push({ label: "Edition de progiciels", value: "Edition de progiciels" });
        this.activitiesList.push({ label: "Enregistrement de nom de domaine", value: "Enregistrement de nom de domaine" });
        this.activitiesList.push({ label: "Infogérance", value: "Infogérance" });
        this.activitiesList.push({ label: "Hébergement de données et applications informatiques", value: "Hébergement de données et applications informatiques" });
        this.activitiesList.push({ label: "Distribution et/ou installation de matériels informatiques", value: "Distribution et/ou installation de matériels informatiques" });
        this.activitiesList.push({ label: "Installation, configuration et paramétrage de logiciels", value: "Installation, configuration et paramétrage de logiciels" });
        this.activitiesList.push({ label: "Intégration de logiciels ou applicatifs", value: "Intégration de logiciels ou applicatifs" });
        this.activitiesList.push({ label: "Maintenance de logiciels", value: "Maintenance de logiciels" });
        this.activitiesList.push({ label: "Maintenance de matériel informatique", value: "Maintenance de matériel informatique" });
        this.activitiesList.push({ label: "Opérateur de télécommunication", value: "Opérateur de télécommunication" });
        this.activitiesList.push({ label: "Programmation informatique", value: "Programmation informatique" });
        this.activitiesList.push({ label: "Tierce maintenance de systèmes et d'applications informatiques", value: "Tierce maintenance de systèmes et d'applications informatiques" });
        this.activitiesList.push({ label: "Vente de matériel informatique", value: "Vente de matériel informatique" });
        this.activitiesList.push({ label: "Vente de nom de domaine", value: "Vente de nom de domaine" });

        return this.activitiesList;
    }

    getConMarkMediList() {
        this.activitiesList.push({ label: "Centre d'appels", value: "Centre d'appels" });
        this.activitiesList.push({ label: "Conseil en stratégie", value: "Conseil en stratégie" });
        this.activitiesList.push({ label: "Conseil en management", value: "Conseil en management" });
        this.activitiesList.push({ label: "Conseil en organisation et développement", value: "Conseil en organisation et développement" });
        this.activitiesList.push({ label: "Conseil en ressources humaines", value: "Conseil en ressources humaines" });
        this.activitiesList.push({ label: "Conseil en marketing", value: "Conseil en marketing" });
        this.activitiesList.push({ label: "Formation en informatique et média", value: "Formation en informatique et média" });
        this.activitiesList.push({ label: "Banque de données images", value: "Banque de données images" });
        this.activitiesList.push({ label: "Conception / réalisation d'études de marché, de sondages", value: "Conception / réalisation d'études de marché, de sondages" });
        this.activitiesList.push({ label: "Conception de spots et de films publicitaires et bandes annonces (autres que TV)", value: "Conception de spots et de films publicitaires et bandes annonces (autres que TV)" });
        return this.activitiesList;
    }


    getPrice(ngModel: any = null) {
        let context = this;
        setTimeout(function () {
            if (!context.stopProd) {
                if (ngModel) {
                    if (ngModel.errors === null && context.model.AnnualRevenue && context.model.Base.Level
                        && context.model.AnnualRevenue >= 0 && context.model.AnnualRevenue <= 5000000) {
                        context.errors[1] = false;
                        context.errors[2] = false;
                        context.appService.sendModel('/Calculate/RcPro/', context.model).subscribe(callback => {
                            context.model.Franchise = callback[0];
                            context.model.Base.Price = callback[1];
                            context.displayPrice();
                        });
                    } else {
                        context.errors[1] = true;
                        context.errors[2] = true;
                        context.displayPrice();
                    }
                }
            }
        }, 100);
    }

    sendElement() {
        this.appService.sendModel('/Tools/SendBackOfficeMail/', this.model).subscribe(callback => {
            this.model.Id = callback;
            console.log(callback);
        });
    }

    pay() {
        document.forms["paymentForm"].submit();
    }

    getCities(onPostCodeChange: boolean = true) {
        this.appService.sendString('/Tools/GetCity/', this.model.Client.Address.PostCode).subscribe(callback => {
            if (callback) {
                if (callback.length != 0) {
                    this.cities = callback;
                    if (onPostCodeChange) {
                        this.model.Client.Address.City = callback[0];
                    }
                } else {
                    this.cities = [];
                    this.appService.setEmittedValue({
                        severity: "error",
                        summary: "Erreur",
                        detail: "Aucune ville trouvée"
                    });
                }
            }
        });
    }

    question(event: string) {
        if (event == 'true')
            this.model.OptionAp = true;
        else
            this.model.OptionAp = false;

        this.getPrice();
    }

    optionPlus(event: string) {
        if (event == 'true')
            this.model.OptionMd = true;
        else
            this.model.OptionMd = false;

        this.getPrice();
    }

    downloadPdf() {
        event.preventDefault();
        if (this.model.PdfSignUrl) {
            window.location.assign(this.model.PdfSignUrl);
        } else if (this.model.IsPaidSubscription) {
            window.location.assign(this.model.PdfDevisUrl);
        } else {
            this.isWaiting = true;
            this.appService.sendModel("/Pdf/GetPdf", this.model).subscribe(callback => {
                this.isWaiting = false;
                window.location.href = callback;
            });
        }
    }

    signProject() {
        event.preventDefault();
        this.isWaiting = true;
        this.appService.sendModel("/Pdf/SignInit", this.model).subscribe(callback => {
            this.isWaiting = false;
            window.location.replace(callback);
        });
    }

    bloqueProject(event: any) {
        if (event.length >= 1) {
            this.display = true;
            this.stopProd = true;
            this.displayPrice();
        } else {
            this.stopProd = false;
            this.displayPrice();
        }
    }

    check(ngModel: any) {
        let context = this;
        setTimeout(function () {
            if (ngModel.errors === null) {
                switch (ngModel.name) {
                    case 'EffectDate':
                        context.errors[3] = false;
                        break;
                    case 'DueDate':
                        context.errors[4] = false;
                        break;
                }
            } else {
                switch (ngModel.name) {
                    case 'EffectDate':
                        context.errors[3] = true;
                        break;
                    case 'DueDate':
                        context.errors[4] = true;
                        break;
                }
            }
            context.displayPrice();
        }, 100);
    }

    displayPrice(): void {

        if (this.errors.indexOf(true) == -1 && this.stopProd === false)
            this.priceLoad = true;
        else
            this.priceLoad = false;
    }

    setMinDate(date: string) {
        return new Date(date);
    }
}