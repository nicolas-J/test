﻿import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from "@angular/core";
import { Modal } from "ng2-modal";
import { slideToLeft } from '../../tools/router.animations';

//Models
import { ExtranetSubscription } from "../../models/ExtranetSubscription";

@Component({
    selector: "rc-declaration",
    templateUrl: "/app/products/rc-autoentrepreneur/rc-declaration.component.html",
    animations: [slideToLeft()],
    host: { '[@slideToLeft]': '' }
})

export class RcDeclaration implements OnInit{
    @Input() checkedDeclarationInput: boolean;
    @Output() checkedDeclarationOutput = new EventEmitter();

    checkedDeclaration: boolean;

    @ViewChild('rcModal') rcModal: Modal;

    ngOnInit() {
        this.checkedDeclaration = this.checkedDeclarationInput;
    }

    showDialog() {
        this.rcModal.open();
    }

    checkDeclaration(event: any) {
        this.checkedDeclarationOutput.emit(event.target.checked);
    }
}
