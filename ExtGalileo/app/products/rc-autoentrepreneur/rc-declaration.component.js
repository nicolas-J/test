"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng2_modal_1 = require("ng2-modal");
var router_animations_1 = require("../../tools/router.animations");
var RcDeclaration = (function () {
    function RcDeclaration() {
        this.checkedDeclarationOutput = new core_1.EventEmitter();
    }
    RcDeclaration.prototype.ngOnInit = function () {
        this.checkedDeclaration = this.checkedDeclarationInput;
    };
    RcDeclaration.prototype.showDialog = function () {
        this.rcModal.open();
    };
    RcDeclaration.prototype.checkDeclaration = function (event) {
        this.checkedDeclarationOutput.emit(event.target.checked);
    };
    return RcDeclaration;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], RcDeclaration.prototype, "checkedDeclarationInput", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], RcDeclaration.prototype, "checkedDeclarationOutput", void 0);
__decorate([
    core_1.ViewChild('rcModal'),
    __metadata("design:type", ng2_modal_1.Modal)
], RcDeclaration.prototype, "rcModal", void 0);
RcDeclaration = __decorate([
    core_1.Component({
        selector: "rc-declaration",
        templateUrl: "/app/products/rc-autoentrepreneur/rc-declaration.component.html",
        animations: [router_animations_1.slideToLeft()],
        host: { '[@slideToLeft]': '' }
    })
], RcDeclaration);
exports.RcDeclaration = RcDeclaration;
//# sourceMappingURL=rc-declaration.component.js.map