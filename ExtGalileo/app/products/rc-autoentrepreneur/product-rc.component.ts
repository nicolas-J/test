﻿import { Component, OnInit, ViewChild, OnDestroy } from "@angular/core";
import { Router, ActivatedRoute, Params } from "@angular/router";
import { NgForm } from "@angular/forms";
import { Headers, Http, Response } from "@angular/http";
import { slideToLeft } from '../../tools/router.animations';
import { Subscription } from "rxjs/Subscription";

import "rxjs/add/operator/toPromise";

//Models
import { ExtranetSubscription } from "../../models/ExtranetSubscription";

//PrimeNG
import { SelectItem } from "primeng/primeng";
import { Message } from 'primeng/primeng';

//Service
import { ProductRcService } from "../../services/productService/product-rc.service";
import { AuthGuardService } from '../../services/auth-guard.service';
import { MasksService } from '../../services/masks.service';
import { ProductsService } from '../../services/productService/products.service';

import { RcDeclaration } from '../../products/rc-autoentrepreneur/rc-declaration.component';


@Component({
    selector: "family-activity",
    templateUrl: "/app/products/rc-autoentrepreneur/product-rc.component.html",
    animations: [slideToLeft()],
    host: { '[@slideToLeft]': '' }
})

export class ProductRcComponent implements OnInit, OnDestroy {

    @ViewChild('rcForm') rcForm: NgForm;

    @ViewChild("rcModal") modal: RcDeclaration;

    ///Variables
    FamilyActivity: SelectItem[];

    LegalStatutList: SelectItem[];

    FamilyActivityValue: any;

    model: any;

    ActivityList: any;

    display = false;

    msgs: Message[] = [];

    rolesSubscription: Subscription;

    roles: Array<string>;

    startDate: Date;

    endDate: Date;

    commissionList: SelectItem[];

    today: Date = new Date();

    ///Functions
    constructor(private router: Router,
        private productRcService: ProductRcService,
        private http: Http,
        private route: ActivatedRoute,
        private authGuardService: AuthGuardService,
        private maskService: MasksService,
        private productsService: ProductsService) {
        this.endDate = new Date(2017, 11, 31);
        this.startDate = new Date();
        this.startDate.setHours(0, 0, 0, 0);
    }

    ngOnInit() {
        this.route.params.forEach((params: Params) => {
            let id = params['id'];
            if (id) {
                this.productsService.getProductModel(id)
                    .then(data => {
                        this.model = data;
                        this.model.EffectDate = new Date(this.model.EffectDate.toString());
                        this.model.AxaServiceProModel.RecoveryOrCreationDate = new Date(this.model.AxaServiceProModel.RecoveryOrCreationDate);
                        this.calculateRcPrice();
                    });
            }
        });

        this.rolesSubscription = this.authGuardService.roles.subscribe(data => this.roles = data);

        this.FamilyActivity = [];
        this.FamilyActivity.push({ label: "---", value: null });
        this.FamilyActivity.push({ label: "Education et formation", value: "EducationAndFormation" });
        this.FamilyActivity.push({ label: "Sociétés de conseil", value: "AdviceSociety" });
        this.FamilyActivity.push({ label: "Métiers de l'informatique", value: "ItTrade" });
        this.FamilyActivity.push({ label: "Services aux entreprises", value: "ServiceToSociety" });
        this.FamilyActivity.push({ label: "Marketing et communication", value: "MarketingAndCommunication" });
        this.FamilyActivity.push({ label: "Métiers du biens être", value: "SerenityTrade" });
        this.FamilyActivity.push({ label: "Service à la personne", value: "PeopleCare" });

        this.LegalStatutList = [];
        this.LegalStatutList.push({ label: "Personne morale", value: "PersonneMorale" });
        this.LegalStatutList.push({ label: "Personne physique", value: "PersonnePhysique" });

        this.commissionList = [];
        this.commissionList.push({ label: "0", value: 0 });
        this.commissionList.push({ label: "1", value: 1 });
        this.commissionList.push({ label: "2", value: 2 });
        this.commissionList.push({ label: "3", value: 3 });
        this.commissionList.push({ label: "4", value: 4 });
        this.commissionList.push({ label: "5", value: 5 });
        this.commissionList.push({ label: "6", value: 6 });
        this.commissionList.push({ label: "7", value: 7 });
        this.commissionList.push({ label: "8", value: 8 });
        this.commissionList.push({ label: "9", value: 9 });
        this.commissionList.push({ label: "10", value: 10 });
        this.commissionList.push({ label: "11", value: 11 });
        this.commissionList.push({ label: "12", value: 12 });
        this.commissionList.push({ label: "13", value: 13 });
        this.commissionList.push({ label: "14", value: 14 });
        this.commissionList.push({ label: "15", value: 15 });
        this.commissionList.push({ label: "16", value: 16 });
        this.commissionList.push({ label: "17", value: 17 });
        this.commissionList.push({ label: "18", value: 18 });
        this.commissionList.push({ label: "19", value: 19 });
        this.commissionList.push({ label: "20", value: 20 });
        this.commissionList.push({ label: "21", value: 21 });
        this.commissionList.push({ label: "22", value: 22 });
        this.commissionList.push({ label: "23", value: 23 });
        this.commissionList.push({ label: "24", value: 24 });
        this.commissionList.push({ label: "25", value: 25 });
        this.commissionList.push({ label: "26", value: 26 });
        this.commissionList.push({ label: "27", value: 27 });
        this.commissionList.push({ label: "28", value: 28 });
        this.commissionList.push({ label: "29", value: 29 });
        this.commissionList.push({ label: "30", value: 30 });
    }

    pad(s: any) { return (s < 10) ? '0' + s : s; }

    ngOnDestroy() {
        this.rolesSubscription.unsubscribe();
    }

    //DisplayList on HTML
    listFamilyActivity() {
        this.model.AxaServiceProModel.ActivityList = this.productRcService.getFamilyActivity(this.model.AxaServiceProModel.FamilyActivity);
        this.model.AxaServiceProModel.ActivityList = this.transform(this.model.AxaServiceProModel.ActivityList);
    }

    //Handle Object
    transform(value: any): any {
        const keys: any[] = [];
        for (let key in value) {
            if (value.hasOwnProperty(key)) {
                keys.push({ Key: key, Value: value[key] });
            }
        }
        return keys;
    }

    //Display declaration pop up
    showDialog() {
        this.modal.showDialog();
    }

    calculateRcPrice() {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");
        var model = JSON.stringify(this.model);
        this.http.post("/Calculate/AxaAutoEntrepreneurPrice/", model, { headers: headers })
            .toPromise()
            .then(res => console.log(res));
    }

    allocatePrices(response: any) {
        const prices = JSON.parse(response._body);
        if (prices.Error != null) {
            this.showMessage("error", "Attention!", prices.Error);
            this.killPrices();
        } else {
            this.model.AxaServiceProModel.AnnualRate = prices.Total;
            this.model.AxaServiceProModel.SixMonthRate = (prices.Total / 2).toFixed(2);
            this.model.Base.Price = (prices.Total / 12).toFixed(2);
            this.model.AxaServiceProModel.EstimatedCommission = prices.CommBroker;
            this.clearMessages();
        }
    }

    showMessage(severity: any, summary: any, message: any) {
        this.msgs.push({ severity: severity, summary: summary, detail: message });
    }

    clearMessages() {
        this.msgs = [];
    }

    killPrices() {
        this.model.AxaServiceProModel.AnnualRate = 0;
        this.model.AxaServiceProModel.SixMonthRate = 0;
        this.model.Base.Price = 0;
        this.model.AxaServiceProModel.EstimatedCommission = 0;
    }

    updatePayment(event: any) {
        this.model.PaymentMethod = event.PaymentMethod;
        this.model.Client.BankAccountData.Iban = event.Client.BankAccountData.Iban;
        this.model.Client.BankAccountData.Bic = event.Client.BankAccountData.Bic;
    }
}