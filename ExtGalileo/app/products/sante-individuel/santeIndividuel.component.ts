﻿import { Component, OnInit, ViewChild } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import { Router, ActivatedRoute } from '@angular/router';
import { NgForm } from "@angular/forms";
import { Title } from '@angular/platform-browser';

import { slideToLeft } from '../../tools/router.animations';
import { SelectItem } from 'primeng/primeng';

import { AppService } from '../../services/app.service';
import { MasksService } from '../../services/masks.service';
import { AuthGuardService } from '../../services/auth-guard.service';

import { Subscription } from "rxjs/Subscription";

import { ExtranetSubscription } from "../../models/ExtranetSubscription";
import { Product } from "../../models/Product";
import { ClientPerson } from "../../models/ClientPerson";

@Component({
    selector: 'account',
    templateUrl: '/app/products/sante-individuel/santeIndividuel.component.html',
    providers: [AppService],
    animations: [slideToLeft()],
    host: { '[@slideToLeft]': '' }
})

export class SanteIndividuelComponent {

    products: SelectItem[];
    levels: SelectItem[];
    selectedProduct: string;

    rolesSubscription: Subscription;
    model: ExtranetSubscription = new ExtranetSubscription();
    response: any;
    roles: Array<string>;
    startDate: Date;
    endDate: Date;
    santePrice: any = 0;
    subBanner = "Produit";
    banner = "Santé Individuel";

    @ViewChild('productForm') productForm: NgForm;

    constructor(private router: Router,
        private http: Http,
        private appService: AppService,
        private route: ActivatedRoute,
        private titleService: Title,
        private authGuardService: AuthGuardService,
        private maskService: MasksService) {
    }

    ngOnInit() {
        this.titleService.setTitle("Luca | Produit Santé Individuel");
        this.rolesSubscription = this.authGuardService.roles.subscribe(data => this.roles = data);
        this.endDate = new Date(2017, 11, 31);
        this.startDate = new Date();
        this.startDate.setHours(0, 0, 0, 0);
        this.model.EffectDate = new Date();
        this.model.Commission = "10";

        this.levels = [];

        this.products = [];
        this.products.push({ label: '5820b0c150e47218800559f4', value: 'SPVIE-SANTE-Part-Resp' });
        this.products.push({ label: '56bb4cd458c2a0bd945924d0', value: 'SPVIE-SANTE-TNS-Resp' });
        this.products.push({ label: '5625050a9e8b3a38c810174b', value: 'SPVIE-SANTE-VITALITE-Resp' });
        this.products.push({ label: '5804f663a01bb7393ca35c4f', value: 'SPVIE-SANTE-SURCO-NON-RESP' });

        //this.appService.sendModel('/ProductsSante/GetIndividualHealthProduct/', this.model).subscribe(callback => {
        //    this.products = callback;
        //    console.log(callback);
        //});
    }

    submitForm() {
        //this.appService.sendModel('/CalculateSante/IndividualHealth/', this.model).subscribe(callback => {
        //    this.response = callback;
        //});
    }

    getChildValidation(event: any) {
        for (var key in event.control) {
            this.productForm.form.addControl(key, event.control[key]);
        }
    }

    calculatePrice() {
        console.log(this.productForm.form.valid);
        if (!this.productForm.form.valid) {
            this.santePrice = 0;
            if (this.model.Base.Level == null)
                this.appService.showMessage("Erreur", "Veuillez choisir un niveau de garantie");
            //this.appService.showMessage("Erreur", "Veuillez remplir les champs obligatoire pour avoir un tarif");
        }
        else {
            this.appService.sendModel('/CalculateSante/IndividualHealth/', this.model).subscribe(callback => {
                console.log(callback);
                console.log(typeof callback);
                if (typeof callback === "string") {
                    this.santePrice = 0;
                    if (callback.match("Erreur"))
                        this.appService.showMessage("Erreur", callback);
                    else
                        this.appService.showMessage("Valid", callback);
                }
                else
                    this.santePrice = callback;

                console.log(callback);
            });
        }
    }

    getLevelProduct(productId: string) {
        this.model.Base.Level == null;
        this.appService.sendDatas('/Products/GetLevels/', productId).subscribe(callback => {
            this.levels = [];
            for (let levels of callback) {
                this.levels.push({ label: levels.Name, value: levels.Name });
            }
            if (this.model.Base.Level != null)
                this.calculatePrice();
        });
    }

}