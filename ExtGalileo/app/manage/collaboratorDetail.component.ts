﻿import { Component, OnInit, Input, ViewChild, ElementRef } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params, Routes, RouterModule } from '@angular/router';

import { AppService } from '../services/app.service';
import { MasksService } from '../services/masks.service';
import { AuthGuardService } from '../services/auth-guard.service';
import { DialogModule, FileUploadModule, DataTableModule, SharedModule } from 'primeng/primeng';

@Component({
    selector: 'account',
    templateUrl: '/app/manage/collaboratorDetail.component.html'
})
export class CollaboratorDetailComponent {
    loadingPage: boolean = false;
    response:any;

    constructor(private router: Router, private http: Http, private route: ActivatedRoute, public appService: AppService, public mask: MasksService, private authGuardService: AuthGuardService, private titleService: Title) {
    }

    ngOnInit() {
        this.route.params.subscribe((params: Params) => {
            let Email = params['id'];
            this.detailCoBroker(Email);
        });
    }

    detailCoBroker(coBroker: string) {
        this.appService.sendDatas('/Manage/GetCollaboratorsDetails/', coBroker).subscribe(callBack => {
            this.response = callBack;
            console.log(callBack);
            this.loadingPage = true;
        });
    }
}