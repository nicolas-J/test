"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require("@angular/router");
var app_service_1 = require("../services/app.service");
var masks_service_1 = require("../services/masks.service");
var auth_guard_service_1 = require("../services/auth-guard.service");
var CollaboratorDetailComponent = (function () {
    function CollaboratorDetailComponent(router, http, route, appService, mask, authGuardService, titleService) {
        this.router = router;
        this.http = http;
        this.route = route;
        this.appService = appService;
        this.mask = mask;
        this.authGuardService = authGuardService;
        this.titleService = titleService;
        this.loadingPage = false;
    }
    CollaboratorDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.subscribe(function (params) {
            var Email = params['id'];
            _this.detailCoBroker(Email);
        });
    };
    CollaboratorDetailComponent.prototype.detailCoBroker = function (coBroker) {
        var _this = this;
        this.appService.sendDatas('/Manage/GetCollaboratorsDetails/', coBroker).subscribe(function (callBack) {
            _this.response = callBack;
            console.log(callBack);
            _this.loadingPage = true;
        });
    };
    return CollaboratorDetailComponent;
}());
CollaboratorDetailComponent = __decorate([
    core_1.Component({
        selector: 'account',
        templateUrl: '/app/manage/collaboratorDetail.component.html'
    }),
    __metadata("design:paramtypes", [router_1.Router, http_1.Http, router_1.ActivatedRoute, app_service_1.AppService, masks_service_1.MasksService, auth_guard_service_1.AuthGuardService, platform_browser_1.Title])
], CollaboratorDetailComponent);
exports.CollaboratorDetailComponent = CollaboratorDetailComponent;
//# sourceMappingURL=collaboratorDetail.component.js.map