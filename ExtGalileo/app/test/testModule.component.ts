﻿import { Component, OnInit } from '@angular/core';
import { Http, Response } from "@angular/http";

import { slideToLeft } from '../tools/router.animations';
import { AppService } from '../services/app.service';

import { Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { Product } from "../models/Product";

@Component({
    selector: 'santePart',
    templateUrl: '/app/test/testModule.component.html',
    providers: [AppService],
    animations: [slideToLeft()],
    host: { '[@slideToLeft]': '' }
})

export class TestModuleComponent {
    subBanner: "Dev";
    banner: "Test Module";
    products: Product[] = [];
    status: string;

    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<Product> = new Subject();

    constructor(private http: Http, public appService: AppService) {
        this.status = 'loading';
    }

    ngOnInit() {
        this.dtOptions = {
            pagingType: "full_numbers",
            autoWidth: true,
            language: {
                search : "Rechercher&nbsp;:",
                lengthMenu : "Afficher _MENU_ &eacute;l&eacute;ments",
                info : "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                infoEmpty : "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                infoFiltered : "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                infoPostFix : "",
                loadingRecords : "Chargement en cours...",
                zeroRecords : "Aucun &eacute;l&eacute;ment &agrave; afficher",
                emptyTable : "Aucune donn&eacute;e disponible dans le tableau",
                paginate : {
                    first: "Premier",
                    previous : "Pr&eacute;c&eacute;dent",
                    next : "Suivant",
                    last: "Dernier"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre d&eacute;croissant"
                }
            }
        };

        let del = new Product();
        del.Target = "24/03/2017";
        del.Family = "Dupont";
        del.OriginalName = "RC PRO";
        del.BrokerCommission = 945.47;

        let del2 = new Product();
        del.Target = "18/04/2017";
        del.Family = "Merlin";
        del.OriginalName = "RC PRO";
        del.BrokerCommission = 458.47;

        this.status = 'active';

        this.products = [del, del2];

        this.dtTrigger.next();

        //this.appService.getDatas('/Projects/AllProjects/').subscribe(callback => {
        //    this.status = 'active';
        //    console.log(callback);
        //    this.products = callback;
        //    this.dtTrigger.next();
        //});
    }
}