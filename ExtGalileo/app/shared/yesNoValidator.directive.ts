﻿import { Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Validator, AbstractControl, ValidatorFn, NG_VALIDATORS, Validators } from '@angular/forms';

@Directive({
    selector: '[yesNo]',
    providers: [{ provide: NG_VALIDATORS, useExisting: YesNoValidatorDirective, multi: true }]
})
export class YesNoValidatorDirective implements Validator {
    @Input() yesNo: boolean;

    validate(control: AbstractControl): { [key: string]: any } {
        return this.yesNo ? yesNoValidator(this.yesNo)(control)
            : null;
    }
}

export function yesNoValidator(yesNo: boolean): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        const error = control.value === yesNo;
        return error ? { 'yesNo': true } : null;
    };
}