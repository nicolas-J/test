﻿import { Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Validator, AbstractControl, ValidatorFn, NG_VALIDATORS, Validators } from '@angular/forms';

@Directive({
    selector: '[maxRevenue]',
    providers: [{ provide: NG_VALIDATORS, useExisting: MaxRevenueValidatorDirective, multi: true }]
})
export class MaxRevenueValidatorDirective implements Validator {
    @Input() maxRevenue: number;

    validate(control: AbstractControl): { [key: string]: any } {
        return this.maxRevenue ? maxRevenueValidator(this.maxRevenue)(control)
            : null;
    }
}

export function maxRevenueValidator(maxRevenue: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        const forbidden = control.value > maxRevenue;
        return forbidden ? { 'maxRevenue': true } : null;
    };
}