"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var MaxRevenueValidatorDirective = MaxRevenueValidatorDirective_1 = (function () {
    function MaxRevenueValidatorDirective() {
    }
    MaxRevenueValidatorDirective.prototype.validate = function (control) {
        return this.maxRevenue ? maxRevenueValidator(this.maxRevenue)(control)
            : null;
    };
    return MaxRevenueValidatorDirective;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], MaxRevenueValidatorDirective.prototype, "maxRevenue", void 0);
MaxRevenueValidatorDirective = MaxRevenueValidatorDirective_1 = __decorate([
    core_1.Directive({
        selector: '[maxRevenue]',
        providers: [{ provide: forms_1.NG_VALIDATORS, useExisting: MaxRevenueValidatorDirective_1, multi: true }]
    })
], MaxRevenueValidatorDirective);
exports.MaxRevenueValidatorDirective = MaxRevenueValidatorDirective;
function maxRevenueValidator(maxRevenue) {
    return function (control) {
        var forbidden = control.value > maxRevenue;
        return forbidden ? { 'maxRevenue': true } : null;
    };
}
exports.maxRevenueValidator = maxRevenueValidator;
var MaxRevenueValidatorDirective_1;
//# sourceMappingURL=maxRevenueValidator.directive.js.map