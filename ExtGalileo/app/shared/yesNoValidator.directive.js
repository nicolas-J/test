"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var YesNoValidatorDirective = YesNoValidatorDirective_1 = (function () {
    function YesNoValidatorDirective() {
    }
    YesNoValidatorDirective.prototype.validate = function (control) {
        return this.yesNo ? yesNoValidator(this.yesNo)(control)
            : null;
    };
    return YesNoValidatorDirective;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Boolean)
], YesNoValidatorDirective.prototype, "yesNo", void 0);
YesNoValidatorDirective = YesNoValidatorDirective_1 = __decorate([
    core_1.Directive({
        selector: '[yesNo]',
        providers: [{ provide: forms_1.NG_VALIDATORS, useExisting: YesNoValidatorDirective_1, multi: true }]
    })
], YesNoValidatorDirective);
exports.YesNoValidatorDirective = YesNoValidatorDirective;
function yesNoValidator(yesNo) {
    return function (control) {
        var error = control.value === yesNo;
        return error ? { 'yesNo': true } : null;
    };
}
exports.yesNoValidator = yesNoValidator;
var YesNoValidatorDirective_1;
//# sourceMappingURL=yesNoValidator.directive.js.map