﻿import { Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Validator, AbstractControl, ValidatorFn, NG_VALIDATORS, Validators } from '@angular/forms';

@Directive({
    selector: '[postCode]',
    providers: [{ provide: NG_VALIDATORS, useExisting: PostCodeValidatorDirective, multi: true }]
})
export class PostCodeValidatorDirective implements Validator {

    validate(control: AbstractControl): { [key: string]: any } {
        return  postCodeValidator()(control);
    }
}

export function postCodeValidator(): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        const rgx = new RegExp("^[0-9]{5}$");
        const forbidden = rgx.test(control.value);
        return forbidden ? null : { 'postCode': true };
    };
}