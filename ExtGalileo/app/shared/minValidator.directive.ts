﻿import { Directive, ElementRef, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Validator, AbstractControl, ValidatorFn, NG_VALIDATORS, Validators } from '@angular/forms';

@Directive({
    selector: '[minDate]',
    providers: [{ provide: NG_VALIDATORS, useExisting: MinValidatorDirective, multi: true }]
})
export class MinValidatorDirective implements Validator {
    @Input() minDate: Date;

    validate(control: AbstractControl): { [key: string]: any } {
        return this.minDate ? minDateValidator(this.minDate)(control)
            : null;
    }
}

export function minDateValidator(minDate: Date): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } => {
        let date = new Date(control.value);
        date.setHours(0, 0, 0);
        minDate.setHours(0, 0, 0);
        minDate.setMilliseconds(0);
        const forbidden = date < minDate;
        return forbidden ? { 'minDate': true } : null;
    };
}