"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var MinValidatorDirective = MinValidatorDirective_1 = (function () {
    function MinValidatorDirective() {
    }
    MinValidatorDirective.prototype.validate = function (control) {
        return this.minDate ? minDateValidator(this.minDate)(control)
            : null;
    };
    return MinValidatorDirective;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Date)
], MinValidatorDirective.prototype, "minDate", void 0);
MinValidatorDirective = MinValidatorDirective_1 = __decorate([
    core_1.Directive({
        selector: '[minDate]',
        providers: [{ provide: forms_1.NG_VALIDATORS, useExisting: MinValidatorDirective_1, multi: true }]
    })
], MinValidatorDirective);
exports.MinValidatorDirective = MinValidatorDirective;
function minDateValidator(minDate) {
    return function (control) {
        var date = new Date(control.value);
        date.setHours(0, 0, 0);
        minDate.setHours(0, 0, 0);
        minDate.setMilliseconds(0);
        var forbidden = date < minDate;
        return forbidden ? { 'minDate': true } : null;
    };
}
exports.minDateValidator = minDateValidator;
var MinValidatorDirective_1;
//# sourceMappingURL=minValidator.directive.js.map