"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var common_1 = require("@angular/common");
var router_animations_1 = require("../tools/router.animations");
var auth_service_1 = require("../services/auth.service");
var app_service_1 = require("../services/app.service");
var AccountViewModel_1 = require("../models/AccountViewModel");
var RegisterComponent = (function () {
    function RegisterComponent(router, authService, appService, _location) {
        this.router = router;
        this.authService = authService;
        this.appService = appService;
        this._location = _location;
        this.model = new AccountViewModel_1.RegisterViewModel();
    }
    RegisterComponent.prototype.ngOnInit = function () {
        //this.authService.logout();
    };
    RegisterComponent.prototype.register = function () {
        var _this = this;
        this.appService.sendModel("Account/Register", this.model)
            .subscribe(function (result) {
            if (result.severity == "success") {
                _this.router.navigate(['/Home/Account']);
            }
            _this.appService.setEmittedValue(result);
        });
    };
    RegisterComponent.prototype.cancel = function () {
        event.preventDefault();
        this._location.back();
    };
    return RegisterComponent;
}());
RegisterComponent = __decorate([
    core_1.Component({
        selector: 'register',
        templateUrl: '/app/account/register.component.html',
        animations: [router_animations_1.slideToBottom()],
        host: { '[@slideToBottom]': '' }
    }),
    __metadata("design:paramtypes", [router_1.Router, auth_service_1.AuthService, app_service_1.AppService, common_1.Location])
], RegisterComponent);
exports.RegisterComponent = RegisterComponent;
//# sourceMappingURL=register.component.js.map