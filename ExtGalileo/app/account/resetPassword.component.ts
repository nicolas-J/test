﻿﻿import { Component, OnInit } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { slideToBottom } from '../tools/router.animations';

import { AuthService } from '../services/auth.service';
import { AppService } from '../services/app.service';
import { ResetPasswordViewModel } from "../models/AccountViewModel";

@Component({
    selector: 'login-reset',
    templateUrl: '/app/account/resetPassword.component.html',
    animations: [slideToBottom()],
    host: { '[@slideToBottom]': '' }
})

export class ResetPasswordComponent {
    response: any;
    model: ResetPasswordViewModel = new ResetPasswordViewModel();
    showSocial: true;

    constructor(private router: Router, private authService: AuthService, private appService: AppService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        this.authService.logout();
    }

    initPassword() {
        let code = this.route.snapshot.queryParams["confirmationCode"];
        this.model.Code = code;
        this.appService.sendModel("/Account/ResetPassword", this.model).subscribe(callback => {
            if (callback[0] == "success") {
                this.router.navigate(['/Account/Login']);
            }
            this.appService.setEmittedValue(callback);
        });
    }
}