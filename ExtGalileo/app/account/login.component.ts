﻿import { Component, OnInit } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import { Router, ActivatedRoute } from '@angular/router';
import { slideToBottom } from '../tools/router.animations';

import { AuthService } from '../services/auth.service';
import { AppService } from '../services/app.service';
import { LoginViewModel } from "../models/AccountViewModel";

@Component({
    selector: 'login',
    templateUrl: '/app/account/login.component.html',
    animations: [slideToBottom()],
    host: { '[@slideToBottom]': '' }
})

export class LoginComponent {
    response: any;
    model: LoginViewModel = new LoginViewModel();
    showSocial: true;

    constructor(private router: Router, private authService: AuthService, private appService: AppService, private route: ActivatedRoute) {
    }

    ngOnInit() {
        

        this.route.queryParams.subscribe(params => {
            if(params["userId"] && params["confirmationCode"])
                this.appService.sendData("/Account/ConfirmEmail", { userId: params['userId'], confirmationCode: params['confirmationCode'] })
                    .subscribe(result => {
                        this.appService.setEmittedValue(result);
                    }
                );
        });
    }

    login() {
        this.authService.login(this.model)
            .subscribe(result => {
                if (result.severity == "success") {
                    this.router.navigate(['/Home/Projects']);
                }
                this.appService.setEmittedValue(result);
            });
    }
}