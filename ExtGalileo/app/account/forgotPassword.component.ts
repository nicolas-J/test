﻿﻿import { Component, OnInit } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import { Router, ActivatedRoute } from '@angular/router';
import { slideToBottom } from '../tools/router.animations';

import { AuthService } from '../services/auth.service';
import { AppService } from '../services/app.service';
import { ForgotPasswordViewModel } from "../models/AccountViewModel";

@Component({
    selector: 'login-forgot',
    templateUrl: '/app/account/forgotPassword.component.html',
    animations: [slideToBottom()],
    host: { '[@slideToBottom]': '' }
})

export class ForgotPasswordComponent {
    response: any;
    model: ForgotPasswordViewModel = new ForgotPasswordViewModel();
    showSocial: true;

    constructor(private router: Router, private authService: AuthService, private appService: AppService) {
    }

    ngOnInit() {
        this.authService.logout();
    }

    forgotPassword() {
        this.appService.sendModel("/Account/ForgotPassword", this.model).subscribe(callback => {
            this.appService.setEmittedValue(callback);
        });
    }
}