﻿import { Component, OnInit } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { slideToBottom } from '../tools/router.animations';
import { PasswordModule } from 'primeng/primeng';

import { AuthService } from '../services/auth.service';
import { AppService } from '../services/app.service';
import { RegisterViewModel } from "../models/AccountViewModel";

@Component({
    selector: 'register',
    templateUrl: '/app/account/register.component.html',
    animations: [slideToBottom()],
    host: { '[@slideToBottom]': '' }
})

export class RegisterComponent {
    response: any;
    model: RegisterViewModel = new RegisterViewModel();

    constructor(private router: Router, private authService: AuthService, private appService: AppService, private _location: Location) {
    }

    ngOnInit() {
        //this.authService.logout();
    }

    register() {
        this.appService.sendModel("Account/Register", this.model)
            .subscribe(result => {
                if (result.severity == "success") {
                    this.router.navigate(['/Home/Account']);
                }
                this.appService.setEmittedValue(result);
            });
    }

    cancel() {
        event.preventDefault();
        this._location.back();
    }
}