﻿import { Component, OnInit } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import { Title } from '@angular/platform-browser';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Routes, RouterModule } from '@angular/router';
import { slideToLeft } from '../tools/router.animations';

import { AppService } from '../services/app.service';
import { AuthGuardService } from '../services/auth-guard.service';

@Component({
    selector: 'account',
    templateUrl: '/app/account/account.component.html',
    animations: [slideToLeft()],
    host: { '[@slideToLeft]': ''}
})

export class AccountComponent {

    constructor(private router: Router, private http: Http, private route: ActivatedRoute, public appService: AppService, private authGuardService: AuthGuardService, private titleService: Title) {
    }

    ngOnInit() {
    }

}

