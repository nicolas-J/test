"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_service_1 = require("../services/app.service");
var LoginSurveyComponent = (function () {
    function LoginSurveyComponent() {
        this.displayOutput = new core_1.EventEmitter();
        this.displaySurvey = false;
    }
    LoginSurveyComponent.prototype.ngOnInit = function () {
        this.displaySurvey = true;
    };
    LoginSurveyComponent.prototype.submitForm = function (event) {
        console.log(event);
        this.displaySurvey = false;
    };
    return LoginSurveyComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], LoginSurveyComponent.prototype, "displayOutput", void 0);
LoginSurveyComponent = __decorate([
    core_1.Component({
        selector: 'login-survey',
        templateUrl: '/app/popup/loginSurvey.component.html',
        providers: [app_service_1.AppService]
    })
], LoginSurveyComponent);
exports.LoginSurveyComponent = LoginSurveyComponent;
//# sourceMappingURL=loginSurvey.component.js.map