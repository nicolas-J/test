﻿import { Component, OnInit, Input, Output, EventEmitter, ViewChild, Injectable } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Routes, RouterModule } from '@angular/router';

import { AuthGuardService } from '../services/auth-guard.service';
import { AppService } from '../services/app.service';

@Component({
    selector: 'login-survey',
    templateUrl: '/app/popup/loginSurvey.component.html',
    providers: [AppService]
})
export class LoginSurveyComponent {
    @Output() displayOutput = new EventEmitter();
    displaySurvey = false;

    ngOnInit() {
        this.displaySurvey = true;
    }

    submitForm(event: any) {
        console.log(event);
        this.displaySurvey = false;
    }
}