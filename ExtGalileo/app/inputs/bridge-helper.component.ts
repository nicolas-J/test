﻿import { Component, OnInit, Input, isDevMode  } from "@angular/core";
import { Headers, Http, Response } from '@angular/http';
import "rxjs/add/operator/toPromise";

//Models
import { ExtranetSubscription } from "../models/ExtranetSubscription";

//Prime ng
import { Message } from 'primeng/primeng';

@Component({
    selector: "bridge-helper",
    templateUrl: "/app/inputs/bridge-helper.component.html"
})

export class BridgeHelper implements OnInit {

    constructor(private http: Http) { console.log(isDevMode());}

    //Variables 
    @Input() documentsInfo: ExtranetSubscription;

    msgs: Message[] = [];

    model: any;

    id: string;

    ngOnInit() {

        this.model = this.documentsInfo;
    }

    bridgeProject() {
        console.log(this.model.Id);
        $(".loader").show();
        let sign = "Ordre66";
        window.location.href = "Sign/SignCallBack/" + this.model.Id + "?sign=" + sign;

    }

    showBridge(response: any) {
        const message = JSON.stringify(response._body).replace(/"/g, "").replace(/\\/g, "");
        if (message.match("Erreur")) {
            this.showMessage("error", "Attention!", message.replace("u0027", "'"));
        } else {
            window.location.href = (message);
        }
        this.clearMessages();

        $(".loader").hide();
    }
    showMessage(severity: any, summary: any, message: any) {
        this.msgs.push({ severity: severity, summary: summary, detail: message });
    }

    clearMessages() {
        this.msgs = [];
    }

}