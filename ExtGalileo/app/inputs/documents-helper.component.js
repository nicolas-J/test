"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var ng2_modal_1 = require("ng2-modal");
var DocumentsHelper = (function () {
    function DocumentsHelper(http) {
        this.http = http;
        this.msgs = [];
        this.modelChange = new core_1.EventEmitter();
    }
    DocumentsHelper.prototype.ngOnInit = function () {
    };
    DocumentsHelper.prototype.downloadPdf = function () {
        var _this = this;
        $(".loader").show();
        this.saveProject();
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post("/Pdf/InitPdfDownload", JSON.stringify(this.model), { headers: headers })
            .toPromise()
            .then(function (res) { return _this.showDownload(res); });
    };
    DocumentsHelper.prototype.showDownload = function (response) {
        var pdfs = JSON.stringify(response._body).replace(/"/g, "").replace(/\\/g, "");
        if (pdfs.match("Erreur")) {
            this.showMessage("error", "Attention!", pdfs.replace("u0027", "'"));
        }
        else if (pdfs == "") {
            this.showMessage("error", "Attention!", "Pdf indisponibles pour le moment, veuillez réessayer plus tard, merci.");
        }
        else {
            var link = document.createElement("a");
            link.setAttribute("download", pdfs);
            link.setAttribute("href", pdfs);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
            this.clearMessages();
        }
        $(".loader").hide();
    };
    DocumentsHelper.prototype.saveProject = function () {
        var _this = this;
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post("/Subscription/Save", this.model, { headers: headers })
            .toPromise()
            .then(function (res) { return _this.showResult(res); });
    };
    DocumentsHelper.prototype.showResult = function (response) {
        var message = JSON.parse(response._body);
        if (message.Error != null) {
            this.showMessage("error", "Attention!", message);
        }
        else {
            this.showMessage("success", "Merci!", message);
        }
    };
    DocumentsHelper.prototype.showSignModal = function () {
        this.signModal.open();
    };
    DocumentsHelper.prototype.updateDocument = function () {
    };
    DocumentsHelper.prototype.signProject = function () {
        var _this = this;
        $(".loader").show();
        this.saveProject();
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post("/Sign/SignInit", JSON.stringify(this.model), { headers: headers })
            .toPromise()
            .then(function (res) { return _this.showResultSign(res); });
    };
    DocumentsHelper.prototype.showResultSign = function (response) {
        var message = JSON.stringify(response._body).replace(/"/g, "").replace(/\\/g, "");
        if (message.match("Erreur")) {
            this.showMessage("error", "Attention!", message.replace("u0027", "'"));
            this.signModal.close();
        }
        else {
            window.location.href = (message);
        }
        $(".loader").hide();
    };
    DocumentsHelper.prototype.bridgeProject = function () {
        var _this = this;
        $(".loader").show();
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post("/Sign/ManualBridge", JSON.stringify(this.model), { headers: headers })
            .toPromise()
            .then(function (res) { return _this.showBridgeResult(res); });
    };
    DocumentsHelper.prototype.showBridgeResult = function (response) {
        var message = JSON.stringify(response._body).replace(/"/g, "").replace(/\\/g, "");
        if (message == "valide") {
            this.model.IsBridged = true;
            var sign = "Ordre66";
            window.location.href = "Sign/SignCallBack/" + this.model.Id + "?sign=" + sign;
        }
        else {
            this.showMessage("error", "Attention!", message.replace("u0027", "'"));
            $(".loader").hide();
        }
    };
    DocumentsHelper.prototype.getPdfSign = function () {
        if (this.model.PdfSignUrl)
            window.location.href = (this.model.PdfSignUrl);
    };
    DocumentsHelper.prototype.showMessage = function (severity, summary, message) {
        this.msgs.push({ severity: severity, summary: summary, detail: message });
    };
    DocumentsHelper.prototype.clearMessages = function () {
        this.msgs = [];
    };
    return DocumentsHelper;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], DocumentsHelper.prototype, "modelChange", void 0);
__decorate([
    core_1.ViewChild('signModal'),
    __metadata("design:type", ng2_modal_1.Modal)
], DocumentsHelper.prototype, "signModal", void 0);
DocumentsHelper = __decorate([
    core_1.Component({
        selector: "documents-helper",
        templateUrl: "/app/inputs/documents-helper.component.html",
        inputs: ['model', 'inputList', 'roles', 'disableElement']
    })
    //INPUT VARIABLES
    //PDF :
    //Bia --> Bulletin individuel d'adhésion
    //Bae --> Bulletin d'adhésion d'entreprise
    //Sepa --> Mandat Sepa
    //Devis --> Devis
    //Notice --> Notice Informations
    //BeneficiaryDesignation --> Clause de désignation de bénéficiaire
    //Plaquette --> Plaquette
    //Action :
    //Download --> Télécharger les pdfs
    //Sign --> Signature électronique
    //Save --> Sauvegarder dans la base de données
    //Bridge --> Bridge Extranet vers Assurware manuel
    //roles --> Liste des rôles attribués à l'utilisateur
    ,
    __metadata("design:paramtypes", [http_1.Http])
], DocumentsHelper);
exports.DocumentsHelper = DocumentsHelper;
//# sourceMappingURL=documents-helper.component.js.map