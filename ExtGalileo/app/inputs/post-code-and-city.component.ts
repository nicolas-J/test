﻿import { Component, Input, OnInit, Output, EventEmitter, ViewChild, ViewChildren, QueryList, AfterViewInit} from '@angular/core';
import { SelectComponent } from 'ng2-select/ng2-select';
import { SelectItem } from 'ng2-select/select/select-item';
import { Headers, Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';

import { AppService } from "../services/app.service";

@Component({
    selector: 'post-code-and-city',
    templateUrl: '/app/inputs/post-code-and-city.component.html',
    inputs: ['postCode', 'city'],
    outputs: ['sharedSelectedPostCode', 'sharedSelectedCity'],
    providers: [AppService]
})
//<post-code-and-city [postCode]="parentPostCodeVar" [city]="parentCityVar" (sharedSelectedPostCode)='parentPostCodeVar=$event' (sharedSelectedCity)='parentCityVar=$event'></post-code-and-city>
export class PostCodeAndCityComponent implements OnInit, AfterViewInit {
    postCode: string;
    city: string;

    sharedSelectedPostCode = new EventEmitter();
    sharedSelectedCity = new EventEmitter();

    @ViewChildren(SelectComponent)
    selects: QueryList<SelectComponent>;

    @ViewChild(SelectComponent)
    private postCodeSelect: SelectComponent;

    @ViewChild(SelectComponent)
    private citySelect: SelectComponent;

    postCodeItems: Array<string> = new Array<string>();
    initPostCode: any;
    cityItems: Array<string> = new Array<string>();
    initCity: any ;

    constructor(private http: Http, private appService : AppService) { }

    ngOnInit(): void {
        this.postCodeItems[0] = this.postCode != null ? this.postCode :  "";
        this.initPostCode = [this.postCode != null ? this.postCode : ""];
        this.cityItems[0] = this.city != null ? this.city : "";
        this.initCity = [this.city != null ? this.city : ""];
    }

    ngAfterViewInit() {
        this.postCodeSelect = this.selects.first;
        this.citySelect = this.selects.last;
    }

    //Post code
    public selectedPostCode(value: any): void {
        if (this.postCode != value.text) {
            this.postCode = value.text;
            this.sharedSelectedPostCode.emit(value.text);
            this.searchCityByCode(value.text);
        }
    }

    public removedPostCode(value: any): void {
    }

    public typedPostCode(value: any): void {
        if (value.length === 5) {
            this.searchCityByCode(value);
        }
    }

    public onChangePostCode(event: any): void {
        if (this.postCodeItems.indexOf(event.target.value) > -1) {
            this.postCodeSelect.active = new Array<SelectItem>(this.postCodeSelect.itemObjects[this.postCodeItems.indexOf(event.target.value)]);
            this.postCodeSelect.activeOption = this.postCodeSelect.itemObjects[this.postCodeItems.indexOf(event.target.value)];
            this.postCodeSelect.selected.emit(this.postCodeSelect.itemObjects[this.postCodeItems.indexOf(event.target.value)]);
        } else {
            this.postCodeItems.push(event.target.value);
            this.postCodeSelect.itemObjects.push(new SelectItem({ id: event.target.value, text: event.target.value }));

            this.postCodeSelect.active = new Array<SelectItem>(this.postCodeSelect.itemObjects[this.postCodeSelect.itemObjects.length - 1]);
            this.postCodeSelect.activeOption = this.postCodeSelect.itemObjects[this.postCodeSelect.itemObjects.length - 1];
            this.postCodeSelect.selected.emit(this.postCodeSelect.itemObjects[this.postCodeSelect.itemObjects.length - 1]);
        }
    }

    //City
    public selectedCity(value: any): void {
        if (this.city != value.text) {
            this.city = value.text;
            this.sharedSelectedCity.emit(value.text);
            this.searchPostCodeByCity(value.text);
        }
    }

    public removedCity(value: any): void {
    }

    public typedCity(value: any): void {
        if (value.length > 3) {
            this.searchPostCodeByCity(value);
        }
    }

    public onChangeCity(event: any): void {
        if (this.cityItems.indexOf(event.target.value.toUpperCase()) > -1) {
            this.citySelect.active = new Array<SelectItem>(this.citySelect.itemObjects[this.cityItems.indexOf(event.target.value.toUpperCase())]);
            this.citySelect.activeOption = this.citySelect.itemObjects[this.cityItems.indexOf(event.target.value.toUpperCase())];
            this.citySelect.selected.emit(this.citySelect.itemObjects[this.cityItems.indexOf(event.target.value.toUpperCase())]);
        } else {
            this.cityItems.push(event.target.value.toUpperCase());
            this.citySelect.itemObjects.push(new SelectItem({ id: event.target.value.toUpperCase(), text: event.target.value.toUpperCase() }));

            this.citySelect.active = new Array<SelectItem>(this.citySelect.itemObjects[this.citySelect.itemObjects.length - 1]);
            this.citySelect.activeOption = this.citySelect.itemObjects[this.citySelect.itemObjects.length - 1];
            this.citySelect.selected.emit(this.citySelect.itemObjects[this.citySelect.itemObjects.length - 1]);
        }
    }

    //PostCode
    private searchPostCodeByCity(city: any) {
        if (city != "") {
            this.appService.sendDatas('/Tools/GetCity/', city).subscribe(callback => {
                this.setCityValue(callback);
            });
        }
    }

    private setPostCode(response: any) {
        var postCodes = JSON.parse(response._body);
        this.postCodeItems = new Array<string>();
        this.postCodeSelect.itemObjects = new Array<SelectItem>();
        for (var i = 0; i < postCodes.length; i++) {
            this.postCodeItems.push(postCodes[i]);
            this.postCodeSelect.itemObjects.push(new SelectItem({ id: postCodes[i], text: postCodes[i] }));
        }
        if (postCodes.length == 1) {
            this.postCodeSelect.active = new Array<SelectItem>(this.postCodeSelect.itemObjects[0]);
            this.postCodeSelect.activeOption = this.postCodeSelect.itemObjects[0];
            this.postCodeSelect.selected.emit(this.postCodeSelect.itemObjects[0]);
        } else if (postCodes.length > 1) {
            $(".postCodeCssClass .ui-select-toggle").click();
        }
    }

    //City
    private searchCityByCode(postcode: any) {
        if (postcode != "") {
            this.appService.sendDatas('/Tools/GetCity/', postcode).subscribe(callback => {
                this.setCityValue(callback);
            });
        }
    }

    private setCityValue(response: any) {
        var citys = response;
        this.cityItems = new Array<string>();
        this.citySelect.itemObjects = new Array<SelectItem>();
        for (var i = 0; i < citys.length; i++) {
            this.cityItems.push(citys[i]);
            this.citySelect.itemObjects.push(new SelectItem({ id: citys[i], text: citys[i] }));
        }
        if (citys.length == 1) {
            this.citySelect.active = new Array<SelectItem>(this.citySelect.itemObjects[0]);
            this.citySelect.activeOption = this.citySelect.itemObjects[0];
            this.citySelect.selected.emit(this.citySelect.itemObjects[0]);
        } else if (citys.length > 1){
            $(".cityCssClass .ui-select-toggle").click();
        }
    }
}