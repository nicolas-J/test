﻿import { Component, EventEmitter, Input, OnInit, ViewChild } from '@angular/core';
import 'rxjs/add/operator/toPromise';

//Services
import { MasksService } from "../services/masks.service";

@Component({
    selector: 'bic',
    templateUrl: '/app/inputs/bic.component.html',
    inputs: ['bicInput'],
    outputs: ['sharedBic']
})

export class BicComponent implements OnInit {
    bicInput: string;
    sharedBic = new EventEmitter();

    bic: string;
    isGreen: boolean = false;
    isRed: boolean = false;
    bicMask: any;

    constructor(private masksService : MasksService) { }

    ngOnInit(): void {
        this.bicMask = this.masksService.getBicMask();
        this.bic = this.bicInput;
    }

    bicChanged(event: any) {
        event.target.value = event.target.value.toUpperCase();
        this.sharedBic.emit(event.target.value);
    }
    
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}