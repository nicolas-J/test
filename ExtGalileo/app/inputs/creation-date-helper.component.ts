﻿import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

//Models
import { AxaServicePro } from "../models/AxaServicePro";
import { ExtranetSubscription } from "../models/ExtranetSubscription";
import { Message } from 'primeng/primeng';

//Service
import { MasksService } from "../services/masks.service";

@Component({
    selector: "creation-date-helper",
    templateUrl: "/app/inputs/creation-date-helper.component.html",
    inputs: ['model'],
    outputs: ['modelChange']
})


export class CreationDateHelper {
    model: ExtranetSubscription;
    modelChange = new EventEmitter();
    dateMask: any;
    dateForForm: string;
    msgs: Message[] = [];

    constructor(private masksService: MasksService) { }

    ngOnInit() {
        var day: string;
        var month: string;
        var checkDate = this.model.RecoveryOrCreationDate.toString().split("/");
        function pad(s: any) { return (s < 10) ? '0' + s : s; }
        this.model.RecoveryOrCreationDate = new Date([pad(checkDate[1]), pad(checkDate[0]), checkDate[2]].join('/'));
        
        if (typeof this.model.RecoveryOrCreationDate !== "undefined") {
            day = this.model.RecoveryOrCreationDate.getDate().toString();
            month = (this.model.RecoveryOrCreationDate.getMonth() + 1).toString();
            this.dateForForm = [pad(day), pad(month), this.model.RecoveryOrCreationDate.getFullYear()].join('/');
        }
        this.dateMask = this.masksService.getDatetMask();
    }

    onDataChange() {
        this.modelChange.emit(this.model);
    }

    updateDate(event: any) {
        var splitDate = event.target.value.split("/");
        var dateForTest = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
        if (Object.prototype.toString.call(dateForTest) === "[object Date]") {
            if (isNaN(dateForTest.getTime())) {
                this.model.RecoveryOrCreationDate = null;
                event.target.value = "";
                this.showWarn("Veuillez entrer une date valide.");
            }
            else {
                this.model.RecoveryOrCreationDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
            }
        }
        else {
            this.model.RecoveryOrCreationDate = null;
            event.target.value = "";
            this.showWarn("Veuillez entrer une date valide.");
        }
        this.onDataChange();
    }

    showWarn(message: string) {
        this.msgs = [];
        this.msgs.push({ severity: "error", summary: "Attention", detail: message });
    }
}