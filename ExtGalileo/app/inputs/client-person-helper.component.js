"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
//Service
var masks_service_1 = require("../services/masks.service");
var app_service_1 = require("../services/app.service");
var ClientPersonHelper = (function () {
    function ClientPersonHelper(masksService, appService) {
        this.masksService = masksService;
        this.appService = appService;
        this.modelChange = new core_1.EventEmitter();
        this.check = true;
        this.SituationDropDown = [];
        this.msgs = [];
        this.validation = new core_1.EventEmitter();
        this.SituationDropDown.push({ label: 'Célibataire', value: "1" });
        this.SituationDropDown.push({ label: 'Marié(e)', value: "2" });
        this.SituationDropDown.push({ label: 'Veuf(ve)', value: "3" });
        this.SituationDropDown.push({ label: 'Divorcé(e)', value: "4" });
        this.SituationDropDown.push({ label: 'Séparé(e) de corps judiciairement', value: "5" });
        this.SituationDropDown.push({ label: 'Concubin(e)', value: "6" });
        this.SituationDropDown.push({ label: 'Pacsé(e)', value: "7" });
    }
    ClientPersonHelper.prototype.ngOnInit = function () {
        if (typeof this.model.ClientPerson.BirthDate !== 'undefined' && this.model.ClientPerson.BirthDate !== null) {
            var day = this.model.ClientPerson.BirthDate.getDate().toString();
            if (this.model.ClientPerson.BirthDate.getDate() < 10)
                day = "0" + (this.model.ClientPerson.BirthDate.getDate());
            var month = (this.model.ClientPerson.BirthDate.getMonth() + 1).toString();
            if ((this.model.ClientPerson.BirthDate.getMonth() + 1) < 10)
                month = "0" + (this.model.ClientPerson.BirthDate.getMonth() + 1);
            this.dateForForm = day + "/" + month + "/" + this.model.ClientPerson.BirthDate.getFullYear();
            this.data = this.model;
            if (this.model.ClientPerson.TitleString == "MR")
                this.check = true;
            else {
                this.check = false;
            }
        }
        this.phoneMask = this.masksService.getPhoneMask();
        this.secuMask = this.masksService.getSecuMask();
        this.dateMask = this.masksService.getDatetMask();
        this.birthDepartmentMask = this.masksService.getBirthDepartmentMask();
        this.model.ClientPerson.SocialSecurityRegime = "RG";
        this.model.ClientPerson.BirthDate = new Date(1980, 1, 1);
    };
    ClientPersonHelper.prototype.ngAfterViewInit = function () {
        var _this = this;
        this.clientPersonInputs.valueChanges.subscribe(function (data) {
            _this.validation.emit({ control: _this.clientPersonInputs.controls });
        });
    };
    ClientPersonHelper.prototype.onDataChange = function () {
        this.modelChange.emit(this.model);
    };
    ClientPersonHelper.prototype.checkTitle = function (event) {
        if (event.checked == true)
            this.model.ClientPerson.TitleString = "MR";
        else {
            this.model.ClientPerson.TitleString = "MME";
        }
        this.onDataChange();
    };
    ClientPersonHelper.prototype.changePostCode = function (event) {
        this.model.ClientPerson.PostalAddress.PostCode = event;
        this.onDataChange();
    };
    ClientPersonHelper.prototype.changeCity = function (event) {
        this.model.ClientPerson.PostalAddress.City = event;
        this.onDataChange();
    };
    ClientPersonHelper.prototype.updateDate = function (event) {
        var splitDate = event.target.value.split("/");
        var dateForTest = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
        if ((new Date().getFullYear() - splitDate[2]) > this.ageMax) {
            event.target.value = "";
            this.model.ClientPerson.BirthDate = null;
            this.model.Base.Price = 0;
            this.showWarn("L'assuré ne peut souscrire au delà de " + this.ageMax + "ans");
        }
        else {
            if (Object.prototype.toString.call(dateForTest) === "[object Date]") {
                if (isNaN(dateForTest.getTime())) {
                    this.model.ClientPerson.BirthDate = null;
                    this.model.Base.Price = 0;
                    event.target.value = "";
                    this.showWarn("Veuillez entrer une date valide.");
                }
                else {
                    this.model.ClientPerson.BirthDate = dateForTest;
                }
            }
            else {
                this.model.ClientPerson.BirthDate = null;
                this.model.Base.Price = 0;
                event.target.value = "";
                this.showWarn("Veuillez entrer une date valide.");
            }
            this.onDataChange();
        }
    };
    ClientPersonHelper.prototype.showWarn = function (message) {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: 'Attention', detail: message });
    };
    return ClientPersonHelper;
}());
__decorate([
    core_1.ViewChild('clientPersonInputs'),
    __metadata("design:type", forms_1.NgForm)
], ClientPersonHelper.prototype, "clientPersonInputs", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ClientPersonHelper.prototype, "validation", void 0);
ClientPersonHelper = __decorate([
    core_1.Component({
        selector: "client-person-helper",
        templateUrl: "/app/inputs/client-person-helper.component.html",
        inputs: ['model', 'inputList', 'ageMax'],
        outputs: ['modelChange'],
        providers: [app_service_1.AppService]
    })
    //arguments possible de inputList:
    //Title -> titre
    //Civility -> civilité
    //Name -> nom + prénom
    //Secu -> numéro de sécu
    //Email -> email
    //Address -> adresse
    //CityPostCode -> ville + code postal
    //Phone -> numéro mobile
    //BirthDate -> date de naissance
    //ageMax -> Age maximum pour souscrire
    //FamilyStatus -> situation familiale
    //SocialSecurityRegime -> Régime Sociale (RG AM TNS)
    ,
    __metadata("design:paramtypes", [masks_service_1.MasksService, app_service_1.AppService])
], ClientPersonHelper);
exports.ClientPersonHelper = ClientPersonHelper;
//# sourceMappingURL=client-person-helper.component.js.map