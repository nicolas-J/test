"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
//Service
var masks_service_1 = require("../services/masks.service");
var CreationDateHelper = (function () {
    function CreationDateHelper(masksService) {
        this.masksService = masksService;
        this.modelChange = new core_1.EventEmitter();
        this.msgs = [];
    }
    CreationDateHelper.prototype.ngOnInit = function () {
        var day;
        var month;
        var checkDate = this.model.RecoveryOrCreationDate.toString().split("/");
        function pad(s) { return (s < 10) ? '0' + s : s; }
        this.model.RecoveryOrCreationDate = new Date([pad(checkDate[1]), pad(checkDate[0]), checkDate[2]].join('/'));
        if (typeof this.model.RecoveryOrCreationDate !== "undefined") {
            day = this.model.RecoveryOrCreationDate.getDate().toString();
            month = (this.model.RecoveryOrCreationDate.getMonth() + 1).toString();
            this.dateForForm = [pad(day), pad(month), this.model.RecoveryOrCreationDate.getFullYear()].join('/');
        }
        this.dateMask = this.masksService.getDatetMask();
    };
    CreationDateHelper.prototype.onDataChange = function () {
        this.modelChange.emit(this.model);
    };
    CreationDateHelper.prototype.updateDate = function (event) {
        var splitDate = event.target.value.split("/");
        var dateForTest = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
        if (Object.prototype.toString.call(dateForTest) === "[object Date]") {
            if (isNaN(dateForTest.getTime())) {
                this.model.RecoveryOrCreationDate = null;
                event.target.value = "";
                this.showWarn("Veuillez entrer une date valide.");
            }
            else {
                this.model.RecoveryOrCreationDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
            }
        }
        else {
            this.model.RecoveryOrCreationDate = null;
            event.target.value = "";
            this.showWarn("Veuillez entrer une date valide.");
        }
        this.onDataChange();
    };
    CreationDateHelper.prototype.showWarn = function (message) {
        this.msgs = [];
        this.msgs.push({ severity: "error", summary: "Attention", detail: message });
    };
    return CreationDateHelper;
}());
CreationDateHelper = __decorate([
    core_1.Component({
        selector: "creation-date-helper",
        templateUrl: "/app/inputs/creation-date-helper.component.html",
        inputs: ['model'],
        outputs: ['modelChange']
    }),
    __metadata("design:paramtypes", [masks_service_1.MasksService])
], CreationDateHelper);
exports.CreationDateHelper = CreationDateHelper;
//# sourceMappingURL=creation-date-helper.component.js.map