﻿import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

//Models
import { ClientPerson } from '../models/ClientPerson';

//Service
import { MasksService } from "../services/masks.service";

@Component({
    selector: "child-info-helper",
    templateUrl: "/app/inputs/child-info-helper.component.html",
    inputs: ['model', 'inputList']
})
//arguments possible de inputList:
//Civility -> civilité + situation familiale
//TItle -> titre
//Name -> nom + prénom
//Secu -> numéro de sécu
//BirthDate -> date de naissance

export class ChildInfoComponent {

    //Variables 
    inputList: string[];
    model: ClientPerson[];
    @Output() modelChange = new EventEmitter();
    check: boolean[] = [];

    secuMask: any;

    data: ClientPerson[];
    constructor(private masksService: MasksService) { }
    ngOnInit() {
        for (var i = 0; i < 4; i++) {
            this.check.push(true);
        }
        this.model.forEach((child) => {
            if (child.TitleString == "MR")
                this.check[this.model.indexOf(child)] = true;
            else {
                this.check[this.model.indexOf(child)] = false;
            }
        });
        this.data = this.model;
        this.secuMask = this.masksService.getSecuMask();
    }

    onDataChange() {
        this.modelChange.emit(this.model);
    }


    completeSecurityNumber(event: any, indexString: string) {
        var index = indexString.toString();
        this.model[index].SocialSecurityNumber = event.target.value;
        this.onDataChange();
    }


    checkTitle(event: any, indexString: string) {
        var index = indexString.toString();
        if (event.checked == true)
            this.model[index].TitleString = "MR";
        else {
            this.model[index].TitleString = "MME";
        }
        this.onDataChange();
    }
}