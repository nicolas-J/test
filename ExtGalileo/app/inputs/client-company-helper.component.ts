﻿import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { Headers, Http, Response } from "@angular/http";

//Models
import { ClientCompany } from "../models/ClientCompany";

//Prime
import { SelectItem } from "primeng/primeng";
import { Message } from 'primeng/primeng';

//Service
import { MasksService } from "../services/masks.service";

@Component({
    selector: "client-company-helper",
    templateUrl: "/app/inputs/client-company-helper.component.html",
    inputs: ['model', 'inputList'],
    outputs: ['modelChange']
})

//arguments possibles de inputList:
//LegalForm -> Forme legale
//Name -> Raison Sociale
//NafCode -> Code APE
//ContactDetails -> Contact details
//Address -> Adresse
//CityPostCode -> Ville + code postal
//Siret -> Siret number

export class ClientCompanyHelper {
    //Variables 
    model: ClientCompany;
    inputList: string[];
    modelChange = new EventEmitter();
    msgs: Message[] = [];

    LegalFormList: SelectItem[];

    siretMask: any;
    nafMask: any;
    phoneMask: any;
    employeesMask: any;

    constructor(maskService: MasksService, private http: Http) {
        this.siretMask = maskService.getSiretMask();
        this.nafMask = maskService.getNafMask();
        this.phoneMask = maskService.getPhoneMask();
        this.employeesMask = maskService.getEmployeesMask();
    }
    ngOnInit() {

        this.LegalFormList = [];
        this.LegalFormList.push({ label: "---", value: "" });
        this.LegalFormList.push({ label: "SAS", value: "SAS" });
        this.LegalFormList.push({ label: "SARL", value: "SARL" });
        this.LegalFormList.push({ label: "EURL", value: "EURL" });
        this.LegalFormList.push({ label: "SASU", value: "SASU" });
        this.LegalFormList.push({ label: "EI", value: "EI" });
        this.LegalFormList.push({ label: "SNC", value: "SNC" });
        this.LegalFormList.push({ label: "Particulier", value: "Particulier" });
    }
    
    //getNafCode(naf) {
    //    console.log(naf.target.value);
    //    if (naf.target.value != "") {
    //        let headers = new Headers();
    //        headers.append('Content-Type', 'application/json');
    //        this.http.post("/api/angular/GetNafCodeList/" + naf.target.value, "", { headers: headers })
    //            .toPromise()
    //            .then(res => this.allocateNafList(res));
    //    }
    //}

    //allocateNafList(response) {
    //    const nafCodes = JSON.parse(response._body);
    //    console.log(nafCodes);
    //    //if (nafCodes.Error != null) {
    //    //    this.showMessage("error", "Attention!", nafCodes.Error);
    //    //} else if (nafCodes.Data == "Aucun code Naf trouvé") {
    //    //    this.showMessage("error", "Attention!", nafCodes.Error);
    //    //} else {
    //    //    this.model.AxaServiceProModel.AnnualRate = nafCodes.Total;
    //    //    this.model.AxaServiceProModel.SixMonthRate = (nafCodes.Total / 2).toFixed(2);
    //    //    this.model.Base.Price = (nafCodes.Total / 12).toFixed(2);
    //    //    this.model.AxaServiceProModel.EstimatedCommission = nafCodes.CommBroker;
    //    //    this.clearMessages();
    //    //}
    //}

    onDataChange() {
        this.modelChange.emit(this.model);
    }


    changeEmployeesNumber(event: any) {
        var temp : number = event.target.value;
        if (temp > 300) {
            this.showWarn("Le nombre de salariés ne peut pas dépasser 300");
            event.target.value = 0;
            this.model.EmployeesNumber = 0;
        } else {
            this.model.EmployeesNumber = event.target.value;
            this.onDataChange();
        }

    }


    changeEmployeesAverageAge(event: any) {
        var temp: number = event.target.value;
        if (temp > 65 || temp < 20) {
            this.showWarn("L'âge moyen des salariés doit être compris en 20 et 65 ans");
            event.target.value = 0;
            this.model.EmployeesAverageAge = 0;
        } else {
            this.model.EmployeesAverageAge = event.target.value;
            this.onDataChange();
        }

    }

    showWarn(message: string) {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: 'Attention', detail: message });
    }
}