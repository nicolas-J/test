﻿import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import { Headers, Http, Response } from '@angular/http';
import "rxjs/add/operator/toPromise";
import { Modal } from "ng2-modal";

//Models
import { ExtranetSubscription } from "../models/ExtranetSubscription";

//Prime ng
import { Message } from 'primeng/primeng';

@Component({
    selector: "documents-helper",
    templateUrl: "/app/inputs/documents-helper.component.html",
    inputs: ['model', 'inputList', 'roles', 'disableElement']
})

//INPUT VARIABLES
//PDF :
//Bia --> Bulletin individuel d'adhésion
//Bae --> Bulletin d'adhésion d'entreprise
//Sepa --> Mandat Sepa
//Devis --> Devis
//Notice --> Notice Informations
//BeneficiaryDesignation --> Clause de désignation de bénéficiaire
//Plaquette --> Plaquette
//Action :
//Download --> Télécharger les pdfs
//Sign --> Signature électronique
//Save --> Sauvegarder dans la base de données
//Bridge --> Bridge Extranet vers Assurware manuel
//roles --> Liste des rôles attribués à l'utilisateur

export class DocumentsHelper implements OnInit {

    msgs: Message[] = [];
    inputList: string[];
    model: ExtranetSubscription;
    @Output() modelChange = new EventEmitter();
    @ViewChild('signModal') signModal: Modal;
    id: string;
    roles: string[];
    disableElement: boolean;

    constructor(private http: Http) { }

    ngOnInit() {
    }

    downloadPdf() {
        $(".loader").show();
        this.saveProject();
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post("/Pdf/InitPdfDownload", JSON.stringify(this.model), { headers: headers })
            .toPromise()
            .then(res => this.showDownload(res));
    }

    showDownload(response: any) {
        const pdfs = JSON.stringify(response._body).replace(/"/g, "").replace(/\\/g, "");
        if (pdfs.match("Erreur")) {
            this.showMessage("error", "Attention!", pdfs.replace("u0027", "'"));
        } else if (pdfs == "") {
            this.showMessage("error", "Attention!", "Pdf indisponibles pour le moment, veuillez réessayer plus tard, merci.");
        } else {
            var link = document.createElement("a");
            link.setAttribute("download", pdfs);
            link.setAttribute("href", pdfs);
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);

            this.clearMessages();
        }
        $(".loader").hide();
    }

    saveProject() {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post("/Subscription/Save", this.model, { headers: headers })
            .toPromise()
            .then(res => this.showResult(res));
    }

    showResult(response: any) {
        const message = JSON.parse(response._body);
        if (message.Error != null) {
            this.showMessage("error", "Attention!", message);
        } else {
            this.showMessage("success", "Merci!", message);
        }
    }

    showSignModal() {
        this.signModal.open();
    }

    updateDocument() {
    }

    signProject() {
        $(".loader").show();
        this.saveProject();
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post("/Sign/SignInit", JSON.stringify(this.model), { headers: headers })
            .toPromise()
            .then(res => this.showResultSign(res));
    }

    showResultSign(response: any) {
        const message = JSON.stringify(response._body).replace(/"/g, "").replace(/\\/g, "");
        if (message.match("Erreur")) {
            this.showMessage("error", "Attention!", message.replace("u0027", "'"));
            this.signModal.close();
        } else {
            window.location.href = (message);
        }
        $(".loader").hide();
    }

    bridgeProject() {
        $(".loader").show();
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        this.http.post("/Sign/ManualBridge", JSON.stringify(this.model), { headers: headers })
            .toPromise()
            .then(res => this.showBridgeResult(res));
    }

    showBridgeResult(response: any) {
        const message = JSON.stringify(response._body).replace(/"/g, "").replace(/\\/g, "");
        if (message == "valide") {
            this.model.IsBridged = true;
            let sign = "Ordre66";
            window.location.href = "Sign/SignCallBack/" + this.model.Id + "?sign=" + sign;
        }
        else {
            this.showMessage("error", "Attention!", message.replace("u0027", "'"));
            $(".loader").hide();
        }
    }

    getPdfSign() {
        if (this.model.PdfSignUrl)
            window.location.href = (this.model.PdfSignUrl);
    }

    showMessage(severity: any, summary: any, message: any) {
        this.msgs.push({ severity: severity, summary: summary, detail: message });
    }

    clearMessages() {
        this.msgs = [];
    }

}