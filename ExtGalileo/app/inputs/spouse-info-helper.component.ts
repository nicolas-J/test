﻿import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { SelectItem } from 'primeng/primeng';

//Models
import { ExtranetSubscription } from "../models/ExtranetSubscription";
import { ClientPerson } from "../models/ClientPerson";
import { Message } from 'primeng/primeng';



//Service
import { MasksService } from "../services/masks.service";

@Component({
    selector: "spouse-info-helper",
    templateUrl: "/app/inputs/spouse-info-helper.component.html",
    inputs: ['model', 'inputList'],
    outputs: ['modelChange']
})
//arguments possible de inputList:
//Civility -> civilité + situation familiale
//TItle -> titre
//Name -> nom + prénom
//Secu -> numéro de sécu
//BirthDate -> date de naissance
//SecondSubscriberBirthDate -> date de naissance du second assuré
//Email -> email du conjoint
//Phone -> mobile du conjoint
//Address -> Adresse
//CityPostCode -> ville + code postal
//FamilyStatus -> situation familiale

export class SpouseInfoHelper {

    inputList: string[];
    model: ExtranetSubscription;
    modelChange = new EventEmitter();
    check: boolean = true;
    dateForForm: string;
    msgs: Message[] = [];
    SituationDropDown: SelectItem[] = [];

    secuMask: any;
    dateMask: any;
    phoneMask: any;
    birthDepartmentMask: any;

    data: ExtranetSubscription;
    constructor(private masksService: MasksService) {
        this.SituationDropDown.push({ label: 'Célibataire', value: "1" });
        this.SituationDropDown.push({ label: 'Marié(e)', value: "2" });
        this.SituationDropDown.push({ label: 'Veuf(ve)', value: "3" });
        this.SituationDropDown.push({ label: 'Divorcé(e)', value: "4" });
        this.SituationDropDown.push({ label: 'Séparé(e) de corps judiciairement', value: "5" });
        this.SituationDropDown.push({ label: 'Concubin(e)', value: "6" });
        this.SituationDropDown.push({ label: 'Pacsé(e)', value: "7" }); }

    ngOnInit() {
        if (this.model.ClientPerson.Spouse != null && this.model.ClientPerson.Spouse.BirthDate != null  && typeof this.model.ClientPerson.Spouse.BirthDate !== 'undefined') {
            var day = this.model.ClientPerson.Spouse.BirthDate.getDate().toString();
            if (this.model.ClientPerson.Spouse.BirthDate.getDate() < 10)
                day = "0" + (this.model.ClientPerson.Spouse.BirthDate.getDate());
            var month = (this.model.ClientPerson.Spouse.BirthDate.getMonth() + 1).toString();
            if ((this.model.ClientPerson.Spouse.BirthDate.getMonth() + 1) < 10)
                month = "0" + (this.model.ClientPerson.Spouse.BirthDate.getMonth() + 1);
            this.dateForForm = day + "/" + month + "/" + this.model.ClientPerson.Spouse.BirthDate.getFullYear();
            this.data = this.model;
            if (this.model.ClientPerson.Spouse.TitleString == "MR")
                this.check = true;
            else {
                this.check = false;
            }
        }
        this.secuMask = this.masksService.getSecuMask();
        this.dateMask = this.masksService.getDatetMask();
        this.phoneMask = this.masksService.getPhoneMask();
        this.birthDepartmentMask = this.masksService.getBirthDepartmentMask();
    }

    onDataChange() {
        this.modelChange.emit(this.model);
    }


    completeSecurityNumber(event: any) {
        this.model.ClientPerson.Spouse.SocialSecurityNumber = event.target.value;
        this.onDataChange();
    }


    checkTitle(event: any) {
        if (event.checked == true)
            this.model.ClientPerson.Spouse.TitleString = "MR";
        else {
            this.model.ClientPerson.Spouse.TitleString = "MME";
        }
        this.onDataChange();
    }

    updateDate(event: any) {
        var splitDate = event.target.value.split("/");
        var dateForTest = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
        if (Object.prototype.toString.call(dateForTest) === "[object Date]") {
            if (isNaN(dateForTest.getTime())) {
                if (this.model.ClientPerson.Spouse != null)
                    this.model.ClientPerson.Spouse.BirthDate = null;
                event.target.value = "";
                this.showWarn("Veuillez entrer une date valide.");
            }
            else {
                if (this.model.ClientPerson.Spouse == null)
                    this.model.ClientPerson.Spouse = new ClientPerson;
                this.model.ClientPerson.Spouse.BirthDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
            }
        }
        else {
            if (this.model.ClientPerson.Spouse != null)
                this.model.ClientPerson.Spouse.BirthDate = null;
            event.target.value = "";
            this.showWarn("Veuillez entrer une date valide.");
        }
        this.onDataChange();
    }

    changePostCode(event: any) {
        this.model.ClientPerson.Spouse.PostalAddress.PostCode = event;
        this.onDataChange();
    }

    changeCity(event: any) {
        this.model.ClientPerson.Spouse.PostalAddress.City = event;
        this.onDataChange();
    }

    showWarn(message: string) {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: 'Attention', detail: message });
    }
}