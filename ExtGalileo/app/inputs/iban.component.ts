﻿import { Component, EventEmitter, Input, OnInit } from '@angular/core';

//Services
import { MasksService } from "../services/masks.service";

@Component({
    selector: 'iban',
    templateUrl: '/app/inputs/iban.component.html',
    inputs: ['ibanInput'],
    outputs: ['sharedIban']
})

export class IbanComponent implements OnInit {
    ibanInput: string;
    sharedIban = new EventEmitter();

    iban: string;
    isGreen: boolean = false;
    isRed: boolean = false;
    ibanMask: any;

    constructor(private masksService: MasksService) { }

    ngOnInit(): void {
        this.iban = this.ibanInput;
        this.ibanMask = this.masksService.getIbanMask();
    }

    ibanChanged(event: any) {
        event.target.value = event.target.value.toUpperCase();
        this.sharedIban.emit(event.target.value);
    }
    
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}