﻿import { Component, OnInit, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import { SelectItem } from 'primeng/primeng';
import { Message } from 'primeng/primeng';
import { NgForm } from "@angular/forms";

//Models
import { ExtranetSubscription } from "../models/ExtranetSubscription";

//Service
import { MasksService } from "../services/masks.service";
import { AppService } from "../services/app.service";

@Component({
    selector: "client-person-helper",
    templateUrl: "/app/inputs/client-person-helper.component.html",
    inputs: ['model', 'inputList', 'ageMax'],
    outputs: ['modelChange'],
    providers: [AppService]
})

//arguments possible de inputList:
//Title -> titre
//Civility -> civilité
//Name -> nom + prénom
//Secu -> numéro de sécu
//Email -> email
//Address -> adresse
//CityPostCode -> ville + code postal
//Phone -> numéro mobile
//BirthDate -> date de naissance
//ageMax -> Age maximum pour souscrire
//FamilyStatus -> situation familiale
//SocialSecurityRegime -> Régime Sociale (RG AM TNS)

export class ClientPersonHelper {

    model: ExtranetSubscription;
    inputList: string[];
    modelChange = new EventEmitter();
    check: boolean = true;
    SituationDropDown: SelectItem[] = [];
    msgs: Message[] = [];
    dateForForm: string;
    secuMask: any;
    phoneMask: any;
    dateMask: any;
    birthDepartmentMask: any;
    ageMax: number;
    required: boolean;
    @ViewChild('clientPersonInputs') clientPersonInputs: NgForm;
    @Output() validation = new EventEmitter();

    data: ExtranetSubscription;

    constructor(private masksService: MasksService, private appService : AppService) {
        this.SituationDropDown.push({ label: 'Célibataire', value: "1" });
        this.SituationDropDown.push({ label: 'Marié(e)', value: "2" });
        this.SituationDropDown.push({ label: 'Veuf(ve)', value: "3" });
        this.SituationDropDown.push({ label: 'Divorcé(e)', value: "4" });
        this.SituationDropDown.push({ label: 'Séparé(e) de corps judiciairement', value: "5" });
        this.SituationDropDown.push({ label: 'Concubin(e)', value: "6" });
        this.SituationDropDown.push({ label: 'Pacsé(e)', value: "7" });
    }
    
    ngOnInit() {
        if (typeof this.model.ClientPerson.BirthDate !== 'undefined' && this.model.ClientPerson.BirthDate !== null) {
            var day = this.model.ClientPerson.BirthDate.getDate().toString();
            if (this.model.ClientPerson.BirthDate.getDate() < 10)
                day = "0" + (this.model.ClientPerson.BirthDate.getDate());
            var month = (this.model.ClientPerson.BirthDate.getMonth() + 1).toString();
            if ((this.model.ClientPerson.BirthDate.getMonth() + 1) < 10)
                month = "0" + (this.model.ClientPerson.BirthDate.getMonth() + 1);
            this.dateForForm = day + "/" + month + "/" + this.model.ClientPerson.BirthDate.getFullYear();
            this.data = this.model;
            if (this.model.ClientPerson.TitleString == "MR")
                this.check = true;
            else {
                this.check = false;
            }
        }
        this.phoneMask = this.masksService.getPhoneMask();
        this.secuMask = this.masksService.getSecuMask();
        this.dateMask = this.masksService.getDatetMask();
        this.birthDepartmentMask = this.masksService.getBirthDepartmentMask();
        this.model.ClientPerson.SocialSecurityRegime = "RG";
        this.model.ClientPerson.BirthDate = new Date(1980, 1, 1);
    }

    ngAfterViewInit() {
        this.clientPersonInputs.valueChanges.subscribe(data => {
            this.validation.emit({ control: this.clientPersonInputs.controls });
        });
    }

    onDataChange() {
        this.modelChange.emit(this.model);
    }
    
    checkTitle(event: any) {
        if (event.checked == true)
            this.model.ClientPerson.TitleString = "MR";
        else {
            this.model.ClientPerson.TitleString = "MME";
        }
        this.onDataChange();
    }

    changePostCode(event: any) {
        this.model.ClientPerson.PostalAddress.PostCode = event;
        this.onDataChange();
    }

    changeCity(event: any) {
        this.model.ClientPerson.PostalAddress.City = event;
        this.onDataChange();
    }
    
    updateDate(event: any) {
        var splitDate = event.target.value.split("/");
        var dateForTest = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);

        if ((new Date().getFullYear() - splitDate[2]) > this.ageMax) {
            event.target.value = "";
            this.model.ClientPerson.BirthDate = null;
            this.model.Base.Price = 0;
            this.showWarn("L'assuré ne peut souscrire au delà de " + this.ageMax + "ans");
        }
        else {
            if (Object.prototype.toString.call(dateForTest) === "[object Date]") {
                if (isNaN(dateForTest.getTime())) {
                    this.model.ClientPerson.BirthDate = null;
                    this.model.Base.Price = 0;
                    event.target.value = "";
                    this.showWarn("Veuillez entrer une date valide.");
                }
                else {
                    this.model.ClientPerson.BirthDate = dateForTest;
                }
            }
            else {
                this.model.ClientPerson.BirthDate = null;
                this.model.Base.Price = 0;
                event.target.value = "";
                this.showWarn("Veuillez entrer une date valide.");
            }
            this.onDataChange();
        }
    }

    showWarn(message: string) {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: 'Attention', detail: message });
    }
}