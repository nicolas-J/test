"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
//Service
var masks_service_1 = require("../services/masks.service");
var EffectDateHelper = (function () {
    function EffectDateHelper(masksService) {
        this.masksService = masksService;
        this.modelChange = new core_1.EventEmitter();
        this.msgs = [];
    }
    EffectDateHelper.prototype.ngOnInit = function () {
        var day;
        var month;
        var checkDate = this.model.EffectDate.toDateString().split("/");
        this.model.EffectDate = new Date([this.pad(checkDate[1]), this.pad(checkDate[0]), checkDate[2]].join('/'));
        if (typeof this.startDate !== "undefined") {
            day = this.startDate.getDate().toString();
            month = (this.startDate.getMonth() + 1).toString();
            this.startDateForForm = [this.pad(day), this.pad(month), this.startDate.getFullYear()].join('/');
        }
        if (typeof this.endDate !== "undefined") {
            day = this.endDate.getDate().toString();
            month = (this.endDate.getMonth() + 1).toString();
            this.endDateForForm = [this.pad(day), this.pad(month), this.endDate.getFullYear()].join('/');
        }
        if (typeof this.model.EffectDate !== "undefined") {
            day = this.model.EffectDate.getDate().toString();
            month = (this.model.EffectDate.getMonth() + 1).toString();
            if (this.firstOfMonth)
                day = "1";
            this.dateForForm = [this.pad(day), this.pad(month), this.model.EffectDate.getFullYear()].join('/');
        }
        this.dateMask = this.masksService.getDatetMask();
    };
    EffectDateHelper.prototype.onDataChange = function () {
        this.modelChange.emit(this.model);
    };
    EffectDateHelper.prototype.pad = function (s) { return (s < 10) ? '0' + s : s; };
    EffectDateHelper.prototype.updateDate = function (event) {
        var splitDate = event.target.value.split("/");
        var dateForTest = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
        if (Object.prototype.toString.call(dateForTest) === "[object Date]") {
            if (isNaN(dateForTest.getTime())) {
                this.model.EffectDate = null;
                event.target.value = "";
                this.showWarn("Veuillez entrer une date valide.");
            }
            else {
                if (this.roles.indexOf("Admin") < 0 && this.roles.indexOf("SpvieManager") < 0) {
                    if (dateForTest < this.startDate) {
                        this.model.EffectDate = null;
                        event.target.value = "";
                        this.showWarn("La date d'effet ne peut pas \u00EAtre ant\u00E9rieure au " + this.startDateForForm);
                    }
                    else if (dateForTest > this.endDate) {
                        this.model.EffectDate = null;
                        event.target.value = "";
                        this.showWarn("La date d'effet ne peut pas \u00EAtre ult\u00E9rieure au " + this.endDateForForm);
                    }
                    else {
                        if (this.firstOfMonth) {
                            this.model.EffectDate = new Date(splitDate[1] + "/" + 1 + "/" + splitDate[2]);
                            this.dateForForm = "01/" + splitDate[1] + "/" + splitDate[2];
                            if (this.model.EffectDate < this.startDate) {
                                this.model.EffectDate = new Date((parseInt(splitDate[1]) + 1) + "/" + 1 + "/" + splitDate[2]);
                                this.dateForForm = "01/" + this.pad((parseInt(splitDate[1]) + 1)) + "/" + splitDate[2];
                            }
                        }
                        else {
                            this.model.EffectDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
                        }
                    }
                }
                else {
                    if (dateForTest > this.endDate) {
                        this.model.EffectDate = null;
                        event.target.value = "";
                        this.showWarn("La date d'effet ne peut pas \u00EAtre sup\u00E9rieure au " + this.endDateForForm);
                    }
                    else {
                        if (this.firstOfMonth) {
                            this.model.EffectDate = new Date(splitDate[1] + "/" + 1 + "/" + splitDate[2]);
                            this.dateForForm = "01/" + splitDate[1] + "/" + splitDate[2];
                        }
                        else {
                            this.model.EffectDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
                        }
                    }
                }
            }
        }
        else {
            this.model.EffectDate = null;
            event.target.value = "";
            this.showWarn("Veuillez entrer une date d'effet valide.");
        }
        this.onDataChange();
    };
    EffectDateHelper.prototype.showWarn = function (message) {
        this.msgs = [];
        this.msgs.push({ severity: "error", summary: "Attention", detail: message });
    };
    return EffectDateHelper;
}());
EffectDateHelper = __decorate([
    core_1.Component({
        selector: "effect-date-helper",
        templateUrl: "/app/inputs/effect-date-helper.component.html",
        inputs: ['model', 'startDate', 'endDate', 'firstOfMonth', 'roles', 'validationForm'],
        outputs: ['modelChange']
    })
    //startDate --> Début de date d'effet
    //endDate --> Date d'effet limite
    //firstOfMonth --> Date d'effet doit être au 1er du mois
    //roles --> Liste des rôles attribués à l'utilisateur
    ,
    __metadata("design:paramtypes", [masks_service_1.MasksService])
], EffectDateHelper);
exports.EffectDateHelper = EffectDateHelper;
//# sourceMappingURL=effect-date-helper.component.js.map