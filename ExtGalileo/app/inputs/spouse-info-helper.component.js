"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ClientPerson_1 = require("../models/ClientPerson");
//Service
var masks_service_1 = require("../services/masks.service");
var SpouseInfoHelper = (function () {
    function SpouseInfoHelper(masksService) {
        this.masksService = masksService;
        this.modelChange = new core_1.EventEmitter();
        this.check = true;
        this.msgs = [];
        this.SituationDropDown = [];
        this.SituationDropDown.push({ label: 'Célibataire', value: "1" });
        this.SituationDropDown.push({ label: 'Marié(e)', value: "2" });
        this.SituationDropDown.push({ label: 'Veuf(ve)', value: "3" });
        this.SituationDropDown.push({ label: 'Divorcé(e)', value: "4" });
        this.SituationDropDown.push({ label: 'Séparé(e) de corps judiciairement', value: "5" });
        this.SituationDropDown.push({ label: 'Concubin(e)', value: "6" });
        this.SituationDropDown.push({ label: 'Pacsé(e)', value: "7" });
    }
    SpouseInfoHelper.prototype.ngOnInit = function () {
        if (this.model.ClientPerson.Spouse != null && this.model.ClientPerson.Spouse.BirthDate != null && typeof this.model.ClientPerson.Spouse.BirthDate !== 'undefined') {
            var day = this.model.ClientPerson.Spouse.BirthDate.getDate().toString();
            if (this.model.ClientPerson.Spouse.BirthDate.getDate() < 10)
                day = "0" + (this.model.ClientPerson.Spouse.BirthDate.getDate());
            var month = (this.model.ClientPerson.Spouse.BirthDate.getMonth() + 1).toString();
            if ((this.model.ClientPerson.Spouse.BirthDate.getMonth() + 1) < 10)
                month = "0" + (this.model.ClientPerson.Spouse.BirthDate.getMonth() + 1);
            this.dateForForm = day + "/" + month + "/" + this.model.ClientPerson.Spouse.BirthDate.getFullYear();
            this.data = this.model;
            if (this.model.ClientPerson.Spouse.TitleString == "MR")
                this.check = true;
            else {
                this.check = false;
            }
        }
        this.secuMask = this.masksService.getSecuMask();
        this.dateMask = this.masksService.getDatetMask();
        this.phoneMask = this.masksService.getPhoneMask();
        this.birthDepartmentMask = this.masksService.getBirthDepartmentMask();
    };
    SpouseInfoHelper.prototype.onDataChange = function () {
        this.modelChange.emit(this.model);
    };
    SpouseInfoHelper.prototype.completeSecurityNumber = function (event) {
        this.model.ClientPerson.Spouse.SocialSecurityNumber = event.target.value;
        this.onDataChange();
    };
    SpouseInfoHelper.prototype.checkTitle = function (event) {
        if (event.checked == true)
            this.model.ClientPerson.Spouse.TitleString = "MR";
        else {
            this.model.ClientPerson.Spouse.TitleString = "MME";
        }
        this.onDataChange();
    };
    SpouseInfoHelper.prototype.updateDate = function (event) {
        var splitDate = event.target.value.split("/");
        var dateForTest = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
        if (Object.prototype.toString.call(dateForTest) === "[object Date]") {
            if (isNaN(dateForTest.getTime())) {
                if (this.model.ClientPerson.Spouse != null)
                    this.model.ClientPerson.Spouse.BirthDate = null;
                event.target.value = "";
                this.showWarn("Veuillez entrer une date valide.");
            }
            else {
                if (this.model.ClientPerson.Spouse == null)
                    this.model.ClientPerson.Spouse = new ClientPerson_1.ClientPerson;
                this.model.ClientPerson.Spouse.BirthDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
            }
        }
        else {
            if (this.model.ClientPerson.Spouse != null)
                this.model.ClientPerson.Spouse.BirthDate = null;
            event.target.value = "";
            this.showWarn("Veuillez entrer une date valide.");
        }
        this.onDataChange();
    };
    SpouseInfoHelper.prototype.changePostCode = function (event) {
        this.model.ClientPerson.Spouse.PostalAddress.PostCode = event;
        this.onDataChange();
    };
    SpouseInfoHelper.prototype.changeCity = function (event) {
        this.model.ClientPerson.Spouse.PostalAddress.City = event;
        this.onDataChange();
    };
    SpouseInfoHelper.prototype.showWarn = function (message) {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: 'Attention', detail: message });
    };
    return SpouseInfoHelper;
}());
SpouseInfoHelper = __decorate([
    core_1.Component({
        selector: "spouse-info-helper",
        templateUrl: "/app/inputs/spouse-info-helper.component.html",
        inputs: ['model', 'inputList'],
        outputs: ['modelChange']
    })
    //arguments possible de inputList:
    //Civility -> civilité + situation familiale
    //TItle -> titre
    //Name -> nom + prénom
    //Secu -> numéro de sécu
    //BirthDate -> date de naissance
    //SecondSubscriberBirthDate -> date de naissance du second assuré
    //Email -> email du conjoint
    //Phone -> mobile du conjoint
    //Address -> Adresse
    //CityPostCode -> ville + code postal
    //FamilyStatus -> situation familiale
    ,
    __metadata("design:paramtypes", [masks_service_1.MasksService])
], SpouseInfoHelper);
exports.SpouseInfoHelper = SpouseInfoHelper;
//# sourceMappingURL=spouse-info-helper.component.js.map