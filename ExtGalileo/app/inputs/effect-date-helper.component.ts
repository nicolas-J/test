﻿import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

//Models
import { ExtranetSubscription } from "../models/ExtranetSubscription";
import { Message } from 'primeng/primeng';

//Service
import { MasksService } from "../services/masks.service";

@Component({
    selector: "effect-date-helper",
    templateUrl: "/app/inputs/effect-date-helper.component.html",
    inputs: ['model', 'startDate', 'endDate', 'firstOfMonth', 'roles', 'validationForm'],
    outputs: ['modelChange']
})

//startDate --> Début de date d'effet
//endDate --> Date d'effet limite
//firstOfMonth --> Date d'effet doit être au 1er du mois
//roles --> Liste des rôles attribués à l'utilisateur

export class EffectDateHelper {
    startDate: Date;
    endDate: Date;
    model: ExtranetSubscription;
    modelChange = new EventEmitter();
    firstOfMonth: boolean;
    dateMask: any;
    startDateForForm: string;
    endDateForForm: string;
    dateForForm: string;
    msgs: Message[] = [];
    roles: string[];
    isValidate: boolean;

    constructor(private masksService: MasksService) { }

    ngOnInit() {
        var day: string;
        var month: string;
        var checkDate = this.model.EffectDate.toDateString().split("/");
        this.model.EffectDate = new Date([this.pad(checkDate[1]), this.pad(checkDate[0]), checkDate[2]].join('/'));
        if (typeof this.startDate !== "undefined") {
            day = this.startDate.getDate().toString();
            month = (this.startDate.getMonth() + 1).toString();
            this.startDateForForm = [this.pad(day), this.pad(month), this.startDate.getFullYear()].join('/');
        }
        if (typeof this.endDate !== "undefined") {
            day = this.endDate.getDate().toString();
            month = (this.endDate.getMonth() + 1).toString();
            this.endDateForForm = [this.pad(day), this.pad(month), this.endDate.getFullYear()].join('/');
        }
        if (typeof this.model.EffectDate !== "undefined") {
            day = this.model.EffectDate.getDate().toString();
            month = (this.model.EffectDate.getMonth() + 1).toString();
            if (this.firstOfMonth)
                day = "1";
            this.dateForForm = [this.pad(day), this.pad(month), this.model.EffectDate.getFullYear()].join('/');
        }
        this.dateMask = this.masksService.getDatetMask();
    }

    onDataChange() {
        this.modelChange.emit(this.model);
    }
    pad(s: any) { return (s < 10) ? '0' + s : s; }

    updateDate(event: any) {
        var splitDate = event.target.value.split("/");
        var dateForTest = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
        if (Object.prototype.toString.call(dateForTest) === "[object Date]") {
            if (isNaN(dateForTest.getTime())) {
                this.model.EffectDate = null;
                event.target.value = "";
                this.showWarn("Veuillez entrer une date valide.");
            }
            else {
                if (this.roles.indexOf("Admin") < 0 && this.roles.indexOf("SpvieManager") < 0) {
                    if (dateForTest < this.startDate) {
                        this.model.EffectDate = null;
                        event.target.value = "";
                        this.showWarn(`La date d'effet ne peut pas être antérieure au ${this.startDateForForm}`);
                    } else if (dateForTest > this.endDate) {
                        this.model.EffectDate = null;
                        event.target.value = "";
                        this.showWarn(`La date d'effet ne peut pas être ultérieure au ${this.endDateForForm}`);
                    } else {
                        if (this.firstOfMonth) {
                            this.model.EffectDate = new Date(splitDate[1] + "/" + 1 + "/" + splitDate[2]);
                            this.dateForForm = `01/${splitDate[1]}/${splitDate[2]}`;
                            if (this.model.EffectDate < this.startDate) {
                                this.model.EffectDate = new Date((parseInt(splitDate[1]) + 1) + "/" + 1 + "/" + splitDate[2]);
                                this.dateForForm = `01/${this.pad((parseInt(splitDate[1]) + 1))}/${splitDate[2]}`;
                            }
                        } else {
                            this.model.EffectDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
                        }
                    }
                } else {
                    if (dateForTest > this.endDate) {
                        this.model.EffectDate = null;
                        event.target.value = "";
                        this.showWarn(`La date d'effet ne peut pas être supérieure au ${this.endDateForForm}`);
                    } else {
                        if (this.firstOfMonth) {
                            this.model.EffectDate = new Date(splitDate[1] + "/" + 1 + "/" + splitDate[2]);
                            this.dateForForm = `01/${splitDate[1]}/${splitDate[2]}`;
                        } else {
                            this.model.EffectDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
                        }
                    }
                }
            }
        }
        else {
            this.model.EffectDate = null;
            event.target.value = "";
            this.showWarn("Veuillez entrer une date d'effet valide.");
        }
        this.onDataChange();
    }

    showWarn(message: string) {
        this.msgs = [];
        this.msgs.push({ severity: "error", summary: "Attention", detail: message });
    }
}