﻿import { ClientPerson } from '../models/ClientPerson';
import { Component, OnInit, EventEmitter, Input } from '@angular/core';
import { Message } from 'primeng/primeng';
import { MasksService } from "../services/masks.service";

@Component({
        selector: "children-helper",
        templateUrl: "/app/inputs/children-helper.component.html",
        inputs: ['childInput', 'returnChildBirthDates'],
        outputs: ['childOutput']
    })

export class ChildrenHeplerComponent implements OnInit {
    childInput: ClientPerson[];
    childOutput = new EventEmitter();
    children: ClientPerson[];
    childrenTransit: string[] = [];
    numberOfChildren: number;
    oldNumberOfChildren: number;
    msgs: Message[] = [];
    dateMask: any;
    returnChildBirthDates: boolean;
    childrenTooltip = "Sont considérés comme « Enfants à charge » de l’Adhérent, les enfants qui bénéficient d’un régime d’assurance maladie obligatoire français du fait de l’affiliation de l’Adhérent (ou de son conjoint, à défaut de son partenaire de PACS ou de son concubin) ou d’une affiliation personnelle, et qui sont âgés de moins de 21 ans, ou qui sont âgés de moins de 26 ans et poursuivent leurs études et ne disposent pas de ressources propres provenant d’une activité salariée (sauf emplois occasionnels ou saisonniers ou d’appoint, durant leurs études), ou suivent une formation en alternance ou se trouvent en contrat d’apprentissage, ou sont inscrits à Pôle Emploi comme primo-demandeur d’emploi ou effectuent un stage préalable à un premier emploi rémunéré, ou, quel que soit leur âge, qui bénéficient d’une allocation prévue par la législation sociale en faveur des Handicapés en vigueur.";

    constructor(private masksService: MasksService) { }

    ngOnInit() {
        this.children = this.childInput;
        this.numberOfChildren = this.children.length;
        this.oldNumberOfChildren = this.children.length;
       this.children.forEach((child) => {
            this.childrenTransit[this.children.indexOf(child)] = this.formateDate(child.BirthDate);
        });
        this.dateMask = this.masksService.getDatetMask();
    }

    public changeChild(val: number) {
        this.numberOfChildren = val;
        var childChange = this.numberOfChildren - this.oldNumberOfChildren;
        if (childChange > 0) {
            var index = this.oldNumberOfChildren;
            while (index < this.numberOfChildren) {
                this.children.push(new ClientPerson());
                this.childrenTransit.push('');
                index++;
            }
        }
        if (childChange < 0) {
            this.children.splice(this.numberOfChildren, -childChange);
            this.childrenTransit.splice(this.numberOfChildren, -childChange);


        }
        this.oldNumberOfChildren = this.numberOfChildren;
        this.sendChanges();
    }

    public noChild(event: any) {
        this.numberOfChildren = 0;
        this.oldNumberOfChildren = 0;
        this.children = [];
        this.childrenTransit = [];
        this.sendChanges();
    }


    public removeChild(event: any, i: any) {
        if (event.target.value == '') {
            this.children[i] = new ClientPerson();
            this.sendChanges();
        }
    }

    public affectDateToChild(event: any, name: number) {
        if (event.target.value == '') {
            this.children[name] = new ClientPerson();
            this.sendChanges();
            return;
        }
        var splitDate = event.target.value.split("/");
        var goodDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
        if (Object.prototype.toString.call(goodDate) === "[object Date]" && !isNaN(goodDate.getTime())) {
            switch (name) {
                case 0:
                    this.children[0].BirthDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
                    break;
                case 1:
                    this.children[1].BirthDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
                    break;
                case 2:
                    this.children[2].BirthDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
                    break;
                case 3:
                    this.children[3].BirthDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
                    break;
            }
            this.sendChanges();
        }
        else {
            if (this.children[name].BirthDate != null) {

                var day = this.children[name].BirthDate.getDate().toString();
                if (this.children[name].BirthDate.getDate() < 10)
                    day = "0" + this.children[name].BirthDate.getDate().toString();
                var month = this.children[name].BirthDate.getMonth().toString();
                if (this.children[name].BirthDate.getMonth() < 10)
                    month = "0" + this.children[name].BirthDate.getMonth().toString();
                event.target.value = day + "/" + month + "/" + this.children[name].BirthDate.getFullYear();
            }
            this.showWarn("veuillez entrer une date valide");
            this.sendChanges();
            return;

        }

    }

    public showWarn(message: string) {
        this.msgs = [];
        this.msgs.push({ severity: 'warn', summary: 'Warn Message', detail: message });
    }

    formateDate(date: Date): string {
        var dateForForm: string;
        var day = date.getDate().toString();
        if (date.getDate() < 10)
            day = "0" + (date.getDate());
        var month = (date.getMonth() + 1).toString();
        if ((date.getMonth() + 1) < 10)
            month = "0" + (date.getMonth() + 1);
        dateForForm = day + "/" + month + "/" + date.getFullYear();
        return (dateForForm);
    }


    public sendChanges() {
        this.childOutput.emit(this.children);
    }
}