"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var ClientPerson_1 = require("../models/ClientPerson");
var core_1 = require("@angular/core");
var masks_service_1 = require("../services/masks.service");
var ChildrenHeplerComponent = (function () {
    function ChildrenHeplerComponent(masksService) {
        this.masksService = masksService;
        this.childOutput = new core_1.EventEmitter();
        this.childrenTransit = [];
        this.msgs = [];
        this.childrenTooltip = "Sont considérés comme « Enfants à charge » de l’Adhérent, les enfants qui bénéficient d’un régime d’assurance maladie obligatoire français du fait de l’affiliation de l’Adhérent (ou de son conjoint, à défaut de son partenaire de PACS ou de son concubin) ou d’une affiliation personnelle, et qui sont âgés de moins de 21 ans, ou qui sont âgés de moins de 26 ans et poursuivent leurs études et ne disposent pas de ressources propres provenant d’une activité salariée (sauf emplois occasionnels ou saisonniers ou d’appoint, durant leurs études), ou suivent une formation en alternance ou se trouvent en contrat d’apprentissage, ou sont inscrits à Pôle Emploi comme primo-demandeur d’emploi ou effectuent un stage préalable à un premier emploi rémunéré, ou, quel que soit leur âge, qui bénéficient d’une allocation prévue par la législation sociale en faveur des Handicapés en vigueur.";
    }
    ChildrenHeplerComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.children = this.childInput;
        this.numberOfChildren = this.children.length;
        this.oldNumberOfChildren = this.children.length;
        this.children.forEach(function (child) {
            _this.childrenTransit[_this.children.indexOf(child)] = _this.formateDate(child.BirthDate);
        });
        this.dateMask = this.masksService.getDatetMask();
    };
    ChildrenHeplerComponent.prototype.changeChild = function (val) {
        this.numberOfChildren = val;
        var childChange = this.numberOfChildren - this.oldNumberOfChildren;
        if (childChange > 0) {
            var index = this.oldNumberOfChildren;
            while (index < this.numberOfChildren) {
                this.children.push(new ClientPerson_1.ClientPerson());
                this.childrenTransit.push('');
                index++;
            }
        }
        if (childChange < 0) {
            this.children.splice(this.numberOfChildren, -childChange);
            this.childrenTransit.splice(this.numberOfChildren, -childChange);
        }
        this.oldNumberOfChildren = this.numberOfChildren;
        this.sendChanges();
    };
    ChildrenHeplerComponent.prototype.noChild = function (event) {
        this.numberOfChildren = 0;
        this.oldNumberOfChildren = 0;
        this.children = [];
        this.childrenTransit = [];
        this.sendChanges();
    };
    ChildrenHeplerComponent.prototype.removeChild = function (event, i) {
        if (event.target.value == '') {
            this.children[i] = new ClientPerson_1.ClientPerson();
            this.sendChanges();
        }
    };
    ChildrenHeplerComponent.prototype.affectDateToChild = function (event, name) {
        if (event.target.value == '') {
            this.children[name] = new ClientPerson_1.ClientPerson();
            this.sendChanges();
            return;
        }
        var splitDate = event.target.value.split("/");
        var goodDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
        if (Object.prototype.toString.call(goodDate) === "[object Date]" && !isNaN(goodDate.getTime())) {
            switch (name) {
                case 0:
                    this.children[0].BirthDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
                    break;
                case 1:
                    this.children[1].BirthDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
                    break;
                case 2:
                    this.children[2].BirthDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
                    break;
                case 3:
                    this.children[3].BirthDate = new Date(splitDate[1] + "/" + splitDate[0] + "/" + splitDate[2]);
                    break;
            }
            this.sendChanges();
        }
        else {
            if (this.children[name].BirthDate != null) {
                var day = this.children[name].BirthDate.getDate().toString();
                if (this.children[name].BirthDate.getDate() < 10)
                    day = "0" + this.children[name].BirthDate.getDate().toString();
                var month = this.children[name].BirthDate.getMonth().toString();
                if (this.children[name].BirthDate.getMonth() < 10)
                    month = "0" + this.children[name].BirthDate.getMonth().toString();
                event.target.value = day + "/" + month + "/" + this.children[name].BirthDate.getFullYear();
            }
            this.showWarn("veuillez entrer une date valide");
            this.sendChanges();
            return;
        }
    };
    ChildrenHeplerComponent.prototype.showWarn = function (message) {
        this.msgs = [];
        this.msgs.push({ severity: 'warn', summary: 'Warn Message', detail: message });
    };
    ChildrenHeplerComponent.prototype.formateDate = function (date) {
        var dateForForm;
        var day = date.getDate().toString();
        if (date.getDate() < 10)
            day = "0" + (date.getDate());
        var month = (date.getMonth() + 1).toString();
        if ((date.getMonth() + 1) < 10)
            month = "0" + (date.getMonth() + 1);
        dateForForm = day + "/" + month + "/" + date.getFullYear();
        return (dateForForm);
    };
    ChildrenHeplerComponent.prototype.sendChanges = function () {
        this.childOutput.emit(this.children);
    };
    return ChildrenHeplerComponent;
}());
ChildrenHeplerComponent = __decorate([
    core_1.Component({
        selector: "children-helper",
        templateUrl: "/app/inputs/children-helper.component.html",
        inputs: ['childInput', 'returnChildBirthDates'],
        outputs: ['childOutput']
    }),
    __metadata("design:paramtypes", [masks_service_1.MasksService])
], ChildrenHeplerComponent);
exports.ChildrenHeplerComponent = ChildrenHeplerComponent;
//# sourceMappingURL=children-helper.component.js.map