﻿import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { ExtranetSubscription } from "../models/ExtranetSubscription";

@Component({
    selector: 'payment',
    templateUrl: '/app/inputs/payment.component.html',
    inputs: ['model', 'inputList']
})

//INPUT VARIABLES
//C --> Chèque
//P --> Prélèvement
//V --> Virement
//Division --> Mensuel
//         --> Trimestriel
//         --> Semestriel
//         --> Annuel
//AccountData --> Iban & Bic

export class PaymentComponent implements OnInit {
    model: ExtranetSubscription;
    @Output() modelChange = new EventEmitter();
    inputList: string[];

    ibanMask: string;
    bicMask: string;

    ngOnInit() {

    }

    onDataChange() {
        this.modelChange.emit(this.model);
    }

    changePaymentMethod(paymentMethod: any) {
        this.model.PaymentMethod = paymentMethod;
        this.onDataChange();
    }

    changeClientIban(event: any) {
        this.model.Client.BankAccountData.Iban = event;
        this.onDataChange();
    }

    changeClientPersonIban(event: any) {
        this.model.ClientPerson.BankAccountData.Iban = event;
        this.onDataChange();
    }
    
    changeClientBic(event: any) {
        this.model.Client.BankAccountData.Bic = event;
        this.onDataChange();
    }

    changeClientPersonBic(event: any) {
        this.model.ClientPerson.BankAccountData.Bic = event;
        this.onDataChange();
    }

    updateDivision(value: string) {
        this.model.Division = value;
        this.onDataChange();
    }

    
    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }

    radioCheat(value: any, elem: any) {
        if (elem === value) {
            return true;
        } else {
            return false;
        }
    }

}