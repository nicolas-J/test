"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var PaymentComponent = (function () {
    function PaymentComponent() {
        this.modelChange = new core_1.EventEmitter();
    }
    PaymentComponent.prototype.ngOnInit = function () {
    };
    PaymentComponent.prototype.onDataChange = function () {
        this.modelChange.emit(this.model);
    };
    PaymentComponent.prototype.changePaymentMethod = function (paymentMethod) {
        this.model.PaymentMethod = paymentMethod;
        this.onDataChange();
    };
    PaymentComponent.prototype.changeClientIban = function (event) {
        this.model.Client.BankAccountData.Iban = event;
        this.onDataChange();
    };
    PaymentComponent.prototype.changeClientPersonIban = function (event) {
        this.model.ClientPerson.BankAccountData.Iban = event;
        this.onDataChange();
    };
    PaymentComponent.prototype.changeClientBic = function (event) {
        this.model.Client.BankAccountData.Bic = event;
        this.onDataChange();
    };
    PaymentComponent.prototype.changeClientPersonBic = function (event) {
        this.model.ClientPerson.BankAccountData.Bic = event;
        this.onDataChange();
    };
    PaymentComponent.prototype.updateDivision = function (value) {
        this.model.Division = value;
        this.onDataChange();
    };
    PaymentComponent.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    PaymentComponent.prototype.radioCheat = function (value, elem) {
        if (elem === value) {
            return true;
        }
        else {
            return false;
        }
    };
    return PaymentComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], PaymentComponent.prototype, "modelChange", void 0);
PaymentComponent = __decorate([
    core_1.Component({
        selector: 'payment',
        templateUrl: '/app/inputs/payment.component.html',
        inputs: ['model', 'inputList']
    })
    //INPUT VARIABLES
    //C --> Chèque
    //P --> Prélèvement
    //V --> Virement
    //Division --> Mensuel
    //         --> Trimestriel
    //         --> Semestriel
    //         --> Annuel
    //AccountData --> Iban & Bic
], PaymentComponent);
exports.PaymentComponent = PaymentComponent;
//# sourceMappingURL=payment.component.js.map