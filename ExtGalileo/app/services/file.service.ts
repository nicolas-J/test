﻿import { Injectable } from '@angular/core';
import { Headers, Http, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/share';
import 'rxjs/add/operator/toPromise';
import { AppService } from "./app.service";

@Injectable()
export class FileService {

    public url: string = "";
    public fileName: string = "";
    public serverResponse: any = "";
    public serverError: any = "";
    public model: any;

    private progressObserver = new BehaviorSubject<number>(0);
    public progress$ = this.progressObserver.asObservable();

    constructor(private http: Http, private appService: AppService) {
        this.progress$ = new Observable<number>((observer: any) => {
            this.progressObserver = observer;
        });
    }

    changeProgress(number: any) {
        this.progressObserver.next(number);
    }

    public getObserver(): Observable<number> {
        return this.progress$;
    }

    resetService() {
        this.url = "";
        this.fileName = "";
        this.serverResponse = "";
        this.serverError = "";
    }

    private b64ToBlob(b64Data: any, contentType = '', sliceSize = 512) {
        var byteCharacters = atob(b64Data);
        var byteArrays: any[] = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    }

    uploadFile(file: File, fileName: string, model: any): Promise<any> {
        return new Promise((resolve, reject) => {
            let xhr: XMLHttpRequest = new XMLHttpRequest();

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        setTimeout(function () {
                            this.changeProgress(0);
                        }.bind(this), 2000);
                        resolve(<any>JSON.parse(xhr.response));
                        var response = JSON.parse(xhr.response);
                        if (response[0] === "Error") {
                            this.serverError = response[1];
                        } else {
                            this.serverResponse = response[1];
                        }
                    } else {
                        reject(xhr.response);
                    }
                }
            };

            xhr.upload.onprogress = (event) => {
                this.changeProgress(Math.round(event.loaded / event.total * 100));
            };

            xhr.open('POST', this.url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            let formData = new FormData();
            formData.append("file", file);
            formData.append("fileName", fileName);
            formData.append("model", JSON.stringify(model));
            xhr.send(formData);
        });
    }

    uploadExcel(file: File): Promise<any> {
        $(".loading").show();
        return new Promise((resolve, reject) => {
            let xhr: XMLHttpRequest = new XMLHttpRequest();

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        setTimeout(function () {
                            this.changeProgress(0);
                        }.bind(this), 2000);
                        resolve(<any>JSON.parse(xhr.response));
                        var response = JSON.parse(xhr.response);
                        $(".loading").hide();
                        if (response[0] === "Error") {
                            this.serverError = response[1];
                        } else {
                            this.serverResponse = response[1];
                        }
                    } else {
                        reject(xhr.response);
                    }
                }
            };

            xhr.upload.onprogress = (event) => {
                this.changeProgress(Math.round(event.loaded / event.total * 100));
            };

            xhr.open('POST', this.url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            let formData = new FormData();
            formData.append("file", file);
            xhr.send(formData);
        });
    }

    readFile(fileName: string, model: any): Promise<any> {
        return new Promise((resolve, reject) => {
            let xhr: XMLHttpRequest = new XMLHttpRequest();

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(<any>JSON.parse(xhr.response));
                        var response = JSON.parse(xhr.response);
                        if (response[0] === "Error") {
                            this.serverError = response[1];
                        } else {
                            var blob = this.b64ToBlob(response[0], response[1]);
                            var blobUrl = URL.createObjectURL(blob);

                            var link = document.createElement("a");
                            //Download
                            if (window.navigator.msSaveOrOpenBlob) {
                                document.body.appendChild(link);
                                link.addEventListener('click', function fileClickHandler() {
                                    window.navigator.msSaveOrOpenBlob(blob, response[3] + response[2]);
                                }, false);
                            } else {
                                if (response[2] === ".pdf") {
                                    var win = window.open(blobUrl, '_blank');
                                }
                                link.setAttribute("download", response[3] + response[2]);
                                // Construct the uri
                                link.setAttribute("href", blobUrl);
                                document.body.appendChild(link);
                            }
                            link.click();
                            // Cleanup the DOM
                            document.body.removeChild(link);
                        }
                    } else {
                        reject(xhr.response);
                    }
                }
            };


            xhr.open('POST', this.url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            let formData = new FormData();
            formData.append("fileName", fileName);
            formData.append("model", JSON.stringify(model));
            xhr.send(formData);
        });
    }

    getFile(fileName: string, model: any): Promise<any> {
        return new Promise((resolve, reject) => {
            let xhr: XMLHttpRequest = new XMLHttpRequest();

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        
                        var response = JSON.parse(xhr.response);
                        if (response[0] === "Error") {
                            this.serverError = response[1];
                        } else {
                            var blob = this.b64ToBlob(response[0], response[1]);
                            var blobUrl = URL.createObjectURL(blob);
                            resolve(<any>blobUrl);
                        }
                    } else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open('POST', this.url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            let formData = new FormData();
            formData.append("fileName", fileName);
            formData.append("model", JSON.stringify(model));
            xhr.send(formData);
        });
    }

    getFileSample(url: string): Promise<any> {
        return new Promise((resolve, reject) => {
            let xhr: XMLHttpRequest = new XMLHttpRequest();

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        var response = JSON.parse(xhr.response);
                        if (response.Error) {
                            this.appService.setEmittedValue({ severity: "error", summary: "Erreur", detail: response.Error });
                        } else {
                            var blob = this.b64ToBlob(response[0], response[1]);
                            var blobUrl = URL.createObjectURL(blob);

                            var link = document.createElement("a");
                            //Download
                            if (window.navigator.msSaveOrOpenBlob) {
                                document.body.appendChild(link);
                                link.addEventListener('click', function fileClickHandler() {
                                    window.navigator.msSaveOrOpenBlob(blob, response[3] + response[2]);
                                }, false);
                            } else {
                                if (response[2] === ".pdf") {
                                    var win = window.open(blobUrl, '_blank');
                                }
                                link.setAttribute("download", response[3] + response[2]);
                                // Construct the uri
                                link.setAttribute("href", blobUrl);
                                document.body.appendChild(link);
                            }
                            link.click();
                            // Cleanup the DOM
                            document.body.removeChild(link);
                        }
                    } else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open('POST', url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            let formData = new FormData();
            xhr.send(formData);
        });
    }

    deleteFile(fileName: string, model: any): Promise<any> {
        return new Promise((resolve, reject) => {
            let xhr: XMLHttpRequest = new XMLHttpRequest();

            xhr.onreadystatechange = () => {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(<any>JSON.parse(xhr.response));
                        var response = JSON.parse(xhr.response);
                        if (response[0] === "Error") {
                            this.serverError = response[1];
                        } else {
                            this.serverResponse = response[1];
                            this.model = response[2];
                        }
                    } else {
                        reject(xhr.response);
                    }
                }
            };

            xhr.open('POST', this.url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            let formData = new FormData();
            formData.append("fileName", fileName);
            formData.append("model", JSON.stringify(model));
            xhr.send(formData);
        });
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}