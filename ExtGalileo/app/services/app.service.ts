﻿import { Injectable, Inject, Output, EventEmitter } from '@angular/core';
import { Headers, Http, JsonpModule, RequestOptions, Response, ResponseContentType } from '@angular/http';

import { ExtranetSubscription } from "../models/ExtranetSubscription";
import { AuthService } from "./auth.service";
import { Cookie } from "ng2-cookies";

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class AppService {
    roles: any;
    @Output() msg: EventEmitter<any> = new EventEmitter<any>();

    public headers: Headers;
    constructor(public http: Http, private authService: AuthService) {
        this.http = http;
        this.headers = new Headers();
        this.headers.append('Content-Type', 'application/json');
    }

    getDatas(link: string) {
        return this.http.post(link, { headers: this.headers }).map(this.extractData);
    }

    sendString(link: string, model: string) {
        return this.http.post(link, { model: model }, { headers: this.headers }).map(this.extractData);
    }

    sendDatas(link: string, model: any) {
        return this.http.post(link, { model: JSON.stringify(model) }, { headers: this.headers }).map(this.extractData);
    }

    sendUser(link: string, model: any, roles: string[]) {
        return this.http.post(link, { model: model, roles: roles }, { headers: this.headers }).map(this.extractData);
    }

    sendRole(link: string, name: string, id: string) {
        return this.http.post(link, { name: name, id: id }, { headers: this.headers }).map(this.extractData);
    }

    sendModel(link: string, model: any) {
        return this.http.post(link, { model: model }, { headers: this.headers }).map(this.extractData);
    }

    sendData(link: string, data: any) {
        return this.http.post(link, data, { headers: this.headers }).map(this.extractData);
    }

    findIndex(arraytosearch: any, key: any, valuetosearch: any) {
        for (var i = 0; i < arraytosearch.length; i++) {
            if (arraytosearch[i][key] == valuetosearch) {
                return i;
            }
        }
        return null;
    }

    sendExtranetSub(link: string, model: ExtranetSubscription) {
        return this.post(model, link);
    }

    saveProject(model: ExtranetSubscription, url: string): Promise<any> {
        return this.post(model, url);
    }

    saveToBufferZone(model: ExtranetSubscription, url: string): Promise<any> {
        return this.post(model, url);
    }

    private post(model: ExtranetSubscription, url): Promise<any> {
        let headers = new Headers({
            'Content-Type': 'application/json; charset=utf-8'
        });

        return this.http
            .post(url, JSON.stringify(model), { headers: headers })
            .toPromise()
            .then(res => res.json())
            .catch((resp: Response) => this.handleError(resp));
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    }

    isValidDateString(dateString: string): boolean {
        if (dateString == null || dateString == "")
            return false;
        var splitDate = dateString.split("/");
        if (splitDate[0].length != 2 || splitDate[1].length != 2 || splitDate[2].length != 4)
            return false;
        var testDate = Date.parse(splitDate[2] + "/" + parseInt(splitDate[1]) + "/" + splitDate[0]);
        if (isNaN(testDate) || parseInt(dateString.split("/")[1]) > 12)
            return false;
        return true;
    }

    dateToDateString(date: Date): string {
        date = new Date(date.toString());
        return ("0" + date.getDate()).slice(-2) + "/" + ("0" + (date.getMonth() + 1)).slice(-2) + "/" + date.getFullYear();
    }

    dateStringToDate(dateString: string): Date {
        if (!this.isValidDateString(dateString)) {
            return null;
        } else {
            var splitDate = dateString.split("/");
            return new Date(parseInt(splitDate[2]), parseInt(splitDate[1]) - 1, parseInt(splitDate[0]));
        }
    }

    private extractData(res: Response) {
        let body = res.json();
        return body || "null";
    }

    showMessage(type: string, detail: string) {
        if (type == "Erreur") {
            $("#warning").css("display", "block");
            $("#warning p").html(detail);
            setTimeout(function () {
                $("#warning").css("display", "none");
            }, 8000);
        } else if (type == "Info") {
            $("#info").css("display", "block");
            $("#info p").html(detail);
            setTimeout(function () {
                $("#info").css("display", "none");
            }, 8000);
        }
        else if (type == "Valide") {
            $("#valid").css("display", "block");
            $("#valid p").html(detail);
            setTimeout(function () {
                $("#success").css("display", "none");
            }, 8000);
        }
        else {
            $("#valid").css("display", "block");
            $("#valid p").html(detail);
            setTimeout(function () {
                $("#success").css("display", "none");
            }, 8000);
        }
    }

    hideMessage() {
        $("#warning").css("display", "none");
        $("#info").css("display", "none");
    }

    getCity(event: any) {
        let postCode = event.target.value;
        this.sendDatas("/Tools/GetCity/", postCode).subscribe(callBack => {
            return callBack;
        });
    }

    loader(data: boolean) {
        if (data) {
            $(".loader").css("display", "block");
        }
        else {
            $(".loader").css("display", "none");
        }
    }

    logoff(): Promise<string[]> {
        let url = "Account/LogOff";
        let headers = new Headers({ 'Content-Type': "application/json" });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(url, options)
            .toPromise()
            .then(response => response.json() as string[])
            .catch((resp: Response) => { resp });
    }

    getAge(dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    }

    getProductInfo(model: ExtranetSubscription) {
        return this.post(model, "/Projects/GetProductInfo/");
    }

    setEmittedValue(value: any) {
        this.msg.emit(value);
    }

    getEmittedValue() {
        return this.msg;
    }
}