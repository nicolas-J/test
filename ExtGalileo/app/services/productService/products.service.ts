﻿import { Injectable } from "@angular/core";
import { Headers, Http, Response, RequestOptions } from "@angular/http";

import { AxaServicePro } from "../../models/AxaServicePro";

import "rxjs/add/operator/toPromise";
import { ExtranetSubscription } from "../../models/ExtranetSubscription";

@Injectable()
export class ProductsService {

    constructor(private http: Http) {
    }

    private dateTimeReceiver(key: any, value: any) {
        if (typeof value === 'string') {
            if (isNaN(Date.parse(value))) {
                return value;
            } else {
                var splitedDateString = value.substring(0, 10).split("-");
                if (splitedDateString.length === 3)
                    return splitedDateString[0] + "/" + splitedDateString[1] + "/" + splitedDateString[2];
                else
                    return value;
            }
        }
        return value;
    }

    //Functions

    getProductModel(modelId: string): Promise<ExtranetSubscription> {
        let url = `Products/LoadExtranetSubscription/${modelId}`;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(url, "", options)
            .toPromise()
            .then(response => JSON.parse(response["_body"] as string, this.dateTimeReceiver) as ExtranetSubscription)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
