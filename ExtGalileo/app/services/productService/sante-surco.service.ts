﻿import { Injectable } from "@angular/core";
import { Headers, Http, Response, RequestOptions } from "@angular/http";

import "rxjs/add/operator/map";
import "rxjs/add/operator/toPromise";
import { ClientSanteSurco } from "../../models/clientSanteSurco";
import { ExtranetSubscription } from "../../models/ExtranetSubscription";


@Injectable()
export class SanteSurcoService {

    private url = "/api/Angular/";  // URL to web api

    constructor(private  http: Http) { }

    //Variables
    
    //Fonctions
    getPriceFromModel(model: ExtranetSubscription){
        let url = `${this.url}getsantesurcoprice`;
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(url , model, options)
            .toPromise()
            .then(response => response.json() as string)
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error("An error occurred", error);
        return Promise.reject(error.message || error);
    }
}
