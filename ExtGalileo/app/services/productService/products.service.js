"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var ProductsService = (function () {
    function ProductsService(http) {
        this.http = http;
    }
    ProductsService.prototype.dateTimeReceiver = function (key, value) {
        if (typeof value === 'string') {
            if (isNaN(Date.parse(value))) {
                return value;
            }
            else {
                var splitedDateString = value.substring(0, 10).split("-");
                if (splitedDateString.length === 3)
                    return splitedDateString[0] + "/" + splitedDateString[1] + "/" + splitedDateString[2];
                else
                    return value;
            }
        }
        return value;
    };
    //Functions
    ProductsService.prototype.getProductModel = function (modelId) {
        var _this = this;
        var url = "Products/LoadExtranetSubscription/" + modelId;
        var headers = new http_1.Headers({ 'Content-Type': 'application/json' });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(url, "", options)
            .toPromise()
            .then(function (response) { return JSON.parse(response["_body"], _this.dateTimeReceiver); })
            .catch(this.handleError);
    };
    ProductsService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return ProductsService;
}());
ProductsService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], ProductsService);
exports.ProductsService = ProductsService;
//# sourceMappingURL=products.service.js.map