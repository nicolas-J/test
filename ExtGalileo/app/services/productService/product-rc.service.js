"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var AxaServicePro_1 = require("../../models/AxaServicePro");
var ProductRcService = (function () {
    function ProductRcService() {
        this.url = "/api/Angular/"; // URL to web api
        this.products = new AxaServicePro_1.AxaServicePro();
    }
    //Fonctions
    ProductRcService.prototype.getFamilyActivity = function (id) {
        this.family = null;
        switch (id) {
            case "EducationAndFormation":
                this.family = this.products.EducationAndFormationModel;
                break;
            case "AdviceSociety":
                this.family = this.products.AdviceSocietyModel;
                break;
            case "ItTrade":
                this.family = this.products.ItTradeModel;
                break;
            case "ServiceToSociety":
                this.family = this.products.ServiceToSocietyModel;
                break;
            case "SerenityTrade":
                this.family = this.products.SerenityTradeModel;
                break;
            case "MarketingAndCommunication":
                this.family = this.products.MarketinAndCommunicationModel;
                break;
            case "PeopleCare":
                this.family = this.products.PeopleCareModel;
                break;
        }
        return this.family;
    };
    ProductRcService.prototype.handleError = function (error) {
        return Promise.reject(error.message || error);
    };
    return ProductRcService;
}());
ProductRcService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [])
], ProductRcService);
exports.ProductRcService = ProductRcService;
//# sourceMappingURL=product-rc.service.js.map