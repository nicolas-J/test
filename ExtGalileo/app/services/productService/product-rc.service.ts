﻿import { Observable } from "rxjs";
import { Injectable } from '@angular/core';

import { AxaServicePro } from "../../models/AxaServicePro";

@Injectable()
export class ProductRcService {
    
    private url = "/api/Angular/";  // URL to web api

    constructor() {
        this.products = new AxaServicePro();
    }
    
    //Variables
    products: any;
    family: any;

    //Fonctions
    getFamilyActivity(id: string): Observable<AxaServicePro> {
        this.family = null;
        switch(id) {
            case "EducationAndFormation":
                this.family =  this.products.EducationAndFormationModel;
                break;
            case "AdviceSociety":
                this.family = this.products.AdviceSocietyModel;
                break;
            case "ItTrade":
                this.family = this.products.ItTradeModel;
                break;
            case "ServiceToSociety":
                this.family = this.products.ServiceToSocietyModel;
                break;
            case "SerenityTrade":
                this.family = this.products.SerenityTradeModel;
                break;
            case "MarketingAndCommunication":
                this.family = this.products.MarketinAndCommunicationModel;
                break;
            case "PeopleCare":
                this.family = this.products.PeopleCareModel;
                break;
        }
        return this.family;
    }
    
    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}
