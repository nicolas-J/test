﻿import { Injectable } from '@angular/core';

@Injectable()
export class MasksService {

    //Fonctions
    getPhoneMask(): (string | RegExp)[] {
        var mask: (string | RegExp)[] = [/\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/] ;
        return (mask);
    }

    getIbanMask(): (string | RegExp)[] {
        var mask: (string | RegExp)[] = [/[a-zA-Z]/, /[a-zA-Z]/, /\d/, /\d/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, ];
        return (mask);
    }

    getBicMask(): (string | RegExp)[] {
        var mask: (string | RegExp)[] = [/[a-zA-Z]/, /[a-zA-Z]/, /[a-zA-Z]/, /[a-zA-Z]/, ' ', /[a-zA-Z]/, /[a-zA-Z]/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/];
        return (mask);
    }

    getPeriodeMask(): (string | RegExp)[] {
        var mask: (string | RegExp)[] = [/\d/, /\d/];
        return (mask);
    }

    getSiretMask(): (string | RegExp)[] {
        var mask: (string | RegExp)[] = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
        return (mask);
    }

    getSecuMask(): (string | RegExp)[] {
        var mask: (string | RegExp)[] = [/\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/];
        return (mask);
    }

    getDatetMask(): (string | RegExp)[] {
        var mask: (string | RegExp)[] = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
        return (mask);
    }

    getNafMask(): (string | RegExp)[] {
        var mask: (string | RegExp)[] = [/\d/, /\d/, /\d/, /\d/, /[a-zA-Z]/];
        return (mask);
    }

    getPostCodeMask(): (string | RegExp)[] {
        var mask: (string | RegExp)[] = [/\d/, /\d/, /\d/, /\d/, /\d/];
        return (mask);
    }


    getShortDateMask(): (string | RegExp)[] {
        var mask: (string | RegExp)[] = [/\d/, /\d/, '/', /\d/, /\d/];
        return (mask);
    }

    getEmployeesMask(): (string | RegExp)[] {
        var mask: (string | RegExp)[] = [/\d/, /\d/, /\d/];
        return (mask);
    }

    getBirthDepartmentMask(): (string | RegExp)[] {
        var mask: (string | RegExp)[] = [/\d/, /\d/, /\d/];
        return (mask);
    }

    private handleError(error: any): Promise<any> {
        console.error("An error occurred", error);
        return Promise.reject(error.message || error);
    }
}
