﻿import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService } from './auth.service';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Injectable()
export class AuthGuardService implements CanActivate {

    constructor(private authService: AuthService, private router: Router) { } //login
    roles = new BehaviorSubject<Array<string>>(new Array<string>());
    roles$ = this.roles.asObservable();

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        let url: string = state.url;
        this.authService.getUserRoles().then(data => {
            this.roles.next(data);
        });
        let isLog = this.authService.getToken();
        if (isLog != null) {
            return true;
        }
        return false;
    }
}