"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var auth_service_1 = require("./auth.service");
require("rxjs/add/operator/map");
require("rxjs/add/operator/toPromise");
var AppService = (function () {
    function AppService(http, authService) {
        this.http = http;
        this.authService = authService;
        this.msg = new core_1.EventEmitter();
        this.http = http;
        this.headers = new http_1.Headers();
        this.headers.append('Content-Type', 'application/json');
    }
    AppService.prototype.getDatas = function (link) {
        return this.http.post(link, { headers: this.headers }).map(this.extractData);
    };
    AppService.prototype.sendString = function (link, model) {
        return this.http.post(link, { model: model }, { headers: this.headers }).map(this.extractData);
    };
    AppService.prototype.sendDatas = function (link, model) {
        return this.http.post(link, { model: JSON.stringify(model) }, { headers: this.headers }).map(this.extractData);
    };
    AppService.prototype.sendUser = function (link, model, roles) {
        return this.http.post(link, { model: model, roles: roles }, { headers: this.headers }).map(this.extractData);
    };
    AppService.prototype.sendRole = function (link, name, id) {
        return this.http.post(link, { name: name, id: id }, { headers: this.headers }).map(this.extractData);
    };
    AppService.prototype.sendModel = function (link, model) {
        return this.http.post(link, { model: model }, { headers: this.headers }).map(this.extractData);
    };
    AppService.prototype.sendData = function (link, data) {
        return this.http.post(link, data, { headers: this.headers }).map(this.extractData);
    };
    AppService.prototype.findIndex = function (arraytosearch, key, valuetosearch) {
        for (var i = 0; i < arraytosearch.length; i++) {
            if (arraytosearch[i][key] == valuetosearch) {
                return i;
            }
        }
        return null;
    };
    AppService.prototype.sendExtranetSub = function (link, model) {
        return this.post(model, link);
    };
    AppService.prototype.saveProject = function (model, url) {
        return this.post(model, url);
    };
    AppService.prototype.saveToBufferZone = function (model, url) {
        return this.post(model, url);
    };
    AppService.prototype.post = function (model, url) {
        var _this = this;
        var headers = new http_1.Headers({
            'Content-Type': 'application/json; charset=utf-8'
        });
        return this.http
            .post(url, JSON.stringify(model), { headers: headers })
            .toPromise()
            .then(function (res) { return res.json(); })
            .catch(function (resp) { return _this.handleError(resp); });
    };
    AppService.prototype.handleError = function (error) {
        console.error('An error occured', error);
        return Promise.reject(error.message || error);
    };
    AppService.prototype.isValidDateString = function (dateString) {
        if (dateString == null || dateString == "")
            return false;
        var splitDate = dateString.split("/");
        if (splitDate[0].length != 2 || splitDate[1].length != 2 || splitDate[2].length != 4)
            return false;
        var testDate = Date.parse(splitDate[2] + "/" + parseInt(splitDate[1]) + "/" + splitDate[0]);
        if (isNaN(testDate) || parseInt(dateString.split("/")[1]) > 12)
            return false;
        return true;
    };
    AppService.prototype.dateToDateString = function (date) {
        date = new Date(date.toString());
        return ("0" + date.getDate()).slice(-2) + "/" + ("0" + (date.getMonth() + 1)).slice(-2) + "/" + date.getFullYear();
    };
    AppService.prototype.dateStringToDate = function (dateString) {
        if (!this.isValidDateString(dateString)) {
            return null;
        }
        else {
            var splitDate = dateString.split("/");
            return new Date(parseInt(splitDate[2]), parseInt(splitDate[1]) - 1, parseInt(splitDate[0]));
        }
    };
    AppService.prototype.extractData = function (res) {
        var body = res.json();
        return body || "null";
    };
    AppService.prototype.showMessage = function (type, detail) {
        if (type == "Erreur") {
            $("#warning").css("display", "block");
            $("#warning p").html(detail);
            setTimeout(function () {
                $("#warning").css("display", "none");
            }, 8000);
        }
        else if (type == "Info") {
            $("#info").css("display", "block");
            $("#info p").html(detail);
            setTimeout(function () {
                $("#info").css("display", "none");
            }, 8000);
        }
        else if (type == "Valide") {
            $("#valid").css("display", "block");
            $("#valid p").html(detail);
            setTimeout(function () {
                $("#success").css("display", "none");
            }, 8000);
        }
        else {
            $("#valid").css("display", "block");
            $("#valid p").html(detail);
            setTimeout(function () {
                $("#success").css("display", "none");
            }, 8000);
        }
    };
    AppService.prototype.hideMessage = function () {
        $("#warning").css("display", "none");
        $("#info").css("display", "none");
    };
    AppService.prototype.getCity = function (event) {
        var postCode = event.target.value;
        this.sendDatas("/Tools/GetCity/", postCode).subscribe(function (callBack) {
            return callBack;
        });
    };
    AppService.prototype.loader = function (data) {
        if (data) {
            $(".loader").css("display", "block");
        }
        else {
            $(".loader").css("display", "none");
        }
    };
    AppService.prototype.logoff = function () {
        var url = "Account/LogOff";
        var headers = new http_1.Headers({ 'Content-Type': "application/json" });
        var options = new http_1.RequestOptions({ headers: headers });
        return this.http.post(url, options)
            .toPromise()
            .then(function (response) { return response.json(); })
            .catch(function (resp) { resp; });
    };
    AppService.prototype.getAge = function (dateString) {
        var today = new Date();
        var birthDate = new Date(dateString);
        var age = today.getFullYear() - birthDate.getFullYear();
        var m = today.getMonth() - birthDate.getMonth();
        if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
            age--;
        }
        return age;
    };
    AppService.prototype.getProductInfo = function (model) {
        return this.post(model, "/Projects/GetProductInfo/");
    };
    AppService.prototype.setEmittedValue = function (value) {
        this.msg.emit(value);
    };
    AppService.prototype.getEmittedValue = function () {
        return this.msg;
    };
    return AppService;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], AppService.prototype, "msg", void 0);
AppService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, auth_service_1.AuthService])
], AppService);
exports.AppService = AppService;
//# sourceMappingURL=app.service.js.map