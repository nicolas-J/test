﻿import { Injectable, Output, EventEmitter } from '@angular/core';
import { Headers, Http, Response } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'


@Injectable()
export class AuthService {
    public token: string;
    redirectUrl: string;

    constructor(private http: Http) {
        // set token if saved in local storage
        var currentUser = localStorage.getItem('currentUser');
        this.token = currentUser;
    }

    login(model: any): Observable<any> {
        return this.http.post('/Account/Login', model)
            .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let res = JSON.parse(response["_body"]);
                if (res[0] == "success") {
                    // set token property
                    this.token = res[1];
                    //set success message
                    var msg = { severity: "success", summary: "Succès", detail: "Bienvenue, sur Galiléo!" };
                    // store username and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', model.Email);
                    // return true to indicate successful login
                    return msg;
                } else {
                    this.token = null;
                    localStorage.removeItem('currentUser');
                    return res;
                }
            });
    }

    getToken() {
        return this.token;
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }

    getUserRoles(): Promise<Array<string>> {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json');
        return this.http
            .get("/Api/authapi/getuserroles", { headers: headers })
            .toPromise()
            .then(response => JSON.parse(response['_body']) as Array<string>)
            .catch((resp: Response) => this.handleError(resp));
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    }
}