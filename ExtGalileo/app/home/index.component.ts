﻿import { Component, OnInit } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Routes, RouterModule } from '@angular/router';
import { slideToLeft } from '../tools/router.animations';

@Component({
    selector: 'aw-index',
    templateUrl: '/app/home/index.component.html',
    animations: [slideToLeft()],
    host: { '[@slideToLeft]': '' }
})

export class IndexComponent {

    constructor(private router: Router, private http: Http, private route: ActivatedRoute) {
    }

    ngOnInit() {

    }
}