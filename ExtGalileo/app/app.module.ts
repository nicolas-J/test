//
import { Component, NgModule, EventEmitter, LOCALE_ID } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpModule, JsonpModule, Http } from '@angular/http';
import { RouterModule } from '@angular/router';
import { routing } from './app.routing';

//Services
import { AuthService } from './services/auth.service';
import { MasksService } from './services/masks.service';
import { AuthGuardService } from './services/auth-guard.service';
import { AppService } from './services/app.service';
import { SanteSurcoService } from './services/productService/sante-surco.service';
import { ProductRcService } from './services/productService/product-rc.service';
import { ProductsService } from './services/productService/products.service';

//Main
import { AppComponent } from './app.component';
import { NavComponent } from './items/nav.component';
import { HeaderComponent } from './items/header.component';
import { FooterComponent } from './items/footer.component';
import { MessageComponent } from './items/message.component';

//Home 
import { IndexComponent } from './home/index.component';

//Acccount
import { AccountComponent } from './account/account.component';
import { LoginComponent } from './account/login.component';
import { RegisterComponent } from './account/register.component';
import { ForgotPasswordComponent } from './account/forgotPassword.component';
import { ResetPasswordComponent } from './account/resetPassword.component';

//Dashboard
import { DashboardComponent } from './dashboard/dashboard.component';
import { TarificationComponent } from './dashboard/tarification.component';
import { ProjectsComponent } from './dashboard/projects.component';
import { DocumentsComponent } from './dashboard/documents.component';
import { PortefeuilleComponent } from './dashboard/portefeuille.component';

//Input
//import { CityPostCodeComponent } from './input/cityPostCode.component';

//Popups
import { LoginSurveyComponent } from './popup/loginSurvey.component';

//Attribute Directives
import { MinValidatorDirective } from './shared/minValidator.directive';
import { PostCodeValidatorDirective } from './shared/postCodeValidator.directive';
import { YesNoValidatorDirective } from './shared/yesNoValidator.directive';
import { MaxRevenueValidatorDirective } from './shared/maxRevenueValidator.directive';


//IMPORT EXTERNAL LIB
import { ListboxModule, DataTableModule, GalleriaModule, SharedModule, DialogModule, MultiSelectModule, ConfirmDialogModule, ConfirmationService, SelectButtonModule, MessagesModule, FileUploadModule, ChartModule, DropdownModule, RadioButtonModule, GrowlModule, CheckboxModule, SliderModule, InputSwitchModule, TooltipModule, PasswordModule } from 'primeng/primeng';
import { TextMaskModule } from 'angular2-text-mask';
import { CarouselModule } from 'ng2-bootstrap';
import { ModalModule } from 'ng2-modal';
import { SelectModule } from 'ng2-select';
import { DataTablesModule } from 'angular-datatables';

//Manage
import { CollaboratorDetailComponent } from './manage/collaboratorDetail.component';

import { OnlinePaymentComponent } from './onlinePayment/onlinePayment.component';
import { PaymentCallbackComponent } from './onlinePayment/paymentCallback.component';
import { PaymentListComponent } from './onlinePayment/payment-list.component';


//Products
import { TestModuleComponent } from './test/testModule.component';
import { ProductRcComponent } from './products/rc-autoentrepreneur/product-rc.component';
import { RcProComponent } from './products/rc-pro/rc-pro.component';
import { RcDeclaration } from './products/rc-autoentrepreneur/rc-declaration.component';
import { ClientCompanyHelper } from './inputs/client-company-helper.component';
import { ClientPersonHelper } from './inputs/client-person-helper.component';
import { DocumentsHelper } from './inputs/documents-helper.component';
import { EffectDateHelper } from './inputs/effect-date-helper.component';
import { SpouseInfoHelper } from './inputs/spouse-info-helper.component';
import { BridgeHelper } from './inputs/bridge-helper.component';
import { ChildrenHeplerComponent } from './inputs/children-helper.component';
import { ChildInfoComponent } from './inputs/child-info-helper.component';
import { IbanComponent } from './inputs/iban.component';
import { BicComponent } from './inputs/bic.component';
import { PaymentComponent } from './inputs/payment.component';
import { PostCodeAndCityComponent } from './inputs/post-code-and-city.component';
import { CreationDateHelper } from './inputs/creation-date-helper.component';
import { SanteIndividuelComponent } from './products/sante-individuel/santeIndividuel.component';
import { FileService } from "./services/file.service";


@NgModule({
    imports: [
        BrowserModule,
        DataTablesModule,
        BrowserAnimationsModule,
        routing,
        FormsModule,
        DataTableModule,
        SharedModule,
        DialogModule,
        SelectButtonModule,
        MultiSelectModule,
        ConfirmDialogModule,
        MessagesModule,
        FileUploadModule,
        ChartModule,
        TextMaskModule,
        RouterModule,
        CarouselModule,
        GalleriaModule,
        SelectModule,
        ModalModule,
        PasswordModule,
        ListboxModule,
        DropdownModule,
        RadioButtonModule,
        GrowlModule,
        CheckboxModule,
        SliderModule,
        InputSwitchModule,
        TooltipModule
    ],
    declarations: [
        AppComponent,
        HeaderComponent,
        NavComponent,
        FooterComponent,
        MessageComponent,
        AccountComponent,
        IndexComponent,
        RegisterComponent,
        LoginComponent,
        ForgotPasswordComponent,
        ResetPasswordComponent,
        DashboardComponent,
        ProjectsComponent,
        LoginSurveyComponent,
        DocumentsComponent,
        PortefeuilleComponent,
        TarificationComponent,
        CollaboratorDetailComponent,
        SanteIndividuelComponent,
        ProductRcComponent,
        RcProComponent,
        DocumentsHelper,
        EffectDateHelper,
        SpouseInfoHelper,
        BridgeHelper,
        ChildrenHeplerComponent,
        ChildInfoComponent,
        ClientCompanyHelper,
        ClientPersonHelper,
        RcDeclaration,
        IbanComponent,
        BicComponent,
        PostCodeAndCityComponent,
        OnlinePaymentComponent,
        PaymentCallbackComponent,
        PaymentComponent,
        PaymentListComponent,
        CreationDateHelper,
        TestModuleComponent,
        MinValidatorDirective,
        PostCodeValidatorDirective,
        YesNoValidatorDirective,
        MaxRevenueValidatorDirective
    ],
    exports: [
        BrowserModule,
        HttpModule,
    ],
    providers: [
        Title,
        AuthService,
        AuthGuardService,
        AppService,
        FileService,
        MasksService,
        ProductRcService,
        ProductsService,
        SanteSurcoService,
        ConfirmationService,
        {
            provide: LOCALE_ID,
            useValue: navigator.language
        }
    ],
    bootstrap: [AppComponent],
})

export class AppModule { }
