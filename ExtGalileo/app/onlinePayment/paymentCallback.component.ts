﻿import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { slideToLeft } from '../tools/router.animations';

import { AppService } from '../services/app.service';

@Component({
    selector: 'aw-payment',
    templateUrl: '/app/onlinePayment/paymentCallback.component.html',
    providers: [AppService],
    animations: [slideToLeft()],
    host: { '[@slideToLeft]': '' }
})

export class PaymentCallbackComponent {



    constructor(private router: Router, private appService: AppService, private route: ActivatedRoute) {

    }

    ngOnInit() {

        //Check transaction status
        this.route.queryParams.subscribe(params => {
            if (params["id"] && params["transactionReference"]) {
                console.log(params["id"]);
                console.log(params["transactionReference"]);
            }
        });

        this.route.params.subscribe(params => {
            //Retrieve subscriptiondata from database
            console.log(params);
        });

    }
}