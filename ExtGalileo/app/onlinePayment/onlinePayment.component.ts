﻿import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { slideToLeft } from '../tools/router.animations';

import { AppService } from '../services/app.service';
import { MasksService } from '../services/masks.service';

@Component({
    selector: 'aw-payment',
    templateUrl: '/app/onlinePayment/onlinePayment.component.html',
    providers: [AppService],
    animations: [slideToLeft()],
    host: { '[@slideToLeft]': '' }
})

export class OnlinePaymentComponent {

    seal: string;
    amount: string;
    stringData: string;

    constructor(private maskService: MasksService, private appService: AppService) {
        
    }

    getSeal() {
        console.log("paymentLog");

        this.appService.sendString("/Payment/GetSeal", this.amount).subscribe(callback => {
            this.seal = callback.Seal;
            this.stringData = callback.StringData;
        });
    }
}