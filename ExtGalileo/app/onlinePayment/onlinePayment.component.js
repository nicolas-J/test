"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_animations_1 = require("../tools/router.animations");
var app_service_1 = require("../services/app.service");
var masks_service_1 = require("../services/masks.service");
var OnlinePaymentComponent = (function () {
    function OnlinePaymentComponent(maskService, appService) {
        this.maskService = maskService;
        this.appService = appService;
    }
    OnlinePaymentComponent.prototype.getSeal = function () {
        var _this = this;
        console.log("paymentLog");
        this.appService.sendString("/Payment/GetSeal", this.amount).subscribe(function (callback) {
            _this.seal = callback.Seal;
            _this.stringData = callback.StringData;
        });
    };
    return OnlinePaymentComponent;
}());
OnlinePaymentComponent = __decorate([
    core_1.Component({
        selector: 'aw-payment',
        templateUrl: '/app/onlinePayment/onlinePayment.component.html',
        providers: [app_service_1.AppService],
        animations: [router_animations_1.slideToLeft()],
        host: { '[@slideToLeft]': '' }
    }),
    __metadata("design:paramtypes", [masks_service_1.MasksService, app_service_1.AppService])
], OnlinePaymentComponent);
exports.OnlinePaymentComponent = OnlinePaymentComponent;
//# sourceMappingURL=onlinePayment.component.js.map