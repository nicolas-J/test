"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require("@angular/router");
var auth_service_1 = require("../services/auth.service");
var app_service_1 = require("../services/app.service");
var auth_guard_service_1 = require("../services/auth-guard.service");
var NavComponent = (function () {
    function NavComponent(titleService, router, appService, authService, authGuardService) {
        this.titleService = titleService;
        this.router = router;
        this.appService = appService;
        this.authService = authService;
        this.authGuardService = authGuardService;
        this.userRoles = [];
    }
    NavComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.authService.getUserRoles().then(function (data) { return _this.userRoles = data; });
        this.router.events.subscribe(function (event) {
            if (event instanceof router_1.NavigationEnd) {
                _this.authService.getUserRoles().then(function (data) {
                    _this.userRoles = data;
                    console.log(_this.userRoles);
                });
            }
            // NavigationEnd
            // NavigationCancel
            // NavigationError
            // RoutesRecognized
        });
    };
    NavComponent.prototype.logOut = function () {
        var _this = this;
        if (localStorage.getItem('currentUser'))
            this.appService.logoff().then(function (result) {
                _this.appService.setEmittedValue(result);
            });
        this.authService.logout();
        this.userRoles = [];
        var link = ['/Account/Login'];
        this.router.navigate(link);
    };
    return NavComponent;
}());
NavComponent = __decorate([
    core_1.Component({
        selector: 'nav-bar',
        templateUrl: '/app/items/nav.component.html'
    }),
    __metadata("design:paramtypes", [platform_browser_1.Title, router_1.Router, app_service_1.AppService, auth_service_1.AuthService, auth_guard_service_1.AuthGuardService])
], NavComponent);
exports.NavComponent = NavComponent;
//# sourceMappingURL=nav.component.js.map