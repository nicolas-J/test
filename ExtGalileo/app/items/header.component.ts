﻿import { Component, OnInit, Input, ViewChild, Output, EventEmitter, } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { Routes, RouterModule } from '@angular/router';

import { AppService } from '../services/app.service';

@Component({
    selector: 'header-bar',
    templateUrl: '/app/items/header.component.html',
    providers: [AppService]
})

export class HeaderComponent {
    loadPage: boolean = false;
    response: any;
    @Input() banner: string;

    constructor(private router: Router, public appService: AppService) {
    }

    //ngOnInit() {
    //    this.appService.getDatas('/Tools/GetUser/').subscribe(callback => {
    //        this.response = callback;
    //        this.loadPage = true;
    //    });
    //}
}

