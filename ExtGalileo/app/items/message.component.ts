﻿import { Component, Input } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { AppService } from '../services/app.service';
import { AuthGuardService } from '../services/auth-guard.service';

@Component({
    selector: 'message',
    templateUrl: '/app/items/message.component.html'
})

export class MessageComponent {

    msgs: any[] = [];

    public constructor(private titleService: Title, private router: Router, private appService: AppService, private authService: AuthService, private authGuardService: AuthGuardService) {
    }

    ngOnInit() {
        this.appService.getEmittedValue().subscribe(msg => {
            this.msgs.push(msg);
        });
    }
}