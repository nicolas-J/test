﻿import { Component, Input } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Router, NavigationStart, NavigationEnd } from '@angular/router';

import { AuthService } from '../services/auth.service';
import { AppService } from '../services/app.service';
import { AuthGuardService } from '../services/auth-guard.service';

@Component({
    selector: 'nav-bar',
    templateUrl: '/app/items/nav.component.html'
})

export class NavComponent {
    userRoles: Array<string> = [];

    public constructor(private titleService: Title, private router: Router, private appService: AppService, private authService: AuthService, private authGuardService: AuthGuardService) {
    }

    ngOnInit() {
        this.authService.getUserRoles().then(data => this.userRoles = data);

        this.router.events.subscribe(event => {
            if (event instanceof NavigationEnd) {
                this.authService.getUserRoles().then(data => {
                    this.userRoles = data;
                    console.log(this.userRoles);
                });
            }
            // NavigationEnd
            // NavigationCancel
            // NavigationError
            // RoutesRecognized
        });
    }

    logOut() {
        if (localStorage.getItem('currentUser'))
            this.appService.logoff().then(result => {
                this.appService.setEmittedValue(result);
            });

        this.authService.logout();
        this.userRoles = [];
        let link = ['/Account/Login'];
        this.router.navigate(link);
    }
}

