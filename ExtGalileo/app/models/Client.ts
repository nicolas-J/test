﻿
import { MailPhoneFax } from './MailPhoneFax';

export class Client{
    WinPassId: string;
    FileNumber: number;
    Contributor: string;
    Name: string;
    MailPhoneFax: MailPhoneFax;
    ContractsId: Array<string>;
    EventsId: Array<string>;
    AccountNb: number;
}