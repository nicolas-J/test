﻿import { Benefit } from './Benefit';


export class ProductLevel {
    KendoId: number;
    Name: string;
    Level: string;
    OriginalName: string;
    BaseCode: string;
    OptionCode: string;
    PortabilityBaseCode: string;
    PortabilityOptionCode: string;
    DisplayOrder: number;
    SelectedProduct: boolean;
    Price: number;
    SupPrice: number;
    AdditionalPrice: number;
    Insurance: string;
    Benefits: Array<Benefit>;
}

export class Product {
    Target: string;
    Family: string;
    Name: string;
    OriginalName: string;
    Branch: string;
    MinimumAge: number;
    MaximumAge: number;
    BrokerCommission: number;
    ContributorCommission: number;
    CommissionMin: number;
    CommissionMax: number;
    Levels: Array<ProductLevel>;
}