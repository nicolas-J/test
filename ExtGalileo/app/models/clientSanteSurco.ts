﻿export class ClientSanteSurco {
    personAge: number;
    spouseAge: number;
    childAge: number[];
    level: number;
    commission: number;
    constructor() {
        this.childAge = [];
        this.level = 1;
        this.commission = 0;
    }
}