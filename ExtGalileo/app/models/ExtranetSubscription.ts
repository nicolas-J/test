﻿import { PostalAddress } from './PostalAddress';
import { MailPhoneFax } from './MailPhoneFax';
import { ClientPerson } from './ClientPerson';
import { ClientCompany } from './ClientCompany';
import { ProductLevel } from './Product';
import { Product } from './Product';
import { RepositoryCollection } from './repository-collection';


export class ExtranetSubscription extends RepositoryCollection{

    //For bridge to assurware
    AssurwareContractId: string;
    AssurwareClientId: string;
    ContractNumber: string;
    EffectDate: Date;
    AffiliationDate: Date;
    CreationDate: Date;
    MaturityDate: Date;
    Client: ClientCompany;
    ClientPerson: ClientPerson;
    Product : Product;
    SupplementalHealthInsurance: boolean;
    SupplementalHealthInsurancePrice: number;
    Price: number;
    AccountHolder: string;
    DebitDay: string;
    OptionList: Array<boolean>;
    Periodicity: string;
    PaymentMethod: string;
    SubsidisedCompany: string;
    SubsidisedEmployee: string;
    PriceSubsidisedCompany: string;
    Contribution: string;
    PriceSubsidisedEmployee: string;
    Base: ProductLevel;
    Taxes: number;
    Option1: string;
    Option2: string;
    MadelinDeduction: number;
    Option1Price: number;
    Option2Price: number;
    IsBridged: boolean;

    Promotional: boolean;
    SendFrom: string;

    OptionMd: boolean;
    OptionAp: boolean;

    Commission: string;
    CommissionOption1: string;
    CommissionOption2: string;

    PriceUnique: string;
    PriceIsolated: string;
    PriceFamily: string;
    PriceAdult: string;
    PriceChild: string;
    PriceSpouse: number;
    PmssUnique: number;
    PmssIsolated: number;
    PmssFamily: number;
    PmssAdult: number;
    PmssChild: number;
    PmssSpouse: number;
    NumberOfPass: number;

    CcnBasePriceEmployee: number;
    CcnBasePriceConjoint: number;
    CcnBasePriceChild: number;
    CcnBasePriceFamily: number;

    CcnOption1PriceEmployee: number;
    CcnOption1PriceConjoint: number;
    CcnOption1PriceChild: number;
    CcnOption1PriceFamily: number;

    CcnOption2PriceEmployee: number;
    CcnOption2PriceConjoint: number;
    CcnOption2PriceChild: number;
    CcnOption2PriceFamily: number;

    CcnOption3PriceEmployee: number;
    CcnOption3PriceConjoint: number;
    CcnOption3PriceChild: number;
    CcnOption3PriceFamily: number;

    PmssAdultOption1: number;
    PmssAdultOption2: number;
    PmssChildOption1: number;
    PmssChildOption2: number;

    PriceAdultOption1: number;
    PriceAdultOption2: number;
    PriceChildOption1: number;
    PriceChildOption2: number;

    TeleTransmission: boolean;
    ApplicationFees: boolean;
    ApplicationFeesPrice: number;
    OnlineAccount: boolean;
    EmailAlert: boolean;

    UmanLife: boolean;
    PackSpvie: boolean;

    PdfLink: string;

    PdfSignId: string;
    PdfSignUrl: string;

    Statut: string;
    StatutTns: string;
    EditData: boolean;

    PdfDevis: boolean;
    PdfBae: boolean;
    PdfBia: boolean;
    PdfBenefit: boolean;
    PdfDevoirConseil: boolean;
    PdfNoticeInfo: boolean;
    PdfPlaquette: boolean;
    PdfSepa: boolean;
    PdfCg: boolean;
    PdfInsuranceProbability: boolean;
    PdfEmployeesState: boolean;
    PdfBeneficiaryDesignation: boolean;

    BrokerCode: number;
    BrokerMail: string;
    BrokerMailMasquerade: string;
    CommercialManagerCode: string;

    MadelinId: string;

    //Representative included fields
    IncludeRepresentative: boolean;
    RepresentativeOptionContribution: string;
    RepresentativeIncludedOptionPrice: number;

    //Checkbox if broker have validation to sell product in CCN
    SpvieValidation: boolean;

    Anonymous: boolean;
    //view that has called the method
    CallingView: string;
    //createMissingInputsForPrevoyance---INSURED
    Profession: string;
    ProfessionRiskClassAt: number;
    ProfessionRiskClassDc: number;
    ProfessionRiskClass: string;
    AnnualRevenue: number;
    SalesRevenue: number;
    FundPensionInsurance: string;
    CreatorOrBuyer: string;
    MultipleAssociatesSubscription: boolean;
    FiscalStatusMajorityManager: boolean;
    Dividends: number;
    EmployeesNumber: number;
    AverageAge: string;
    RecoveryOrCreationDate: Date;
    Division: string;
    ProfessionalSpending: number;

    //createMissingInputsForPrevoyance---SOCIETY
    DifferentSociety: boolean;
    CompaniesCommerceRegister: string;
    SocialReason: string;
    PostalAddress2: PostalAddress;
    LegalStatus: string;
    MailPhoneFax2: MailPhoneFax;

    //createMissingInputsForPrevoyance---COVERING
    DeathsAallCausesBool: boolean;
    DeathsAllCausesCapital: number;
    MadelinLaw: boolean;
    AccidentalDeathBool: boolean;
    AccidentalDeathCapital: string;
    EducationAnnuityBool: boolean;
    EducationAnnuityPension: number;
    EducationAnnuityChildNumber: number;
    SpousePensionBool: boolean;
    SpousePensionPension: number;
    InvalidityBool: boolean;
    InvalidityPension: number;
    IncapacityBool: boolean;
    IncapacityDaily: number;
    IncapacityContinuesFranchise: string;
    PremiumWaiverBool: boolean;
    GeneralChargesBool: boolean;
    GeneralChargesAmount: string;
    GeneralChargesCm3: string;
    GeneralChargesFranchise: string;
    DoubleEffectBool: boolean;
    PayrollTa: string;
    PayrollTb: string;
    PayrollTc: string;
    Body: string;
    Franchise: string;
    FranchisePurchase: boolean;

    //createMissingInputsForPrevoyance---SportAndTravel
    ExcludedSportPractice: boolean;
    ExcludedSportPracticeCover: boolean;
    AddSport: string;
    BusinessTravel: boolean;
    BusinessTravelNotEurope: boolean;
    BusinessTravelSuccessive: boolean;
    BusinessTravelInsurance: boolean;
    //Total First annual fee for pdf
    FirstAnnualFee: number;

    //Prev Corp Rates
    TauxTa: number;
    TauxTb: number;
    TauxTc: number;
    PrimeTa: number;
    PrimeTb: number;
    PrimeTc: number;
    Result: number;

    constructor() {
        super();
        this.Client = new ClientCompany();
        this.ClientPerson = new ClientPerson();
        this.Product = new Product();
        this.Base = new ProductLevel();
    }
}

export class ProductOptions{
    Prices: Array<PriceList>;
    OptionName: number;
    OptionCommission: number;
    OptionBool: boolean;
    Level: string;
}

export class PriceList{
    PriceName: string;
    Price: number;
}