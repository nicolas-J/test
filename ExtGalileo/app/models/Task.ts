﻿import { Event } from './Event';

export class Task extends Event {
    DoneBy: string;
    Automatic: boolean;
    DueDate: Date;
    TaskType: TaskTypes;
    StringTaskType: string;
    ContractId: string;
    ContractNumber: string;

}
export enum TaskTypes {
        Contribution,
    
        PaymentSchedule,
        
        DirectDebitSepa
}