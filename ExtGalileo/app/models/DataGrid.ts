﻿export class DataGrid {
    public Id: string;
    public CreationDate: string;
    public Hour: string;
    public Status: string;
    public BrokerCode: number;
    public BrokerMail: string;
    public Client: string;
    public Family: string;
    public Target: string;
    public Product: string;
    public Level: string;
    public Price: number;
    public CommercialManagerCode: string;
    public ProductionDate: string;
    public EffectDate: string;
    public ContractId: string;
    public ContractNumber: string;
    public ContractStatus: string;

    public MailStatus: boolean;
    public PostStatus: boolean;
    public PendingDay: number;
    public ProjectNumber: string;

    //PaymentInfo
    public TransactionDate: string;
    public TransactionReference: string;
    public ResponseCode: string;
    public MobilePhoneNumber: string;
    public EmailAddress: string;
}