"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Task = (function (_super) {
    __extends(Task, _super);
    function Task() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Task;
}(Event));
exports.Task = Task;
var TaskTypes;
(function (TaskTypes) {
    TaskTypes[TaskTypes["Contribution"] = 0] = "Contribution";
    TaskTypes[TaskTypes["PaymentSchedule"] = 1] = "PaymentSchedule";
    TaskTypes[TaskTypes["DirectDebitSepa"] = 2] = "DirectDebitSepa";
})(TaskTypes = exports.TaskTypes || (exports.TaskTypes = {}));
//# sourceMappingURL=Task.js.map