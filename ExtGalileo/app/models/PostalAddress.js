"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PostalAddress = (function () {
    function PostalAddress() {
    }
    PostalAddress.prototype.GetFullPostalAddress = function () {
        return (Number != null ? Number + " " : "") + (this.GetFullAdress() != null ? this.GetFullAdress() + " " : "") + (this.PostCode != null ? this.PostCode + " " : "") + (this.City != null ? this.City + " " : "") + (this.Country ? "" : "");
    };
    PostalAddress.prototype.GetFullAdress = function () {
        return this.Address + " " + this.Address2 + " " + this.Address3;
    };
    return PostalAddress;
}());
exports.PostalAddress = PostalAddress;
//# sourceMappingURL=PostalAddress.js.map