﻿export class AxaServicePro {
    EducationAndFormationModel: EducationAndFormation;
    AdviceSocietyModel: AdviceSociety;
    ItTradeModel: ItTrade;
    ServiceToSocietyModel: ServiceToSociety;
    SerenityTradeModel: SerenityTrade;
    MarketinAndCommunicationModel: MarketinAndCommunication;
    PeopleCareModel: PeopleCare;

    AutoEntrepreneurConditions: boolean;
    InfoNotice: boolean;
    Freelancer: boolean;
    RevenueUpper: boolean;
    DeclaredRevenue: number;
    FamilyActivity: string;
    TotalPremium: number ;
    CommissioningFee: number;
    Siret: string ;
    NafCode : string;
    IsAccepted: boolean ;
    Division : string;
    AnnualRate:number ;
    SixMonthRate:number ;
    EstimatedCommission : number;
    LegalStatut: string ;
    LegalForm: string;
    RecoveryOrCreationDate: Date;
    ActivityList: any;

    constructor() {
        this.EducationAndFormationModel = new EducationAndFormation();
        this.AdviceSocietyModel = new AdviceSociety();
        this.ItTradeModel = new ItTrade();
        this.ServiceToSocietyModel = new ServiceToSociety();
        this.SerenityTradeModel = new SerenityTrade();
        this.MarketinAndCommunicationModel = new MarketinAndCommunication();
        this.PeopleCareModel = new PeopleCare();
        this.Freelancer = false;
        this.RevenueUpper = false;
        this.DeclaredRevenue = 0;
        this.FamilyActivity = "";
        this.TotalPremium = 0;
        this.CommissioningFee = 0;
        this.ActivityList = [];
        this.Siret = "";
        this.NafCode = "";
        this.AutoEntrepreneurConditions = false;
        this.InfoNotice = false;
        this.IsAccepted = false;
        this.Division = "Mensuel";
        this.AnnualRate = 0;
        this.SixMonthRate = 0;
        this.EstimatedCommission = 0;
        this.LegalStatut = "PersonneMorale";
        this.LegalForm = "";
    }
}

export class EducationAndFormation {
    ActivityCareersAdviser: boolean;
    ActivityDistanceLearning: boolean;
    ActivityDogTrainer: boolean;
    ActivityEducator: boolean;
    ActivityExamProofreader: boolean;
    ActivityFirstAidTrainer: boolean;
    ActivityPrivateLessons: boolean;
    ActivitySpeaker: boolean;
    ActivitySupervisor: boolean;
    ActivityTeacher: boolean;
    ActivityTrainerSecurityFire: boolean;
    
    constructor() {
        this.ActivityCareersAdviser = false;
        this.ActivityDistanceLearning = false;
        this.ActivityDogTrainer = false;
        this.ActivityEducator = false;
        this.ActivityExamProofreader = false;
        this.ActivityFirstAidTrainer = false;
        this.ActivityPrivateLessons = false;
        this.ActivitySpeaker = false;
        this.ActivitySupervisor = false;
        this.ActivityTeacher = false;
        this.ActivityTrainerSecurityFire = false;
    }
}

export class AdviceSociety {
    ActivityAdviceHumanResources: boolean;
    ActivityProfessionalCoaching: boolean;
    ActivityAdviceStrategyOrganisation: boolean;
    ActivityManagementConsultant: boolean;
    ActivityAdviceOutsourcing: boolean;
    ActivityAdviceCorporateSocialResponsibility: boolean;
    ActivityAdviceQualityCertification: boolean;
    ActivityAdviceHealthAndSecurity: boolean;
    ActivityAdviceInformationSystem: boolean;
    ActivityAdviceTelecommunication: boolean;
    ActivityAdviceCertifiedPublicAccountant: boolean;
    ActivityAdviceAdministrativeManagement: boolean;
    ActivityAdviceMarketStudy: boolean;
    ActivityAdviceCostReduction: boolean;
    ActivityAdviceLogistics: boolean;
    ActivityAdviceImportExport: boolean;
    ActivityAdviceCommunication: boolean;
    ActivityAdviceDesignAndCreativity: boolean;
    ActivityAdviceMarketing: boolean;
    ActivityAdviceBrandSociety: boolean;
    ActivityAdviceAdvertisement: boolean;
    ActivityAdviceMedia: boolean;
    ActivityAdvicePressAgent: boolean;

    constructor() {
        this.ActivityAdviceHumanResources = false;
        this.ActivityProfessionalCoaching = false;
        this.ActivityAdviceStrategyOrganisation = false;
        this.ActivityManagementConsultant = false;
        this.ActivityAdviceOutsourcing = false;
        this.ActivityAdviceCorporateSocialResponsibility = false;
        this.ActivityAdviceQualityCertification = false;
        this.ActivityAdviceHealthAndSecurity = false;
        this.ActivityAdviceInformationSystem = false;
        this.ActivityAdviceTelecommunication = false;
        this.ActivityAdviceCertifiedPublicAccountant = false;
        this.ActivityAdviceAdministrativeManagement = false;
        this.ActivityAdviceMarketStudy = false;
        this.ActivityAdviceCostReduction = false;
        this.ActivityAdviceLogistics = false;
        this.ActivityAdviceImportExport = false;
        this.ActivityAdviceCommunication = false;
        this.ActivityAdviceDesignAndCreativity = false;
        this.ActivityAdviceMarketing = false;
        this.ActivityAdviceBrandSociety = false;
        this.ActivityAdviceAdvertisement = false;
        this.ActivityAdviceMedia = false;
        this.ActivityAdvicePressAgent = false;
    }
}

export class ItTrade {
    ActivityAdviceInformationSystemAndIntegration: boolean;
    ActivityPersonalTrainingIt: boolean;
    ActivityCreationWebSite: boolean;
    ActivityItAdvice: boolean;
    ActivityTechnicalAssistant: boolean;
    ActivityMakeEditSaleSoftware: boolean;
    ActivitySaleSoftwarePackage: boolean;
    ActivityDataProcessor: boolean;
    ActivityApplicationHosting: boolean;
    ActivitySalesComputerEquipment: boolean;
    ActivityMaintenanceAndTechnicalAssistant: boolean;
    ActivityManagementItStock: boolean;
    ActivityComputerGraphicsDesigner: boolean;
    ActivityAdvertisingAgency: boolean;

    constructor() {
        this.ActivityAdviceInformationSystemAndIntegration = false;
        this.ActivityPersonalTrainingIt = false;
        this.ActivityCreationWebSite = false;
        this.ActivityItAdvice = false;
        this.ActivityTechnicalAssistant = false;
        this.ActivityMakeEditSaleSoftware = false;
        this.ActivitySaleSoftwarePackage = false;
        this.ActivityDataProcessor = false;
        this.ActivityApplicationHosting = false;
        this.ActivitySalesComputerEquipment = false;
        this.ActivityMaintenanceAndTechnicalAssistant = false;
        this.ActivityManagementItStock = false;
        this.ActivityComputerGraphicsDesigner = false;
        this.ActivityAdvertisingAgency = false;
    }
}

export class ServiceToSociety {
    ActivityHeadHunter: boolean;
    ActivitySocietyTemporaryWork: boolean;
    ActivityOutsourcingAdministrativeTask: boolean;
    ActivityAgencyCreationOfSociety: boolean;
    ActivtyOutsourcingManagement: boolean;
    ActivityDevelopmentOfficeEquipment: boolean;
    ActivityKeepingAccountsBook: boolean;
    ActivityOutsourcingPayday: boolean;
    ActivityTranslationAndInterpreting: boolean;
    ActivityAdministrativeAssistant: boolean;
    ActivityReceptionAgent: boolean;
    ActivityPhotographer: boolean;
    ActivityStageDesigner: boolean;
    ActivityApartmentBuilding: boolean;
    ActivityHomeStaging: boolean;
    ActivityCleaner: boolean;

    constructor() {
        this.ActivityHeadHunter = false;
        this.ActivitySocietyTemporaryWork = false;
        this.ActivityOutsourcingAdministrativeTask = false;
        this.ActivityAgencyCreationOfSociety = false;
        this.ActivtyOutsourcingManagement = false;
        this.ActivityDevelopmentOfficeEquipment = false;
        this.ActivityKeepingAccountsBook = false;
        this.ActivityOutsourcingPayday = false;
        this.ActivityTranslationAndInterpreting = false;
        this.ActivityAdministrativeAssistant = false;
        this.ActivityReceptionAgent = false;
        this.ActivityPhotographer = false;
        this.ActivityStageDesigner = false;
        this.ActivityApartmentBuilding = false;
        this.ActivityHomeStaging = false;
        this.ActivityCleaner = false;
    }
}

export class SerenityTrade {
    ActivityFengShui: boolean;
    ActivityMusicTherapy: boolean;
    ActivityReflexology: boolean;
    ActivityCoachSports: boolean;
    ActivityErgonomist: boolean;
    ActivityBeautician: boolean;
    ActivityAdviceHealthDietary: boolean;
    ActivityMobileHairdresser: boolean;

    constructor() {
        this.ActivityFengShui = false;
        this.ActivityMusicTherapy = false;
        this.ActivityReflexology = false;
        this.ActivityCoachSports = false;
        this.ActivityErgonomist = false;
        this.ActivityBeautician = false;
        this.ActivityAdviceHealthDietary = false;
        this.ActivityMobileHairdresser = false;
    }
}

export class MarketinAndCommunication {
    ActivtyCallCenter: boolean;
    ActivityConceptionMarketStudy: boolean;
    ActivityConceptionFilmAdvertising: boolean;
    ActivityConceptionSurvey: boolean;
    ActivityConceptionGraphic: boolean;
    ActivityConceptionAdvertisingObject: boolean;
    ActivityConceptionAdvertisingCampaign: boolean;
    ActivityConceptionEventOrganisation: boolean;
    ActivityAdviceCommunication: boolean;
    ActivityAdviceDesignAndCreativity: boolean;
    ActivityAdviceMarketing: boolean;
    ActivityAdviceBrandSociety: boolean;
    ActivityAdviceAdvertisement: boolean;
    ActivityCreationMultimedia: boolean;
    ActivityAdvicePressAgent: boolean;
    ActivityManagementAdvertisingSpace: boolean;
    ActivityManagementAdvertisingMedia: boolean;
    ActivityPhotoLibrary: boolean;
    ActivityPromotionMark: boolean;
    ActivityPromotionSales: boolean;
    ActivityAdvertisingMobile: boolean;
    ActivityAdvertisingLeaflet: boolean;
    ActivityAdvertisingInPointOfSale: boolean;
    ActivityCommunityManager: boolean;
    ActivityPrintingDocument: boolean;
    ActivityMarketingDirect: boolean;
    ActivityBuyAdvertisingSpace: boolean;
    ActivityAdvertisingAgency: boolean;
    ActivityComputerGraphicsDesigner: boolean;
    ActivityAdviceMedia: boolean;
    ActivitySocietyTelemarketing: boolean;
    ActivityCreationAndManagementOfWebSite: boolean;

    constructor() {
        this.ActivtyCallCenter = false;
        this.ActivityConceptionMarketStudy = false;
        this.ActivityConceptionFilmAdvertising = false;
        this.ActivityConceptionSurvey = false;
        this.ActivityConceptionGraphic = false;
        this.ActivityConceptionAdvertisingObject = false;
        this.ActivityConceptionAdvertisingCampaign = false;
        this.ActivityConceptionEventOrganisation = false;
        this.ActivityAdviceCommunication = false;
        this.ActivityAdviceDesignAndCreativity = false;
        this.ActivityAdviceMarketing = false;
        this.ActivityAdviceBrandSociety = false;
        this.ActivityAdviceAdvertisement = false;
        this.ActivityCreationMultimedia = false;
        this.ActivityAdvicePressAgent = false;
        this.ActivityManagementAdvertisingSpace = false;
        this.ActivityManagementAdvertisingMedia = false;
        this.ActivityPhotoLibrary = false;
        this.ActivityPromotionMark = false;
        this.ActivityPromotionSales = false;
        this.ActivityAdvertisingMobile = false;
        this.ActivityAdvertisingLeaflet = false;
        this.ActivityAdvertisingInPointOfSale = false;
        this.ActivityCommunityManager = false;
        this.ActivityPrintingDocument = false;
        this.ActivityMarketingDirect = false;
        this.ActivityBuyAdvertisingSpace = false;
        this.ActivityAdvertisingAgency = false;
        this.ActivityComputerGraphicsDesigner = false;
        this.ActivityAdviceMedia = false;
        this.ActivitySocietyTelemarketing = false;
        this.ActivityCreationAndManagementOfWebSite = false;
    }
}

export class PeopleCare {
    ActivityMobilityHelp: boolean;
    ActivityAssistanceAdministrative: boolean;
    ActivityAssistanceIt: boolean;
    ActivityHomeLesson: boolean;
    ActivityGardening: boolean;
    ActivityMaintenanceResidence: boolean;
    ActivityCleaning: boolean;
    ActivityPutInTouchWithHomeHelper: boolean;
    ActivityPreparationMeal: boolean;
    ActivityBeautician: boolean;
    ActivityMobileHairdresser: boolean;
    ActivityHealthPet: boolean;
    ActivityPrivateLessons: boolean;
    ActivityHelpline: boolean;
    ActivityAdministrativeWorks: boolean;
    ActivityPhotographer: boolean;
    ActivityApartmentBuilding: boolean;
    ActivityHomeStaging: boolean;
    ActivitySocietyHomeService: boolean;
    ActivitySocietyPeopleCare: boolean;
    ActivityCleaner: boolean;
    ActivitySocialAssistant: boolean;
    ActivityChildrenCulturalCaretaking: boolean;

    constructor() {
        this.ActivityMobilityHelp = false;
        this.ActivityAssistanceAdministrative = false;
        this.ActivityAssistanceIt = false;
        this.ActivityHomeLesson = false;
        this.ActivityGardening = false;
        this.ActivityMaintenanceResidence = false;
        this.ActivityCleaning = false;
        this.ActivityPutInTouchWithHomeHelper = false;
        this.ActivityPreparationMeal = false;
        this.ActivityBeautician = false;
        this.ActivityMobileHairdresser = false;
        this.ActivityHealthPet = false;
        this.ActivityPrivateLessons = false;
        this.ActivityHelpline = false;
        this.ActivityAdministrativeWorks = false;
        this.ActivityPhotographer = false;
        this.ActivityApartmentBuilding = false;
        this.ActivityHomeStaging = false;
        this.ActivitySocietyHomeService = false;
        this.ActivitySocietyPeopleCare = false;
        this.ActivityCleaner = false;
        this.ActivitySocialAssistant = false;
        this.ActivityChildrenCulturalCaretaking = false;
    }
}