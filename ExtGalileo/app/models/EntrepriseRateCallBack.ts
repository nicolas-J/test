﻿export class EntrepriseRateCallBack {
    public Level: string;
    //rate
    public RateUnique: number;
    public RateIsolated: number;
    public RateFamily: number;
    public RateAdult: number;
    public RateChild: number;
    public RateSpouse: number;
    //price
    public PriceUnique: string;
    public PriceIsolated: string;
    public PriceFamily: string;
    public PriceAdult: string;
    public PriceChild: string;
    public PriceSpouse: number;
    //rate option
    public PmssAdultOption1: number;
    public PmssAdultOption2: number;
    public PmssChildOption1: number;
    public PmssChildOption2: number;
    //price option
    public PriceAdultOption1: number;
    public PriceAdultOption2: number;
    public PriceChildOption1: number;
    public PriceChildOption2: number;
}