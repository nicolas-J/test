﻿export class BankAccountData  
{
Iban: string ;
Bic: string ;
AccountOwnerName: string ;
Domiciliation: string ;
AgreementDate: Date ; 
}