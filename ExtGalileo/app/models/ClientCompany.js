"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var PostalAddress_1 = require("./PostalAddress");
var CompanyData_1 = require("./CompanyData");
var BankAccountData_1 = require("./BankAccountData");
var MailPhoneFax_1 = require("./MailPhoneFax");
var ClientCompany = (function (_super) {
    __extends(ClientCompany, _super);
    function ClientCompany() {
        var _this = _super.call(this) || this;
        _this.MailPhoneFax = new MailPhoneFax_1.MailPhoneFax();
        _this.BankAccountData = new BankAccountData_1.BankAccountData();
        _this.Address = new PostalAddress_1.PostalAddress();
        return _this;
    }
    return ClientCompany;
}(CompanyData_1.CompanyData));
exports.ClientCompany = ClientCompany;
var BusinessLine;
(function (BusinessLine) {
    BusinessLine[BusinessLine["Digital"] = 0] = "Digital";
    BusinessLine[BusinessLine["FoodIndustry"] = 1] = "FoodIndustry";
    BusinessLine[BusinessLine["Audit"] = 2] = "Audit";
    BusinessLine[BusinessLine["Lawyer"] = 3] = "Lawyer";
    BusinessLine[BusinessLine["CDNA"] = 4] = "CDNA";
    BusinessLine[BusinessLine["Construction"] = 5] = "Construction";
    BusinessLine[BusinessLine["CTS"] = 6] = "CTS";
    BusinessLine[BusinessLine["Accountant"] = 7] = "Accountant";
    BusinessLine[BusinessLine["Finance"] = 8] = "Finance";
    BusinessLine[BusinessLine["Fondations"] = 9] = "Fondations";
    BusinessLine[BusinessLine["Cold"] = 10] = "Cold";
    BusinessLine[BusinessLine["RealEstate"] = 11] = "RealEstate";
    BusinessLine[BusinessLine["Industry"] = 12] = "Industry";
    BusinessLine[BusinessLine["Medical"] = 13] = "Medical";
    BusinessLine[BusinessLine["MedicoSociale"] = 14] = "MedicoSociale";
    BusinessLine[BusinessLine["Service"] = 15] = "Service";
    BusinessLine[BusinessLine["FastFood"] = 16] = "FastFood";
    BusinessLine[BusinessLine["Anothers"] = 17] = "Anothers";
})(BusinessLine = exports.BusinessLine || (exports.BusinessLine = {}));
//# sourceMappingURL=ClientCompany.js.map