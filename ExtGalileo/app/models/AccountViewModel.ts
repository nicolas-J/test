﻿export class LoginViewModel {
    Email: string;
    Password: string;
    RememberMe: Boolean;
}

export class RegisterViewModel {
    Email: string;
    Password: string;
    ConfirmPassword: string;
}

export class ResetPasswordViewModel {
    Email: string;
    Password: string;
    ConfirmPassword: string;
    Code: string;
}

export class ForgotPasswordViewModel {
    Email: string;
}