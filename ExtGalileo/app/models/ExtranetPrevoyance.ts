﻿import { Product } from "./Product";
import { ProductOptions } from "./ExtranetSubscription";

export class ExtranetPrevoyance {
    Commission: number;
    College: string;
    Convention: string;
    RiskClass: string;
    RiskClassDc: number;
    RiskClassAt: number;
    Product: Product;
    PayrollTa: number;
    PayrollTb: number;
    PayrollTc: number;
    Syntec: boolean;
    SpousePension: string;
    FranchisePurchase: boolean;
    AccidentHospiPurachse: boolean;
    Franchise: string;
    AccidentalDeathCapital: string;
    TcIpWarranty: string;
    CotisationExonerationOption: boolean;
    RevenueEvolutionOption: boolean;
    FranchiseInvalidityPurchase: boolean;
    SportsOptionPurchase: boolean;
    PsychiatricAndNeurologicalDiseasesPurchase: boolean;
    BackProblemPurchase: boolean;

    //Profession
    Regime: string;
    Category: string;
    Profession: string;
    ProfessionPrecision: string;

    //Prices
    Price: number;
    Rate: number;
    PriceTa: number;
    PriceTb: number;
    PriceTc: number;
    IttPrice: number;
    IttRate: number;
    IpPrice: number;
    IpRate: number;

    //PMSS
    PmssTa: number;
    PmssTb : number;
    PmssTc: number;

    //Options
    CompulsoryDeathOptionLevel: string;
    CompulsoryDeathOptionPrice: number;
    CompulsoryDeathOptionRate: number;

    IncludeInvalidityOption: boolean;
    InvalidityOptionLevel: number;
    InvalidityOptionPrice: number;
    InvalidityOptionRate: number;

    IncludeAccidentalDeathOption: boolean;
    AccidentalDeathOptionLevel: string;
    AccidentalDeathOptionPrice: number;
    AccidentalDeathOptionRate: number;

    IncludeSpousePensionOption: boolean;
    SpousePensionOptionLevel: string;
    SpousePensionOptionPrice: number;
    SpousePensionOptionRate: number;

    IncludeEducationPensionOption: boolean;
    EducationPensionOptionLevel: string;
    EducationPensionOptionPrice: number;
    EducationPensionOptionRate: number;

    IncludeDoubleEffectOption: boolean;
    DoubleEffectOptionLevel: string;
    DoubleEffectOptionPrice: number;
    DoubleEffectOptionRate: number;

    IncludeFranchiseOption: boolean;
    FranchiseOptionLevel: string;
    FranchiseOptionPrice: number;
    FranchiseOptionRate: number;

    IncludeIjComplementaryOption: boolean;
    IjComplementaryOptionLevel: string;
    IjComplementaryOptionPrice: number;
    IjComplementaryOptionRate: number;
 }