﻿export class Benefit  
                {
KendoId: number ;
Text: string ;
Value: string ;

 _parent: Benefit;
 _children: Array<Benefit> = new Array<Benefit>();

 Benefit()
        {
    this._parent = null;
}

 Benefit1(text: string, value : string = "")
{
    this._parent = null;
    this.Text = text;
    this.Value = value;
}

 Benefit2(parent: Benefit, text: string,  value:string = "")
{
    this._parent = parent;
    this.Text = text;
    this.Value = value;
}



                }