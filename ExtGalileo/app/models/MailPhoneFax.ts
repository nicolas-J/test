﻿export class MailPhoneFax {
    FixPhoneNumber: string;
    MobilePhoneNumber: string;
    Fax: string;
    EmailAddress: string;

    ToString(): string {
        return (this.EmailAddress != null ? this.EmailAddress : "") + "  " + (this.MobilePhoneNumber != null ? this.MobilePhoneNumber : "") + "  " + (this.FixPhoneNumber != null ? this.FixPhoneNumber : "") + "  " + (this.Fax != null ? this.Fax : "");
    }

}