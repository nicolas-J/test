﻿import {PostalAddress} from './PostalAddress';
import {MailPhoneFax} from './MailPhoneFax';

export class IPersonData {
    Title: Title;
    TitleString: string[];
    FamilySituation: FamilyStatus;
    Name: string;
    MaidenName: string;
    FirstName: string;
    BirthDate: Date;
    PostalAddress: PostalAddress;
    MailPhoneFax: MailPhoneFax;
    SocialSecurityNumber: string;
    SocialSecurityManagementCode: string;
    SocialSecurityRegime: string;
    SocialSecurityAffiliationAgency: string;
}
export enum FamilyStatus {
    Single,

    Married,

    Widowed,

    Divorced,

    Separated,

    Cohabitation,

    CivilUnion
}
export enum Title {
    Mr,

    Mrs
}