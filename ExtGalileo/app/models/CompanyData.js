"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var Client_1 = require("./Client");
var CompanyData = (function (_super) {
    __extends(CompanyData, _super);
    function CompanyData() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return CompanyData;
}(Client_1.Client));
exports.CompanyData = CompanyData;
var Contact = (function () {
    function Contact() {
    }
    return Contact;
}());
exports.Contact = Contact;
//# sourceMappingURL=CompanyData.js.map