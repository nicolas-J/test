﻿import { ExtranetSubscription } from './ExtranetSubscription';

export class RcProChubb extends ExtranetSubscription {
    public PrimaryActivity: string;
    public PrimaryActivitiesList: string[];
    public SecondaryActivities: string[];
    public SecondaryActivitiesList: string[];
    public DueDate: Date;
    public WithGuarantee: boolean;
    public IsPaidSubscription: boolean;
    public PdfDevisUrl: string;
}