﻿export interface Event 
                {
DateCreated: Date ;
EventTypeString: string ;
Origin: string ;
Text: string ;
DateCompleted: Date ;
HasAttachment: boolean ; 
}
