﻿import { PostalAddress } from './PostalAddress';
import { CompanyData } from './CompanyData';
import { BankAccountData } from './BankAccountData';
import { Task } from './Task';
import { ClientPerson } from './ClientPerson';
import { MailPhoneFax } from './MailPhoneFax';

export class ClientCompany extends CompanyData {
    CompanyTask: Array<Task>;
    AffiliatesId: Array<string>;
    ParentCompanyId: string;
    StaffRegime: string;
    EmployeesNumber: number;
    EmployeesAverageAge: number;
    FamilyStatus: string;
    Siret: string;
    NafCode: string;
    ChildNumber: number;
    Employee: Array<ClientPerson>;
    EmployeesAni: string;
    College: string;
    RegimeArea: string;
    BankAccountData: BankAccountData;
    Area: string;
    ConjointNumber: number;
    SingleNumber: number;
    CompanyEvent: Array<Event>;
    BusinessLine: BusinessLine;
    Idcc: string;
    ActivityArea: string;
    MailPhoneFax: MailPhoneFax;
    Address: PostalAddress;

    constructor() {
        super();
        this.MailPhoneFax = new MailPhoneFax();
        this.BankAccountData = new BankAccountData();
        this.Address = new PostalAddress();
    }
}
export enum BusinessLine {
    Digital,

    FoodIndustry,

    Audit,

    Lawyer,

    CDNA,

    Construction,

    CTS,

    Accountant,

    Finance,

    Fondations,

    Cold,

    RealEstate,

    Industry,

    Medical,

    MedicoSociale,

    Service,

    FastFood,

    Anothers
}