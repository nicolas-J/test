﻿import { Client } from './Client';
import { PostalAddress } from './PostalAddress';
import { BankAccountData } from './BankAccountData';


export class CompanyData extends Client {
    LegalForm: string;
    MainContact: Contact;
    OtherContacts: Array<Contact>;
    HeadquarterAddress: PostalAddress;
    Address: PostalAddress;
    /*[BsonDateTimeOptions(Kind = DateTimeKind.Local)]*/
    DateOfCreation: Date;
    Siret: string;
    NafCode: string;
    Rcs: string;
    Payroll: number;
    Msa: boolean;
    VerySmallCompany: boolean;
    Activity: string;
    BankAccountData: BankAccountData;
}
export class Contact {
    Name: string;
    FirstName: string;
    Mail: string;
    Phone: string;
}