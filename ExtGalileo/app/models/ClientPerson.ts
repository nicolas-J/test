﻿import { PostalAddress } from './PostalAddress';
import { Managers } from './Managers';
import { BankAccountData } from './BankAccountData';
import { PersonStatus } from './PersonStatus';
import { MailPhoneFax } from './MailPhoneFax';
import { FamilyStatus } from './IPersonData';

export class ClientPerson {
    FamilyStatus: string;
    //Title: Title;
    TitleString: string;
    FamilySituation: FamilyStatus;
    FirstName: string;
    Name: string;
    MaidenName: string;
    BirthDate: Date;
    PostalAddress: PostalAddress;
    SocialSecurityNumber: string;
    SocialSecurityManagementCode: string;
    SocialSecurityRegime: string;
    SocialSecurityAffiliationAgency: string;
    Function: string;
    AttachmentOrganismNumbmer: string;
    BirthTown: string;
    BirthDepartment: string;
    BirthCountry: string;
    Managers: Managers;
    PersonStatus: PersonStatus;
    Spouse: ClientPerson;
    Children: Array<ClientPerson>;
    BankAccountData: BankAccountData;
    UmanLife: boolean;
    PackSpvie: boolean;
    Siret: string;
    ClientPersonEvent: Array<Event>;
    MailPhoneFax: MailPhoneFax;

    constructor() {
        this.TitleString = "MR";
        this.FirstName = "";
        this.MaidenName = "";
        this.PostalAddress = new PostalAddress();
        this.Managers = new Managers();
        this.PersonStatus = new PersonStatus();
        this.Spouse = null;
        this.Children = new Array<ClientPerson>();
        this.BankAccountData = new BankAccountData();
        //this.ContractsId = new Array<string>();
        this.ClientPersonEvent = new Array<Event>();
        this.MailPhoneFax = new MailPhoneFax();

    }
}