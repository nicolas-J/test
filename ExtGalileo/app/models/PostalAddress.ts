﻿export class PostalAddress
    {
        Number : string;
        Address: string;
        Address2: string;
        Address3: string;
        City: string;
        PostCode: string;
        Country: string;

        GetFullPostalAddress()
        {
            return (Number != null ? Number + " " : "") + (this.GetFullAdress() != null ? this.GetFullAdress() + " " : "") + (this.PostCode != null ? this.PostCode + " " : "") + (this.City != null ? this.City + " " : "") + (this.Country ? "" : "");
        }

        GetFullAdress()
        {
            return this.Address + " " + this.Address2 + " " + this.Address3;
        }
    }