"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AxaServicePro = (function () {
    function AxaServicePro() {
        this.EducationAndFormationModel = new EducationAndFormation();
        this.AdviceSocietyModel = new AdviceSociety();
        this.ItTradeModel = new ItTrade();
        this.ServiceToSocietyModel = new ServiceToSociety();
        this.SerenityTradeModel = new SerenityTrade();
        this.MarketinAndCommunicationModel = new MarketinAndCommunication();
        this.PeopleCareModel = new PeopleCare();
        this.Freelancer = false;
        this.RevenueUpper = false;
        this.DeclaredRevenue = 0;
        this.FamilyActivity = "";
        this.TotalPremium = 0;
        this.CommissioningFee = 0;
        this.ActivityList = [];
        this.Siret = "";
        this.NafCode = "";
        this.AutoEntrepreneurConditions = false;
        this.InfoNotice = false;
        this.IsAccepted = false;
        this.Division = "Mensuel";
        this.AnnualRate = 0;
        this.SixMonthRate = 0;
        this.EstimatedCommission = 0;
        this.LegalStatut = "PersonneMorale";
        this.LegalForm = "";
    }
    return AxaServicePro;
}());
exports.AxaServicePro = AxaServicePro;
var EducationAndFormation = (function () {
    function EducationAndFormation() {
        this.ActivityCareersAdviser = false;
        this.ActivityDistanceLearning = false;
        this.ActivityDogTrainer = false;
        this.ActivityEducator = false;
        this.ActivityExamProofreader = false;
        this.ActivityFirstAidTrainer = false;
        this.ActivityPrivateLessons = false;
        this.ActivitySpeaker = false;
        this.ActivitySupervisor = false;
        this.ActivityTeacher = false;
        this.ActivityTrainerSecurityFire = false;
    }
    return EducationAndFormation;
}());
exports.EducationAndFormation = EducationAndFormation;
var AdviceSociety = (function () {
    function AdviceSociety() {
        this.ActivityAdviceHumanResources = false;
        this.ActivityProfessionalCoaching = false;
        this.ActivityAdviceStrategyOrganisation = false;
        this.ActivityManagementConsultant = false;
        this.ActivityAdviceOutsourcing = false;
        this.ActivityAdviceCorporateSocialResponsibility = false;
        this.ActivityAdviceQualityCertification = false;
        this.ActivityAdviceHealthAndSecurity = false;
        this.ActivityAdviceInformationSystem = false;
        this.ActivityAdviceTelecommunication = false;
        this.ActivityAdviceCertifiedPublicAccountant = false;
        this.ActivityAdviceAdministrativeManagement = false;
        this.ActivityAdviceMarketStudy = false;
        this.ActivityAdviceCostReduction = false;
        this.ActivityAdviceLogistics = false;
        this.ActivityAdviceImportExport = false;
        this.ActivityAdviceCommunication = false;
        this.ActivityAdviceDesignAndCreativity = false;
        this.ActivityAdviceMarketing = false;
        this.ActivityAdviceBrandSociety = false;
        this.ActivityAdviceAdvertisement = false;
        this.ActivityAdviceMedia = false;
        this.ActivityAdvicePressAgent = false;
    }
    return AdviceSociety;
}());
exports.AdviceSociety = AdviceSociety;
var ItTrade = (function () {
    function ItTrade() {
        this.ActivityAdviceInformationSystemAndIntegration = false;
        this.ActivityPersonalTrainingIt = false;
        this.ActivityCreationWebSite = false;
        this.ActivityItAdvice = false;
        this.ActivityTechnicalAssistant = false;
        this.ActivityMakeEditSaleSoftware = false;
        this.ActivitySaleSoftwarePackage = false;
        this.ActivityDataProcessor = false;
        this.ActivityApplicationHosting = false;
        this.ActivitySalesComputerEquipment = false;
        this.ActivityMaintenanceAndTechnicalAssistant = false;
        this.ActivityManagementItStock = false;
        this.ActivityComputerGraphicsDesigner = false;
        this.ActivityAdvertisingAgency = false;
    }
    return ItTrade;
}());
exports.ItTrade = ItTrade;
var ServiceToSociety = (function () {
    function ServiceToSociety() {
        this.ActivityHeadHunter = false;
        this.ActivitySocietyTemporaryWork = false;
        this.ActivityOutsourcingAdministrativeTask = false;
        this.ActivityAgencyCreationOfSociety = false;
        this.ActivtyOutsourcingManagement = false;
        this.ActivityDevelopmentOfficeEquipment = false;
        this.ActivityKeepingAccountsBook = false;
        this.ActivityOutsourcingPayday = false;
        this.ActivityTranslationAndInterpreting = false;
        this.ActivityAdministrativeAssistant = false;
        this.ActivityReceptionAgent = false;
        this.ActivityPhotographer = false;
        this.ActivityStageDesigner = false;
        this.ActivityApartmentBuilding = false;
        this.ActivityHomeStaging = false;
        this.ActivityCleaner = false;
    }
    return ServiceToSociety;
}());
exports.ServiceToSociety = ServiceToSociety;
var SerenityTrade = (function () {
    function SerenityTrade() {
        this.ActivityFengShui = false;
        this.ActivityMusicTherapy = false;
        this.ActivityReflexology = false;
        this.ActivityCoachSports = false;
        this.ActivityErgonomist = false;
        this.ActivityBeautician = false;
        this.ActivityAdviceHealthDietary = false;
        this.ActivityMobileHairdresser = false;
    }
    return SerenityTrade;
}());
exports.SerenityTrade = SerenityTrade;
var MarketinAndCommunication = (function () {
    function MarketinAndCommunication() {
        this.ActivtyCallCenter = false;
        this.ActivityConceptionMarketStudy = false;
        this.ActivityConceptionFilmAdvertising = false;
        this.ActivityConceptionSurvey = false;
        this.ActivityConceptionGraphic = false;
        this.ActivityConceptionAdvertisingObject = false;
        this.ActivityConceptionAdvertisingCampaign = false;
        this.ActivityConceptionEventOrganisation = false;
        this.ActivityAdviceCommunication = false;
        this.ActivityAdviceDesignAndCreativity = false;
        this.ActivityAdviceMarketing = false;
        this.ActivityAdviceBrandSociety = false;
        this.ActivityAdviceAdvertisement = false;
        this.ActivityCreationMultimedia = false;
        this.ActivityAdvicePressAgent = false;
        this.ActivityManagementAdvertisingSpace = false;
        this.ActivityManagementAdvertisingMedia = false;
        this.ActivityPhotoLibrary = false;
        this.ActivityPromotionMark = false;
        this.ActivityPromotionSales = false;
        this.ActivityAdvertisingMobile = false;
        this.ActivityAdvertisingLeaflet = false;
        this.ActivityAdvertisingInPointOfSale = false;
        this.ActivityCommunityManager = false;
        this.ActivityPrintingDocument = false;
        this.ActivityMarketingDirect = false;
        this.ActivityBuyAdvertisingSpace = false;
        this.ActivityAdvertisingAgency = false;
        this.ActivityComputerGraphicsDesigner = false;
        this.ActivityAdviceMedia = false;
        this.ActivitySocietyTelemarketing = false;
        this.ActivityCreationAndManagementOfWebSite = false;
    }
    return MarketinAndCommunication;
}());
exports.MarketinAndCommunication = MarketinAndCommunication;
var PeopleCare = (function () {
    function PeopleCare() {
        this.ActivityMobilityHelp = false;
        this.ActivityAssistanceAdministrative = false;
        this.ActivityAssistanceIt = false;
        this.ActivityHomeLesson = false;
        this.ActivityGardening = false;
        this.ActivityMaintenanceResidence = false;
        this.ActivityCleaning = false;
        this.ActivityPutInTouchWithHomeHelper = false;
        this.ActivityPreparationMeal = false;
        this.ActivityBeautician = false;
        this.ActivityMobileHairdresser = false;
        this.ActivityHealthPet = false;
        this.ActivityPrivateLessons = false;
        this.ActivityHelpline = false;
        this.ActivityAdministrativeWorks = false;
        this.ActivityPhotographer = false;
        this.ActivityApartmentBuilding = false;
        this.ActivityHomeStaging = false;
        this.ActivitySocietyHomeService = false;
        this.ActivitySocietyPeopleCare = false;
        this.ActivityCleaner = false;
        this.ActivitySocialAssistant = false;
        this.ActivityChildrenCulturalCaretaking = false;
    }
    return PeopleCare;
}());
exports.PeopleCare = PeopleCare;
//# sourceMappingURL=AxaServicePro.js.map