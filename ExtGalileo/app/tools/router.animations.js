"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var animations_1 = require("@angular/animations");
function slideToRight() {
    return animations_1.trigger('slideToRight', [
        animations_1.state('void', animations_1.style({ position: 'absolute', width: '100%' })),
        animations_1.state('*', animations_1.style({ position: 'absolute', width: '100%' })),
        animations_1.transition(':enter', [
            animations_1.style({ transform: 'translateX(-100%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateX(0%)' }))
        ]),
        animations_1.transition(':leave', [
            animations_1.style({ transform: 'translateX(0%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateX(100%)' }))
        ])
    ]);
}
exports.slideToRight = slideToRight;
function slideToLeft() {
    return animations_1.trigger('slideToLeft', [
        animations_1.state('void', animations_1.style({ position: 'absolute', width: '100%' })),
        animations_1.state('*', animations_1.style({ position: 'absolute', width: '100%' })),
        animations_1.transition(':enter', [
            animations_1.style({ transform: 'translateX(100%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateX(0%)' }))
        ]),
        animations_1.transition(':leave', [
            animations_1.style({ transform: 'translateX(0%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateX(-100%)' }))
        ]),
    ]);
}
exports.slideToLeft = slideToLeft;
function slideToBottom() {
    return animations_1.trigger('slideToBottom', [
        animations_1.state('void', animations_1.style({ position: 'absolute', width: '100%' })),
        animations_1.state('*', animations_1.style({ position: 'absolute', width: '100%' })),
        animations_1.transition(':enter', [
            animations_1.style({ transform: 'translateY(-100%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateY(0%)' }))
        ]),
        animations_1.transition(':leave', [
            animations_1.style({ transform: 'translateY(0%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateY(100%)' }))
        ])
    ]);
}
exports.slideToBottom = slideToBottom;
function slideToTop() {
    return animations_1.trigger('slideToTop', [
        animations_1.state('void', animations_1.style({ position: 'absolute', width: '100%' })),
        animations_1.state('*', animations_1.style({ position: 'absolute', width: '100%' })),
        animations_1.transition(':enter', [
            animations_1.style({ transform: 'translateY(100%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateY(0%)' }))
        ]),
        animations_1.transition(':leave', [
            animations_1.style({ transform: 'translateY(0%)' }),
            animations_1.animate('0.5s ease-in-out', animations_1.style({ transform: 'translateY(-100%)' }))
        ])
    ]);
}
exports.slideToTop = slideToTop;
//# sourceMappingURL=router.animations.js.map