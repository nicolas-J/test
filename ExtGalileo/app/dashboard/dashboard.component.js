"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var router_animations_1 = require("../tools/router.animations");
var app_service_1 = require("../services/app.service");
var DashboardComponent = (function () {
    function DashboardComponent(router, http, route, appService) {
        this.router = router;
        this.http = http;
        this.route = route;
        this.appService = appService;
        this.displaySurvey = false;
        this.loadPage = false;
        this.contactComManager = false;
        this.banner = "Mon Tableau de bord";
    }
    DashboardComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.appService.loader(true);
        this.appService.getDatas('/Home/GetDashBoard/').subscribe(function (callback) {
            _this.response = callback;
            _this.displaySurvey = _this.response['PopNews'];
            //this.displaySurvey = true;
            _this.loadPage = true;
            _this.appService.loader(false);
        });
    };
    DashboardComponent.prototype.showContactManager = function () {
        this.contactComManager = true;
    };
    DashboardComponent.prototype.contactManager = function (form) {
        var _this = this;
        this.appService.sendDatas('/Contact/MessageToComManager/', form).subscribe(function (callBack) { _this.response = callBack; });
    };
    DashboardComponent.prototype.submitForm = function (form) {
        var _this = this;
        this.appService.sendDatas('/Tools/StartForm/', form).subscribe(function (callBack) { _this.response = callBack; });
    };
    return DashboardComponent;
}());
DashboardComponent = __decorate([
    core_1.Component({
        selector: 'dashboard',
        templateUrl: '/app/dashboard/dashboard.component.html',
        animations: [router_animations_1.slideToLeft()],
        host: { '[@slideToLeft]': '' }
    }),
    __metadata("design:paramtypes", [router_1.Router, http_1.Http, router_1.ActivatedRoute, app_service_1.AppService])
], DashboardComponent);
exports.DashboardComponent = DashboardComponent;
//# sourceMappingURL=dashboard.component.js.map