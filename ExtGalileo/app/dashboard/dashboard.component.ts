﻿import { Component, OnInit } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Routes, RouterModule } from '@angular/router';
import { slideToLeft } from '../tools/router.animations';

import { AppService } from '../services/app.service';

@Component({
    selector: 'dashboard',
    templateUrl: '/app/dashboard/dashboard.component.html',
    animations: [slideToLeft()],
    host: { '[@slideToLeft]': '' }
})

export class DashboardComponent {
    displaySurvey: boolean = false;
    loadPage: boolean = false;
    contactComManager: boolean = false;
    response: any;
    banner = "Mon Tableau de bord";

    constructor(private router: Router, private http: Http, private route: ActivatedRoute, private appService: AppService) {
    }

    ngOnInit() {
        this.appService.loader(true);
        this.appService.getDatas('/Home/GetDashBoard/').subscribe(callback => {
            this.response = callback;
            this.displaySurvey = this.response['PopNews'];
            //this.displaySurvey = true;
            this.loadPage = true;
            this.appService.loader(false);
        });
    }

    showContactManager() {
        this.contactComManager = true;
    }

    contactManager(form: any): void {
        this.appService.sendDatas('/Contact/MessageToComManager/', form).subscribe(callBack => { this.response = callBack });
    }

    submitForm(form: any): void {
        this.appService.sendDatas('/Tools/StartForm/', form).subscribe(callBack => { this.response = callBack });
    }
}