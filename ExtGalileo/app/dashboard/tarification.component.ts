﻿import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import { Router, ActivatedRoute, Params, Routes, RouterModule } from '@angular/router';
import { slideToLeft } from '../tools/router.animations';

import { AppService } from '../services/app.service';

@Component({
    selector: 'aw-products',
    templateUrl: '/app/dashboard/tarification.component.html',
    providers: [AppService],
    animations: [slideToLeft()],
    host: { '[@slideToLeft]': '' }
})

export class TarificationComponent {
    banner = "Nos Produits";

    constructor() {
    }

    ngOnInit() {
    }
}