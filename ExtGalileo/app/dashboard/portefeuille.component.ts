﻿import { Component, OnInit, Input, ViewChild, Injectable } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import { Router, ActivatedRoute, Params, Routes, RouterModule } from '@angular/router';
import { AppService } from '../services/app.service';
import { slideToLeft } from '../tools/router.animations';

@Component({
    selector: 'portefeuille',
    templateUrl: '/app/dashboard/portefeuille.component.html',
    providers: [AppService],
    animations: [slideToLeft()],
    host: { '[@slideToLeft]': '' }
})

export class PortefeuilleComponent {
    banner = "Mon Portefeuille";
    response: string;
    loadPage: boolean = false;
    dataChart: any;
    optionsChart: any;
    chartValue: any;

    constructor(private router: Router, private http: Http, private route: ActivatedRoute, public appService: AppService) {
    }

    ngOnInit() {
        if (this.router.url.match("BrokerName")) {
            this.appService.getDatas('/PortFolio/MyContracts?BrokerName').subscribe(callback => {
                this.response = callback;
                this.loadPage = true;
            });
        }
        else {
            this.appService.getDatas('/PortFolio/MyContracts/').subscribe(callback => {
                this.response = callback[0];
                this.chartValue = callback[1];
                this.showGraph(this.chartValue);
                this.loadPage = true;
            });
        }
    }

    showGraph(chartValue: any) {
        this.dataChart = {
            labels: ['ADP', 'IARD', 'PREV'],
            datasets: [
                {
                    data: [chartValue[0], chartValue[1], chartValue[2]],
                    backgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ]
                }]
        };

        this.optionsChart = {
            title: {
                display: true,
                text: 'Mes affaires',
                fontSize: 16
            },
            legend: {
                position: 'bottom'
            },
            cutoutPercentage : 80,
        };
    }
}