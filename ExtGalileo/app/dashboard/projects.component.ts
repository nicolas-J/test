﻿import { Component, OnInit, Input, ViewChild, Injectable } from '@angular/core';
import { Headers, Http, Response } from "@angular/http";
import { Router, ActivatedRoute, Params, Routes, RouterModule } from '@angular/router';
import { AppService } from '../services/app.service';
import { slideToLeft } from '../tools/router.animations';

import { Product } from "../models/Product";

import { Subject } from 'rxjs/Rx';

import { DataGrid } from "../models/DataGrid";

@Component({
    selector: 'projects',
    templateUrl: '/app/dashboard/projects.component.html',
    animations: [slideToLeft()],
    host: { '[@slideToLeft]': '' }
})

export class ProjectsComponent {
    banner = "Listes des projets";

    //DataGrid
    projects: DataGrid[] = [];
    status: string;
    dtOptions: DataTables.Settings = {};
    dtTrigger: Subject<DataGrid> = new Subject();

    constructor(private router: Router, private http: Http, private route: ActivatedRoute, public appService: AppService) {
    }

    ngOnInit() {

        this.status = 'loading';
        this.dtOptions = {
            pagingType: "full_numbers",
            order: [0, 'desc'],
            columns: [{ type: 'date-eu' }, null, null, null, null, null, null, null],
            language: {
                search: "Rechercher&nbsp;:",
                lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                info: "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                infoEmpty: "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                infoPostFix: "",
                loadingRecords: "Chargement en cours...",
                zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                emptyTable: "Aucune donn&eacute;e disponible dans le tableau",
                paginate: {
                    first: "Premier",
                    previous: "Pr&eacute;c&eacute;dent",
                    next: "Suivant",
                    last: "Dernier"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre d&eacute;croissant"
                }
            }
        };

        this.appService.getDatas('/Home/GetProjects').subscribe(callback => {
            this.status = 'active';
            this.projects = callback;
            console.log(this.projects);
            this.dtTrigger.next();
        });
    }

    initProject(id: any) {
        this.router.navigate(["/Products/RcPro/" + id]);
    }
}

