﻿import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Routes, RouterModule } from '@angular/router';
import { slideToLeft } from '../tools/router.animations';

export class Hero {
    constructor(
        public id: number,
        public name: string,
        public power: string,
        public alterEgo?: string
    ) { }
}

@Component({
    selector: 'documents',
    templateUrl: '/app/dashboard/documents.component.html',
    animations: [slideToLeft()],
    host: { '[@slideToLeft]': '' }
})

export class DocumentsComponent {
    banner = "Les Documents produits";

    powers = ['Really Smart', 'Super Flexible',
        'Super Hot', 'Weather Changer'];

    model = new Hero(18, 'Dr IQ', this.powers[0], 'Chuck Overstreet');

    submitted = false;

    onSubmit() { this.submitted = true; }

    // TODO: Remove this when we're done
    get diagnostic() { return JSON.stringify(this.model); }

    newHero() {
        this.model = new Hero(42, '', '');
    }
    //////// NOT SHOWN IN DOCS ////////

    // Reveal in html:
    //   Name via form.controls = {{showFormControls(heroForm)}}
    showFormControls(form: any) {

        return form && form.controls['name'] &&
            form.controls['name'].value; // Dr. IQ
    }
}