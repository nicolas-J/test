﻿using AssurWareCommon.ModelsExtClient;
using UniversalRepository;
using Autofac;
using System.Web.Mvc;

namespace ExtGalileo.Controllers
{
    public class ProductsController : Controller
    {
        // GET: Products
        public ActionResult RcPro()
        {
            return View("~/Views/Shared/default.cshtml");
        }

        [HttpPost]
        [Route("{controller}/{action}")]
        public ActionResult Save(RcProChubb model)
        {
            using (var scope = RepositoryContainer.Container.BeginLifetimeScope())
            {
                var repository = scope.Resolve<IUniversalRepository>();
                var result = repository.Upsert(model);

                if (result == null)
                    return Json(new { status = "error" });

                return Json(result);
            }
        }

        [HttpPost]
        [Route("{controller}/{action}")]
        public ActionResult GetRcPro(string model)
        {
            using (var scope = RepositoryContainer.Container.BeginLifetimeScope())
            {
                var repository = scope.Resolve<IUniversalRepository>();
                var result = repository.SingleById<RcProChubb>(model);

                if (result == null)
                    return Json(new string[] { "Erreur", "Le serveur a rencontré un problème, veuillez réessayer ultérieurement" });

                return Json(result);
            }
        }
    }
}