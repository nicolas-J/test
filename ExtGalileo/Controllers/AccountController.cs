﻿using System;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AssurWareCommon.Helpers;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ExtGalileo.Models;
using Newtonsoft.Json;
using ExtGalileo;

namespace ExtGalileo.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private ApplicationRoleManager _roleManager;

        public AccountController()
        {
        }

        public AccountController(ApplicationUserManager userManager, ApplicationRoleManager roleManager, ApplicationSignInManager signInManager)
        {
            UserManager = userManager;
            SignInManager = signInManager;
            RoleManager = roleManager;
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public ApplicationRoleManager RoleManager
        {
            get
            {
                return _roleManager ?? HttpContext.GetOwinContext().Get<ApplicationRoleManager>();
            }
            private set
            {
                _roleManager = value;
            }
        }

        //
        // GET: /Account/Login
        [AllowAnonymous]
        [Route("{controller}/{action}")]
        public ActionResult Login(string returnUrl)
        {
            // if server are down uncomment the line below
            //return RedirectToAction("TechnicalError", "Extranet");
            // We do not want to use any existing identity information
            EnsureLoggedOut();
            ViewBag.ReturnUrl = returnUrl;
            //return View(model);
            return View("~/Views/Shared/default.cshtml");
        }

        [HttpPost]
        [AllowAnonymous]
        [Route("{controller}/{action}")]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            try
            {
                var urlIdentification = Request.UrlReferrer.AbsolutePath;

                if (!ModelState.IsValid)
                    return Json(new { severity = "error", summary = "Erreur", detail = "Identifient ou mot de passe invalide" });

                model.Email = model.Email.ToLower();
                var user = UserManager.Find(model.Email, model.Password);

                if (user != null)
                {
                    if (!await UserManager.IsEmailConfirmedAsync(user.Id))
                        return Json(new { severity = "error", summary = "Erreur", detail = "Pour pouvoir vous connecter, vous devez confirmer votre adresse email. Nous vous avons envoyé un email de confirmation." });
                }

                var result = await SignInManager.PasswordSignInAsync(model.Email, model.Password, model.RememberMe, shouldLockout: false);
                switch (result)
                {
                    case SignInStatus.Success:
                        {
                            var token = Guid.NewGuid().ToString("N");
                            var newToken = new Claim("token", token);
                            var userClaims = await UserManager.GetClaimsAsync(user.Id);
                            var oldToken = userClaims.FirstOrDefault(x => x.Type == "token");

                            await UserManager.RemoveClaimAsync(user.Id, oldToken);
                            await UserManager.AddClaimAsync(user.Id, newToken);

                            return Json(new string[] { "success", user.Email });
                        }
                    case SignInStatus.LockedOut:
                        return Json(new { severity = "error", summary = "Erreur", detail = "LockOut" });
                    case SignInStatus.RequiresVerification:
                        return Json(new { severity = "error", summary = "Erreur", detail = "Signout" });
                    default:
                        return Json(new { severity = "error", summary = "Erreur", detail = "Les identifiants ne sont pas corrects" });
                }
            }
            catch (Exception e)
            {
                LogHelper.Exception(e);
                return Json(new { severity = "error", summary = "Erreur", detail = "Une erreur s'est produite, veuillez contacter le support" });
            }
        }

        //
        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("~/Views/Shared/default.cshtml");//View error
            }
            return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        //
        // GET: /Account/Register
        [AllowAnonymous]
        [Route("{controller}/{action}")]
        public ActionResult Register()
        {
            // We do not want to use any existing identity information
            EnsureLoggedOut();
            return View("~/Views/Shared/default.cshtml");
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [Route("{controller}/{action}")]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (model.Email != null)
                model.Email = model.Email.ToLower();

            if (model.Password != model.ConfirmPassword)
                return Json(new { severity = "error", summary = "Erreur", detail = "Le mot de passe ne correspond pas avec le mot de passe de confirmation" });

            if (string.IsNullOrEmpty(model.Password))
                return Json(new { severity = "error", summary = "Erreur", detail = "Veuillez remplir votre mot de passe" });

            if (!InputHelper.IsValidEmail(model.Email))
                return Json(new { severity = "error", summary = "Erreur", detail = "Veuillez remplir votre Email" });

            if (!ModelState.IsValid)
                return Json(new { severity = "error", summary = "Erreur", detail = "Information incorrect" });

            if (UserManager.FindByEmail(model.Email.ToLower()) != null)
                return Json(new { severity = "error", summary = "Erreur", detail = "Vous êtes déjà inscrit sur notre Extranet, veuillez vous loguer" });

            var user = new GalileoUser { UserName = model.Email.ToLower(), Email = model.Email.ToLower() };
            user.AddRole("Admin");
            var result = await UserManager.CreateAsync(user, model.Password);
            if (result.Succeeded)
            {
                var code = await UserManager.GenerateEmailConfirmationTokenAsync(user.Id);
                var callbackUrl = Url.Action("Login", "Account", new { userId = user.Id, confirmationCode = code }, Request.Url.Scheme);

                var body = "<p>Cher(e) Partenaire,</p>" +
                           "<p>Votre accès à l'extranet SPVIE Partenaire a été activé.<br> Vous pouvez dès à présent commencer à tarifer l'ensemble de vos projets sur notre outil de tarification online.</p>" +
                           "<p>Il vous suffit de cliquer sur le lien suivant pour activer votre compte :<br>" +
                           "<a href='" + callbackUrl + "' target='_blank'>Lien d'activation</a></p>" +
                           "<p>Identifiant : " + model.Email + "</p>" +
                           "<p>Nous vous souhaitons la bienvenue sur notre plateforme dédiée.</p>" +
                           "<p>Bonnes affaires !</p>" +
                           "<p>L'équipe Galiléo</p>";

                await UserManager.SendEmailAsync(user.Id, "Bienvenue sur Galiléo", body);
                return Json(new { severity = "success", summary = "Succès", detail = "Merci, Vous êtes bien enregistré sur Galiléo." +
                    "<br>Cliquez sur le lien de confirmation dans le mail que vous venez recevoir avant de pouvoir vous connecter. " });
            }
            AddErrors(result);
            return Json(new { severity = "error", summary = "Erreur", detail = "Information incorrect" });
        }

        //
        // GET: /Account/ConfirmEmail
        [HttpPost]
        [AllowAnonymous]
        [Route("{controller}/{action}")]
        public async Task<ActionResult> ConfirmEmail(string userId, string confirmationCode)
        {
            if (userId == null || confirmationCode == null)
            {
                throw new HttpException(404, "Item Not Found");
            }
            EnsureLoggedOut();
            var result = await UserManager.ConfirmEmailAsync(userId, confirmationCode);
            if (result.Succeeded)
            {
                return Json(new { severity = "success", summary = "Succès", detail = "Votre email est confirmé, vous pouvez dès lors vous connecter." });
            }

            return Json(new { severity = "error", summary = "Erreur", detail = "Une erreur est survenue." });
        }

        //
        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        [Route("{controller}/{action}")]
        public ActionResult ForgotPassword()
        {
            // We do not want to use any existing identity information
            EnsureLoggedOut();

            return View("~/Views/Shared/default.cshtml");
        }


        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [Route("{controller}/{action}")]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return Json(new { severity = "error", summary = "Erreur", detail = "Utilisateur inconnu ! Veuillez créer un compte" });
                }

                // For more information on how to enable account confirmation and password reset please visit http://go.microsoft.com/fwlink/?LinkID=320771
                // Send an email with this link
                var code = await UserManager.GeneratePasswordResetTokenAsync(user.Id);
                var callbackUrl = Url.Action("ResetPassword", "Account", new { userId = user.Id, confirmationCode = code }, protocol: Request.Url.Scheme);

                //SendMessage.Email("yann.chedeville@assurware.com", "Reset Password", "Courtier : " + model.Email + " a reset son mot de passe");

                var body = "<p>Cher(e) Utilisateur,</p>" +
                   "<p>Voici le lien pour réinitaliser votre mot de passe:<br>" +
                   "<a href=\"" + callbackUrl + "\" target=\"_blank\">Lien d'activation</a></p>" +
                   "<p>Identifiant : " + model.Email + "</p>" +
                   "<p>Bonne continuation !</p>" +
                   "<p>L'équipe Galiléo</p>";

                await UserManager.SendEmailAsync(user.Id, "Galiléo - Réinitialiser votre mot de passe", body);
                return Json(new { severity = "success", summary = "Succès", detail = "Vous allez recevoir un email pour générer un nouveau mot de passe." });
            }

            // If we got this far, something failed, redisplay form
            return Json(new { severity = "error", summary = "Erreur", detail = "Veuillez mettre votre adresse email pour générer un nouveau mot de passe" });
        }

        //
        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        [Route("{controller}/{action}")]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        //
        // GET: /Account/ResetPassword
        [AllowAnonymous]
        [Route("{controller}/{action}")]
        public ActionResult ResetPassword(string confirmationCode)
        {
            return View("~/Views/Shared/default.cshtml");
            //if (confirmationCode == null)
            //{
            //    throw new HttpException(404, "Item Not Found");
            //}
            //var viewModel = new ResetPasswordViewModel { Code = confirmationCode };
            //return View(viewModel);
        }

        //
        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [Route("{controller}/{action}")]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return Json(new { severity = "error", summary = "Erreur", detail = "Verifiez que vous avez bien tapé 2 fois le même mot de passe" });
            }
            var user = await UserManager.FindByNameAsync(model.Email);
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return Json(new { severity = "error", summary = "Erreur", detail = "Impossible de réinitialiser votre mot de passe" });
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
            if (result.Succeeded)
            {
                return Json(new { severity = "success", summary = "Succès", detail = "Votre mot de passe a bien été réinitialisé" });
            }
            AddErrors(result);
            return Json(new { severity = "error", summary = "Erreur", detail = "Votre Token a expiré redirigez vous sur la page 'Mot de passe oublié ?'" });
        }

        //
        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        [Route("{controller}/{action}")]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        //
        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
        }

        //
        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("~/Views/Shared/default.cshtml");//View error
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
            return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
        }

        //
        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("~/Views/Shared/default.cshtml");//View error
            }
            return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
        }

        //
        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }

            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                case SignInStatus.Failure:
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewBag.ReturnUrl = returnUrl;
                    ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
            }
        }

        //
        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }

            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var user = new GalileoUser { UserName = model.Email, Email = model.Email };
                var result = await UserManager.CreateAsync(user);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(user.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }

            ViewBag.ReturnUrl = returnUrl;
            return View(model);
        }

        //
        // POST: /Account/LogOff
        [HttpPost]
        [Route("{controller}/{action}")]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut();
            FormsAuthentication.SignOut();

            // Second we clear the principal to ensure the user does not retain any authentication
            HttpContext.User = new GenericPrincipal(new GenericIdentity(string.Empty), null);
            // Last we redirect to a controller/action that requires authentication to ensure a redirect takes place
            // this clears the Request.IsAuthenticated flag since this triggers a new request

            return Json(new { severity = "success", summary = "Succès", detail = "Vous êtes bien déconnecté, à très bientôt! " });
        }

        //
        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_userManager != null)
                {
                    _userManager.Dispose();
                    _userManager = null;
                }

                if (_signInManager != null)
                {
                    _signInManager.Dispose();
                    _signInManager = null;
                }
            }

            base.Dispose(disposing);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl = "")
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return View("~/Views/Shared/default.cshtml");//View index extranet
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri)
                : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirectUri, string userId)
            {
                LoginProvider = provider;
                RedirectUri = redirectUri;
                UserId = userId;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                if (UserId != null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }

        private void EnsureLoggedOut()
        {
            // If the request is (still) marked as authenticated we send the user to the logout action
            if (Request.IsAuthenticated)
            {
                Session.Clear();
                LogOff();
            }
        }

        #endregion  
    }
}