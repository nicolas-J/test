﻿using AssurWareCommon.PdfLibrary;
using System.Web.Mvc;
using AssurWarePdfFarm;
using AssurWareCommon.Helpers;
using System;
using System.Collections.Generic;
using AssurWareCommon.ModelsExtClient;
using AssurWareCommon.Models;
using System.Net;
using ExtGalileo.Helpers;
using UniversalRepository;
using Autofac;

namespace ExtGalileo.Controllers
{
    public class PdfController : Controller
    {
        private string _randNum = DateTime.Now.Ticks.ToString();
        private RcProChubb rcModel;


        private void InitPdfInfo(byte[] pdfBytes, string pdfFile, int pageCount, PdfGeneration.FieldInfo[] signatures, ExtranetSubscription model)
        {
            if (pdfBytes == null)
            {
                LogHelper.Error("Cannot generate" + pdfFile);
                SendMessage.Email("yann.chedeville@assurware.com", "InitPdfInfo PdfController", "pdf null " + pdfFile);
            }
            var docInfo = new DocInfo { Pages = pageCount, Signatures = new List<Signature>() };
            foreach (var signature in signatures)
                docInfo.Signatures.Add(new Signature { FieldName = signature.FieldName, PageNb = signature.PageNb, X = signature.X, Y = signature.Y });
            model.DocInfos.Add(docInfo);
        }

        [HttpPost]
        [Route("{controller}/{action}")]
        public ActionResult GetPdf(RcProChubb model)
        {
            model.DocInfos = new List<DocInfo>();
            var productsController = new ProductsController();
            var result = productsController.Save(model);

            int pageCount;
            PdfGeneration.FieldInfo[] signatures;

            using (var final = new Pdf())
            {
                var productName = "galileo_rc_pro_";
                var SelectedPdf = new List<byte[]>();

                if (model.WithGuarantee)
                {
                    var pdfGeneratedAnnexe = new Pdf().PdfRead(productName + "annexe.pdf");
                    InitPdfInfo(pdfGeneratedAnnexe, productName + "annexe.pdf", 3, new PdfGeneration.FieldInfo[] { }, model);

                    var pdfGeneratedAttestationAG = GalileoRCProPdf.GalileoRCProAttestationAG(productName + "attestation_avec_garantie.pdf", model, out pageCount, out signatures);
                    InitPdfInfo(pdfGeneratedAttestationAG, productName + "attestation_avec_garantie.pdf", pageCount, signatures, model);
                    SelectedPdf.AddRange(new[] { pdfGeneratedAnnexe, pdfGeneratedAttestationAG });
                }
                else
                {
                    var pdfGeneratedAttestationSG = GalileoRCProPdf.GalileoRCProAttestationSG(productName + "attestation_sans_garantie.pdf", model, out pageCount, out signatures);
                    InitPdfInfo(pdfGeneratedAttestationSG, productName + "attestation_sans_garantie.pdf", pageCount, signatures, model);
                    SelectedPdf.Add(pdfGeneratedAttestationSG);
                }

                var pdfGeneratedCP = GalileoRCProPdf.GalileoRCProCP(productName + "cp.pdf", model, out pageCount, out signatures);
                InitPdfInfo(pdfGeneratedCP, productName + "cp.pdf", pageCount, signatures, model);

                var pdfGeneratedEasyTech = GalileoRCProPdf.GalileoRCProEasyTech(productName + "easy_tech.pdf", model, out pageCount, out signatures);
                InitPdfInfo(pdfGeneratedEasyTech, productName + "easy_tech.pdf", pageCount, signatures, model);

                var pdfGeneratedLettreCotation = GalileoRCProPdf.GalileoRCProLettreCotation(productName + "lettre_cotation.pdf", model, out pageCount, out signatures);
                InitPdfInfo(pdfGeneratedLettreCotation, productName + "lettre_cotation.pdf", pageCount, signatures, model);
                SelectedPdf.AddRange(new[] { pdfGeneratedCP, pdfGeneratedEasyTech, pdfGeneratedLettreCotation });

                var pdfFinal = final.PdfMerge(SelectedPdf.ToArray());

                if (final.GetLastOperationStatus() != "OK")
                    LogHelper.Error("pdf merge problem: " + final.GetLastOperationStatus());

                var pdfMergeUrl = final.PdfWrite(productName + model.Client.Name + "_" + DateTime.Now.ToShortDateString().Replace("/", "_") + _randNum + ".pdf", pdfFinal);

                model.PdfDevisUrl = pdfMergeUrl;
                rcModel = model;
                productsController.Save(model);

                return Json(pdfMergeUrl);
            }
        }

        [HttpPost]
        [Route("{controller}/{action}")]
        public ActionResult SignInit(RcProChubb model)
        {
            try
            {
                GetPdf(model);
                var pdfUrl = rcModel.PdfDevisUrl;

                if (!string.IsNullOrEmpty(pdfUrl))
                {

                    var urlToByte = new WebClient();
                    byte[] byteDoc = urlToByte.DownloadData(pdfUrl);

                    // info Prod
                    //var signService = new UniversignRequests("jm.pironneau@assurware.com", "SpvieAssurWare2015");
                    var signService = new UniversignRequests("gestion@spvie.com", "spvie2016");

                    //COMPTE DE TEST
                    //var signService = new UniversignRequests("yann.chedeville@assurware.com", "B0LQ1GK8");
                    // info Recette
                    //var signService = new UniversignRequests("yann.chedeville@assurware.com", "spvie2015");
                    var urlProduct = "http://" + Request.Url.Authority + "/Pdf/SignCallback/" + rcModel.Id;
                    LogHelper.Info("SignInit urlProduct : " + urlProduct);

                    //var urlProduct = "http://"+Request.Url.Authority+"/Sign/GetSignPdf/"+ rcModel.Id;
                    var callBack = signService.SignatureDocument(rcModel.Client.Name.Replace(" ", "_")
                        + "_galileo_rc_pro.pdf", byteDoc, rcModel, urlProduct);

                    if (string.IsNullOrEmpty(callBack[0]) || string.IsNullOrEmpty(callBack[1]))
                    {
                        rcModel.Signature = false;
                        return Json("Erreur lors de la signature du document.");
                    }

                    if (callBack[0].Contains("Error") || string.IsNullOrEmpty(callBack[0]))
                    {
                        //new TrackingService().AddTrackingExtranet(DateTime.Now, User.Identity.Name, "Signature Electronique", "Tentative de signature Electronique", "");
                        LogHelper.Error("Error Signature : " + callBack[0]);
                        LogHelper.Error("Error Signature : " + callBack[1]);
                        rcModel.Signature = false;
                        return Json("Erreur lors de la signature du document.");
                    }
                    LogHelper.Info("SignInit callback[1] : " + callBack[1]);
                    return Json(callBack[1]);
                }
                return Json(false);
            }
            catch (Exception e)
            {
                rcModel.Signature = false;
                LogHelper.Error("Erreur signature : " + e.Message);
                LogHelper.Error("Erreur signature client : " + rcModel.Client.Name);
                //new TrackingService().AddTrackingExtranet(DateTime.Now, User.Identity.Name, "Signature Electronique", "Tentative de signature Electronique", "");
                return Json("Erreur : Une erreur s'est produite, un email a été envoyé au service informatique. Veuillez nous en excuser.");
            }
        }

        [AllowAnonymous]
        public ActionResult SignCallBack(string id, string sign = null)
        {
            using (var scope = RepositoryContainer.Container.BeginLifetimeScope())
            {
                var severity = "error";
                var summary = "Erreur";
                var detail = "Une erreur s'est produite, veuillez réessayer ultérieurement";
                var url = string.Format("/Products/RcPro?id=" + id + "&severity=" + severity + "&summary=" + summary + "&detail=" + detail);

                if (string.IsNullOrEmpty(id))
                {
                    severity = "error";
                    summary = "Erreur";
                    detail = "Une erreur s'est produite, veuillez réessayer ultérieurement";
                    return RedirectToLocal(url);
                }

                var repository = scope.Resolve<IUniversalRepository>();
                var model = repository.SingleById<RcProChubb>(id);
                var returnSignAssurValue = Request.QueryString["sign"];

                if (returnSignAssurValue == "success" && !string.IsNullOrEmpty(model.PdfSignId))
                {
                    model.PdfSignUrl = GetSignPdf(id);

                    if (string.IsNullOrEmpty(model.PdfSignUrl))
                    {
                        severity = "error";
                        summary = "Erreur";
                        detail = "Une erreur s'est produite, veuillez réessayer ultérieurement";
                    }
                    else
                    {
                        severity = "success";
                        summary = "Succès";
                        detail = "Votre signature a été réalisée avec succès.";

                        model.Statut = "signed";
                        repository.Upsert(model, User.Identity.Name);
                    }
                }

                url = string.Format("/Products/RcPro?id=" + id + "&severity=" + severity + "&summary=" + summary + "&detail=" + detail);
                return RedirectToLocal(url);
            }
        }

        public string GetSignPdf(string id)
        {
            if (id != "")
            {
                using (var scope = RepositoryContainer.Container.BeginLifetimeScope())
                {
                    var repository = scope.Resolve<IUniversalRepository>();
                    var model = repository.SingleById<RcProChubb>(id);

                    if (!string.IsNullOrEmpty(model.PdfSignId))
                    {
                        // info Prod
                        var signService = new UniversignRequests("gestion@spvie.com", "spvie2016");
                        var pdfSign = signService.RetrieveDocument(model);

                        return pdfSign;
                    }
                }
            }
            return null;
        }

        private ActionResult RedirectToLocal(string returnUrl = "")
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return View("~/Views/Shared/default.cshtml");//View index extranet
        }
    }
}