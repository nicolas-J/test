﻿using System.Security.Cryptography;
using System.Text;
using System.Web.Mvc;
using System;
using AssurWareCommon.ModelsExtClient;
using System.Collections.Generic;
using ExtGalileo.Models;
using System.Globalization;
using System.Diagnostics;
using UniversalRepository;
using Autofac;
using AssurWareCommon.Helpers;
using ExtGalileo.Controllers;
using System.Linq;
using ExtGalileo.Helpers;
using System.Net.Mime;

namespace ExtGalileo.Controllers
{
    public class PaymentController : Controller
    {
        public ActionResult GetSeal(RcProChubb model)
        {
            var productsController = new ProductsController();
            var result = productsController.Save(model);

            var secretKey = "S9i8qClCnb2CZU3y3Vn0toIOgz3z_aBi79akR30vM9o";

            var date = DateTime.Now;
            var reference = Stopwatch.GetTimestamp();
            //var reference = date.Year.ToString() + date.Month.ToString() + date.Day.ToString() + date.Hour.ToString() + new Random().Next(99).ToString();
            var amount = Math.Truncate(model.Base.Price * 100).ToString();

            var data = "amount=" + amount + "|currencyCode=978|merchantId=211000021310001|returnContext=" + model.Id + "" +
                "|normalReturnUrl=http://galileo.assurware.com/Payment/PaymentCallback?id=" + model.Id + "&transactionReference=" + reference + "" +
                "|automaticResponseUrl=http://galileo.assurware.com/Payment/PaymentCallbackAuto|transactionReference=" + reference + "|keyVersion=1";

            var sChaine = data + secretKey;
            UTF8Encoding utf8 = new UTF8Encoding(); byte[] encodedBytes = utf8.GetBytes(sChaine);
            byte[] shaResult; SHA256 shaM = new SHA256Managed(); shaResult = shaM.ComputeHash(encodedBytes);
            var seal = ByteArrayToHEX(shaResult);

            return Json(new { Seal = seal, StringData = data });
        }

        private string ByteArrayToHEX(byte[] ba)
        {
            StringBuilder hex = new StringBuilder(ba.Length * 2);
            foreach (byte b in ba) hex.AppendFormat("{0:x2}", b);
            return hex.ToString();
        }

        public ActionResult PaymentCallback()
        {
            var dataRequest = Request.Unvalidated.Form["Data"].Split('|');
            var responseData = new Dictionary<string, string>();
            foreach (var data in dataRequest)
            {
                var dataField = data.Split('=');
                responseData.Add(dataField[0], dataField[1]);
            }

            using (var scope = RepositoryContainer.Container.BeginLifetimeScope())
            {
                var repository = scope.Resolve<IUniversalRepository>();
                var id = responseData["returnContext"];

                string severity;
                string summary;
                string detail;
                string url;

                switch (responseData["responseCode"])
                {
                    case "00":
                        severity = "success";
                        summary = "Succès";
                        detail = "Votre transaction a été réalisée avec succès.";
                        var model = repository.SingleById<RcProChubb>(id);
                        model.IsPaidSubscription = true;
                        var pdfController = new PdfController();
                        pdfController.GetPdf(model);
                        break;

                    case "02":
                        severity = "error";
                        summary = "Erreur";
                        detail = "Vous devez appeler votre banque, vous avez dépassé votre plafond.";
                        break;

                    case "05":
                        severity = "error";
                        summary = "Erreur";
                        detail = "Votre transaction a été refusée.";
                        break;

                    case "11":
                        severity = "error";
                        summary = "Erreur";
                        detail = "Votre carte est en opposition, veuillez appeler votre banque.";
                        break;

                    case "14":
                        severity = "error";
                        summary = "Erreur";
                        detail = "Une erreur s'est produite lors de la transaction, veuillez vérifier les données saisies avant de réessayer.";
                        break;

                    case "17":
                        severity = "error";
                        summary = "Erreur";
                        detail = "Votre transaction a bien été annulée, si vous le désirez vous pouvez recommencer.";
                        break;

                    case "34":
                        severity = "error";
                        summary = "Erreur";
                        detail = "Une erreur s'est produite, veuillez réessayer.";
                        break;

                    case "54":
                        severity = "error";
                        summary = "Erreur";
                        detail = "La date de validité de votre moyen de paiement est dépassée.";
                        break;

                    case "60":
                        severity = "error";
                        summary = "Erreur";
                        detail = "Votre transaction est en attente, pour plus d'information appelez votre banque.";
                        break;

                    case "75":
                        severity = "error";
                        summary = "Erreur";
                        detail = "Nombre de tentatives de saisie des coordonnées dépassé, veuillez contacter votre banque.";
                        break;

                    case "90":
                        severity = "error";
                        summary = "Erreur";
                        detail = "Service temporairement indisponible.";
                        break;

                    case "97":
                        severity = "error";
                        summary = "Erreur";
                        detail = "Délais expiré, transaction refusée, veuillez réessayer ultérieurement.";
                        break;

                    case "99":
                        severity = "error";
                        summary = "Erreur";
                        detail = "Problème temporaire du paiement, veuillez réessayer ultérieurement.";
                        break;

                    default:
                        severity = "error";
                        summary = "Erreur";
                        detail = "Une erreur s'est produite, veuillez réessayer";
                        break;
                }

                url = string.Format("/Products/RcPro?id=" + id + "&severity=" + severity + "&summary=" + summary + "&detail=" + detail);
                return RedirectToLocal(url);
            }

            //var sealRequest = Request.Unvalidated.Form["Seal"];
            //var interfaceRequest = Request.Unvalidated.Form["InterfaceVersion"];

            //var msg = Newtonsoft.Json.JsonConvert.SerializeObject(dataRequest);
            //var msg2 = Newtonsoft.Json.JsonConvert.SerializeObject(sealRequest);
            //var msg3 = Newtonsoft.Json.JsonConvert.SerializeObject(interfaceRequest);

            //var finalMsg = msg;
            //finalMsg += msg2;
            //finalMsg += msg3;

            //SendMessage.Email("antoine.caplain@assurware.com", "Galileo PaymentCallBack", finalMsg);

            //return RedirectToLocal("/Products/RcPro/" + id);
            //return View("~/Views/Shared/default.cshtml");
        }

        public void PaymentCallbackAuto()
        {
            var dataRequest = Request.Unvalidated.Form["Data"].Split('|');
            var responseData = new Dictionary<string, string>();
            foreach (var data in dataRequest)
            {
                var dataField = data.Split('=');
                responseData.Add(dataField[0], dataField[1]);
            }

            using (var scope = RepositoryContainer.Container.BeginLifetimeScope())
            {
                var repository = scope.Resolve<IUniversalRepository>();
                var id = responseData["returnContext"];
                var subs = repository.SingleById<RcProChubb>(id);

                var paymentInfo = new GalileoPaymentInfo();
                paymentInfo.CompanyName = subs.Client.Name;
                paymentInfo.MobilePhoneNumber = subs.Client.MailPhoneFax.MobilePhoneNumber;
                paymentInfo.EmailAddress = subs.Client.MailPhoneFax.EmailAddress;
                decimal amount;
                var amountString = responseData["amount"];
                if (Decimal.TryParse(amountString, out amount))
                    paymentInfo.Amount = amount / 100;
                paymentInfo.ResponseCode = responseData["responseCode"];
                paymentInfo.TransactionReference = responseData["transactionReference"];
                paymentInfo.TransactionDateTime = DateTime.Parse(responseData["transactionDateTime"], null, DateTimeStyles.RoundtripKind);

                var result = repository.Upsert(paymentInfo, User.Identity.Name);

                string detail;

                switch (responseData["responseCode"])
                {
                    case "00":
                        detail = "Votre transaction a été réalisée avec succès.";
                        var model = repository.SingleById<RcProChubb>(id);
                        model.IsPaidSubscription = true;
                        var pdfController = new PdfController();
                        pdfController.GetPdf(model);
                        break;

                    case "02":
                        detail = "Vous devez appeler votre banque, vous avez dépassé votre plafond.";
                        break;

                    case "05":
                        detail = "Votre transaction a été refusée.";
                        break;

                    case "11":
                        detail = "Votre carte est en opposition, veuillez appeler votre banque.";
                        break;

                    case "14":
                        detail = "Une erreur s'est produite lors de la transaction, veuillez vérifier les données saisies avant de réessayer.";
                        break;

                    case "17":
                        detail = "Votre transaction a bien été annulée, si vous le désirez vous pouvez recommencer.";
                        break;

                    case "34":
                        detail = "Une erreur s'est produite, veuillez réessayer.";
                        break;

                    case "54":
                        detail = "La date de validité de votre moyen de paiement est dépassée.";
                        break;

                    case "60":
                        detail = "Votre transaction est en attente, pour plus d'information appelez votre banque.";
                        break;

                    case "75":
                        detail = "Nombre de tentatives de saisie des coordonnées dépassé, veuillez contacter votre banque.";
                        break;

                    case "90":
                        detail = "Service temporairement indisponible.";
                        break;

                    case "97":
                        detail = "Délais expiré, transaction refusée, veuillez réessayer ultérieurement.";
                        break;

                    case "99":
                        detail = "Problème temporaire du paiement, veuillez réessayer ultérieurement.";
                        break;

                    default:
                        detail = "Une erreur s'est produite, veuillez réessayer";
                        break;
                }

                var callbackUrl = Url.Action("RcPro", "Products", new { id = id }, Request.Url.Scheme);
                var msg = "<p>Cher(e) Client,</p>" +
                    "<p>" + detail + "</p>" +
                    "<p>Vous avez accès à votre souscription en cliquant sur le lien suivant:<br>" +
                    "<a href='" + callbackUrl + "' target='_blank'>Retour à la souscription</a></p>" +
                    "<p>Bonne continuation!";

                SendMessage.Email("antoine.caplain@assurware.com", "Paiement Galiléo, transaction: " + responseData["transactionReference"], msg);
            }
            //var dataRequest = Request.Unvalidated.Form["Data"];
            //var sealRequest = Request.Unvalidated.Form["Seal"];
            //var interfaceRequest = Request.Unvalidated.Form["InterfaceVersion"];

            //var msg = Newtonsoft.Json.JsonConvert.SerializeObject(dataRequest);
            //var msg2 = Newtonsoft.Json.JsonConvert.SerializeObject(sealRequest);
            //var msg3 = Newtonsoft.Json.JsonConvert.SerializeObject(interfaceRequest);

            //var finalMsg = msg;
            //finalMsg += msg2;
            //finalMsg += msg3;

            //SendMessage.Email("antoine.caplain@assurware.com", "Galileo PaymentCallBackAuto", finalMsg);
        }

        private ActionResult RedirectToLocal(string returnUrl = "")
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }

            return View("~/Views/Shared/default.cshtml");//View index extranet
        }

        public ActionResult PaymentList()
        {
            return View("~/Views/Shared/default.cshtml");
        }

        [HttpPost]
        [Route("{controller}/{action}")]
        public ActionResult GetPaymentInfo()
        {
            using (var scope = RepositoryContainer.Container.BeginLifetimeScope())
            {
                var repository = scope.Resolve<IUniversalRepository>();
                var payments = repository.All<GalileoPaymentInfo>();

                var result = new List<DataGrid>();
                if (payments.Count() > 0)
                {
                    foreach (var payment in payments)
                    {
                        var proj = new DataGrid();
                        proj.CreationDate = payment.Created.ToShortDateString();
                        proj.TransactionDate = payment.TransactionDateTime.ToShortDateString();
                        proj.Client = payment.CompanyName;
                        proj.TransactionReference = payment.TransactionReference;
                        switch (payment.ResponseCode)
                        {
                            case "00":
                                proj.ResponseCode = "Acceptée";
                                break;
                            case "05":
                                proj.ResponseCode = "Refusée";
                                break;
                            default:
                                proj.ResponseCode = "Erreur";
                                break;
                        }
                        proj.Price = (double)payment.Amount;
                        proj.MobilePhoneNumber = payment.MobilePhoneNumber;
                        proj.EmailAddress = payment.EmailAddress;
                        result.Add(proj);
                    }

                    return Json(result);
                }
                return Json(new List<string>());
            }
        }

        [HttpPost]
        [Route("{controller}/{action}")]
        public ActionResult GetPaymentListExcel()
        {
            using (var scope = RepositoryContainer.Container.BeginLifetimeScope())
            {
                var paymentList = PaymentHelper.GetPaymentListFile();

                if (paymentList.Length > 0)
                {
                    var base64String = Convert.ToBase64String(paymentList, 0, paymentList.Length);
                    string[] result = new string[4];
                    result[0] = base64String;
                    result[1] = MediaTypeNames.Application.Octet;
                    result[2] = ".xlsx";
                    result[3] = "Liste_des_paiements";
                    var jsonResult = Json(result);
                    //jsonResult.MaxJsonLength = int.MaxValue;
                    return jsonResult;
                }
                else
                    return Json(new { Error = "Impossible de télécharger le fichier excel" });
            }
        }
    }
}