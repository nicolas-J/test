﻿using System.Web.Mvc;
using AssurWareCommon.PremiumCalculatorsExt;
using AssurWareCommon.ModelsExtClient;

namespace ExtGalileo.Controllers
{
    public class CalculateController : Controller
    {
        [HttpPost]
        [Route("{controller}/{action}")]
        public ActionResult RcPro(RcProChubb model)
        {
            if (model.AnnualRevenue > 0 || model.Base.Level != null)
            {
                var service = new GalileoRcProCalculator();
                var result = service.GetPrice(model.AnnualRevenue, model.Base.Level,model.PrimaryActivity , model.OptionAp, model.OptionMd);

                return Json(result);
            }
            return Json("");
        }
    }
}