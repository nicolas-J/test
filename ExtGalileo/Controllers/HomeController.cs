﻿using UniversalRepository;
using Autofac;
using ExtGalileo.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AssurWareCommon.ModelsExtClient;

namespace ExtGalileo.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View("~/Views/Shared/default.cshtml");
        }

        public ActionResult Account()
        {
            return View("~/Views/Shared/default.cshtml");
        }

        public ActionResult NosProduits()
        {
            return View("~/Views/Shared/default.cshtml");
        }

        public ActionResult Projects()
        {
            return View("~/Views/Shared/default.cshtml");
        }

        public ActionResult OnlinePayment()
        {
            return View("~/Views/Shared/default.cshtml");
        }

        [HttpPost]
        [Route("{controller}/{action}")]
        public ActionResult GetProjects()
        {
            using (var scope = RepositoryContainer.Container.BeginLifetimeScope())
            {
                var repository = scope.Resolve<IUniversalRepository>();
                var projects = repository.All<RcProChubb>();

                var result = new List<DataGrid>();
                if(projects.Count() > 0)
                {
                    foreach (var project in projects)
                    {
                        var proj = new DataGrid();
                        proj.CreationDate = project.CreationDate.ToShortDateString();
                        proj.EffectDate = project.EffectDate.ToShortDateString();
                        proj.Client = project.Client.Name;
                        proj.Family = project.Product.Family;
                        proj.Target = project.Product.Target;
                        proj.Product = project.Product.Name;
                        proj.Level = project.Base.Level;
                        proj.Price = project.Base.Price;
                        proj.Id = project.Id;
                        result.Add(proj);
                    }

                    return Json(result);
                }
                return Json(new List<string>());
            }
        }
    }
}