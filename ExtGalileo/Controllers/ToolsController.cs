﻿using AssurWareCommon.Models;
using AssurWareCommon.Helpers;
using System.Web.Mvc;
using System.Collections.Generic;
using UniversalRepository;
using AssurWareCommon.ModelsExtClient;
using Autofac;

namespace ExtGalileo.Controllers
{
    public class ToolsController : Controller
    {
        [HttpPost]
        [Route("{controller}/{action}")]
        public ActionResult SendBackOfficeMail(RcProChubb model)
        {
            using (var scope = RepositoryContainer.Container.BeginLifetimeScope())
            {
                var repository = scope.Resolve<IUniversalRepository>();
                var result = repository.Upsert(model);

                if (result == null)
                    return Json(new { status = "error" });

                var msg = "Bonjour,<br>";
                msg += "Le Client " + model.Client.Name + "<br>";
                msg += "Joignable au " + model.Client.MailPhoneFax.MobilePhoneNumber + "<br>";
                msg += "est en train de faire un devis<br>";
                //SendMessage.Email("test@test.com", "Devis en cours de réalisation", msg);

                return Json(result);
            }
        }

        [HttpPost]
        [Route("{controller}/{action}")]
        public JsonResult GetCity(string model)
        {
            var cities = new List<string>();
            if (!string.IsNullOrEmpty(model))
                cities = CityPostcode.PostcodeToCity(model);

            return Json(cities);
        }
    }
}