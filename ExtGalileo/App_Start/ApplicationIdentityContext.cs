﻿using System;
using AspNet.Identity.MongoDB;
using AssurWareCommon.Helpers;
using ExtGalileo.Models;
using MongoDB.Driver;

namespace ExtGalileo
{
    public class ApplicationIdentityContext : IDisposable
    {
        private static IMongoCollection<GalileoUser> _user;
        private static IMongoCollection<IdentityRole> _roles;

        public static ApplicationIdentityContext Create()
        {
            if (_user == null || _roles == null)
            {
                var client = new MongoClient(CommonParametersHelper.GetDatabaseConnection());
                var database = client.GetDatabase(CommonParametersHelper.GetDatabaseName());
                _user = database.GetCollection<GalileoUser>("GalileoUsers");
                _roles = database.GetCollection<IdentityRole>("GalileoRole");
            }
            return new ApplicationIdentityContext(_user, _roles);
        }

        private ApplicationIdentityContext(IMongoCollection<GalileoUser> user, IMongoCollection<IdentityRole> role)
        {
            User = user;
            Role = role;
        }

        public IMongoCollection<GalileoUser> User { get; set; }
        public IMongoCollection<IdentityRole> Role { get; set; }

        public void Dispose()
        {
        }
    }
}