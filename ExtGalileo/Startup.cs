﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ExtGalileo.Startup))]
namespace ExtGalileo
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
