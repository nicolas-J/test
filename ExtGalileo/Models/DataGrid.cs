﻿namespace ExtGalileo.Models
{
    public class DataGrid
    {
        public string Id { get; set; }
        public string CreationDate { get; set; }
        public string Hour { get; set; }
        public string Status { get; set; }
        public int BrokerCode { get; set; }
        public string BrokerMail { get; set; }
        public string Client { get; set; }
        public string Family { get; set; }
        public string Target { get; set; }
        public string Product { get; set; }
        public string Level { get; set; }
        public double Price { get; set; }
        public string CommercialManagerCode { get; set; }

        //PortFolio
        public string ProductionDate { get; set; }
        public string EffectDate { get; set; }
        public string ContractId { get; set; }
        public string ContractNumber { get; set; }
        public string ContractStatus { get; set; }

        //BufferZone
        public bool MailStatus { get; set; }
        public bool PostStatus { get; set; }
        public double PendingDay { get; set; }
        public string ProjectNumber { get; set; }

        //PaymentInfo
        public string TransactionDate { get; set; }
        public string TransactionReference { get; set; }
        public string ResponseCode { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string EmailAddress { get; set; }
    }
}