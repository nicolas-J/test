﻿using System.Security.Claims;
using System.Threading.Tasks;
using AspNet.Identity.MongoDB;
using Microsoft.AspNet.Identity;

namespace ExtGalileo.Models
{
    // You can add profile data for the user by adding more properties to your ExtranetUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class GalileoUser : IdentityUser
    {
        public string UserId { get; set; }
        public string Browser { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(ApplicationUserManager manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }

    public class GalileoRole : IdentityRole
    {
        public GalileoRole()
        {

        }

        public GalileoRole(string name)
            : this()
        {
            Name = name;
        }
    }
}