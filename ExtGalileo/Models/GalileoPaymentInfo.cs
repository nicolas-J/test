﻿using System;
using UniversalRepository;

namespace ExtGalileo.Models
{
    public class GalileoPaymentInfo: RepositoryCollection
    {
        public decimal Amount { get; set; }
        public string ResponseCode { get; set; }
        public string TransactionReference { get; set; }
        public DateTime TransactionDateTime { get; set; }
        public string CompanyName { get; set; }
        public string MobilePhoneNumber { get; set; }
        public string EmailAddress { get; set; }
    }
}