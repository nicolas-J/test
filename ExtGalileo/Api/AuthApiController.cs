﻿using System.Linq;
using System.Web;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace ExtGalileo.Api
{
    public class AuthApiController : ApiController
    {
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [HttpGet]
        [Route("api/{controller}/{action}")]
        public IHttpActionResult GetUserRoles()
        {
            if (!string.IsNullOrEmpty(User.Identity.Name))
            {
                var userRole = UserManager.GetRoles(User.Identity.GetUserId()).ToList();
                return Json(userRole);
            }
            return Json(new string[] {});
        }
    }
}
