﻿using AssurWareCommon.Models;
using Autofac;
using ExtGalileo.Models;
using SpreadsheetGeneration.Excel;
using System;
using System.Linq;
using UniversalRepository;

namespace ExtGalileo.Helpers
{
    public static class PaymentHelper
    {
        public static byte[] GetPaymentListFile()
        {
            using (var scope = RepositoryContainer.Container.BeginLifetimeScope())
            {
                var repository = scope.Resolve<IUniversalRepository>();

                var model = repository.All<GalileoPaymentInfo>().ToList();

                if (model != null)
                    using (var excel = new ExcelGeneration("Liste des paiements", "Date de la transaction", "référence", "Réponse", "Client", "Mobile", "Adresse email", "Montant"))
                    {

                        foreach (var p in model)
                        {
                            string response;
                            switch (p.ResponseCode)
                            {
                                case "00":
                                    response = "Acceptée";
                                    break;
                                case "05":
                                    response = "Refusée";
                                    break;
                                default:
                                    response = "Erreur";
                                    break;
                            }
                            excel.QuickFillRow(p.TransactionDateTime.ToShortDateString(), p.TransactionReference, response, p.CompanyName, p.MobilePhoneNumber, p.EmailAddress, (double)p.Amount);
                        }
                        var excelBytes = excel.SaveToBytes();
                        //excel.SaveToFile(@"c:/temp/Quick_excel_test.xlsx");

                        if (excelBytes != null)
                            return excelBytes;
                        else
                            return null;
                    }
                else
                    return null;
            }
        }
    }
}