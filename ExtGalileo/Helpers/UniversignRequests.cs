using System;
using System.Collections.Generic;
using System.IO;
using AssurWareCommon.Helpers;
using AssurWareCommon.Models;
using AssurWareCommon.PdfLibrary;
using UniversalRepository;
using Autofac;
using AssurWareCommon.ModelsExtClient;
using CookComputing.XmlRpc;

namespace ExtGalileo.Helpers
{

    class UniversignRequests
    {
        private string email;
        private string password;

        public UniversignRequests(string email, string password)
        {
            this.email = email;
            this.password = password;
        }

        private ISignature Init()
        {
            //Initialize the proxy
            ISignature proxy = (ISignature)XmlRpcProxyGen.Create(typeof(ISignature));

            // url prod
            proxy.Url = "https://ws.universign.eu/sign/rpc/";

            // url test
            //proxy.Url = "https://sign.test.cryptolog.com/sign/rpc/";

            //set credentials
            proxy.Credentials = new System.Net.NetworkCredential(email, password);
            //The library fails if set to True
            proxy.KeepAlive = false;

            return proxy;
        }

        public string[] SignatureDocument(string docName, byte[] document, RcProChubb model, string urlProduct)
        {

            using (var scope = RepositoryContainer.Container.BeginLifetimeScope())
            {
                var repository = scope.Resolve<IUniversalRepository>();
                model = repository.SingleById<RcProChubb>(model.Id);
            }

            ISignature proxy = Init();

            var docSignaturesFields = new List<DocSignatureField>();
            var pageOffset = 0;
            foreach (var docInfo in model.DocInfos)
            {
                foreach (var t in docInfo.Signatures)
                {
                    var sfield = new DocSignatureField { page = t.PageNb + pageOffset, x = t.X, y = t.Y, signerIndex = 0 };
                    docSignaturesFields.Add(sfield);
                }
                pageOffset += docInfo.Pages;
            }

            // Structure TransactionSigner
            TransactionSigner tsign = new TransactionSigner();
            tsign.firstname = model.Client.Name;
            tsign.emailAddress = model.Client.MailPhoneFax.EmailAddress;
            tsign.phoneNum = model.Client.MailPhoneFax.MobilePhoneNumber;

            // Structure TransactionDocument
            TransactionDocument tdoc = new TransactionDocument();
            tdoc.name = Path.GetFileName(docName);
            tdoc.content = document;

            tdoc.signatureFields = docSignaturesFields.ToArray();

            // Structure TransactionRequest
            TransactionRequest treq = new TransactionRequest();
            //replace by your application URL
            //The loading of this page should trigger the retrieveDocument call
            treq.successURL = urlProduct + "?sign=success";
            treq.failURL = urlProduct + "?sign=error";
            treq.cancelURL = urlProduct + "?sign=cancel";
            treq.profile = "SPVIE";
            treq.signers = new TransactionSigner[1];
            treq.signers[0] = tsign;
            treq.documents = new TransactionDocument[1];
            treq.documents[0] = tdoc;
            treq.mustContactFirstSigner = true;
            treq.finalDocSent = true;
            treq.identificationType = "sms";
            treq.certificateTypes = new string[1];
            treq.certificateTypes[0] = "timestamp";
            treq.language = "fr";
            treq.handwrittenSignature = false;

            try
            {
                //request a new transaction
                TransactionResponse trep = proxy.requestTransaction(treq);

                using (var scope = RepositoryContainer.Container.BeginLifetimeScope())
                {
                    model.PdfSignId = trep.id;
                    var repository = scope.Resolve<IUniversalRepository>();
                    if (model.Id == null)
                        throw new Exception();
                    repository.Upsert(model);

                    var info = new string[] { trep.id, trep.url };

                    return (info);
                }

            }
            catch (Exception ex)
            {
                var error = new string[] { "Error", ex.ToString() };
                SendMessage.Email("yann.chedeville@assurware.com", "Erreur UniversignRequest", ex.Message);
                return (error);
                //System.Console.WriteLine(ex.ToString());
            }
        }

        public string RetrieveDocument(RcProChubb model)
        {
            ISignature proxy = Init();

            try
            {
                //retrieve the documents
                TransactionGetDocument[] docs = proxy.getDocuments(model.PdfSignId);

                var pdfSignResponse = docs[0].content;

                var final = new Pdf();
                var pdfSignedUrl = final.PdfWrite("Signe_" + model.Client.Name.ToUpper() + "_galileo_rc_pro_" + DateTime.Now.ToShortDateString().Replace("/", "_") + "_" + DateTime.Now.Ticks.ToString() + ".pdf", pdfSignResponse);

                model.PdfSignUrl = pdfSignedUrl;

                using (var scope = RepositoryContainer.Container.BeginLifetimeScope())
                {
                    var repository = scope.Resolve<IUniversalRepository>();
                    repository.Upsert(model);
                }

                return (model.PdfSignUrl);
            }
            catch (Exception ex)
            {
                var message = ex;
                LogHelper.Exception(ex);
                throw;
                //System.Console.WriteLine(ex.ToString());
            }
        }

        public TransactionInfo GetInfo(string id)
        {
            ISignature proxy = Init();

            try
            {
                return proxy.getTransactionInfo(id);
            }
            catch (System.Exception ex)
            {
                System.Console.WriteLine(ex.ToString());
                throw ex;
            }
        }
    }
}
