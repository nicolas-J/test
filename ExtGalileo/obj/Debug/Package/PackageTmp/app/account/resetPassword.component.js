"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var router_animations_1 = require("../tools/router.animations");
var auth_service_1 = require("../services/auth.service");
var app_service_1 = require("../services/app.service");
var AccountViewModel_1 = require("../models/AccountViewModel");
var ResetPasswordComponent = (function () {
    function ResetPasswordComponent(router, authService, appService, route) {
        this.router = router;
        this.authService = authService;
        this.appService = appService;
        this.route = route;
        this.model = new AccountViewModel_1.ResetPasswordViewModel();
    }
    ResetPasswordComponent.prototype.ngOnInit = function () {
        this.authService.logout();
    };
    ResetPasswordComponent.prototype.initPassword = function () {
        var _this = this;
        var code = this.route.snapshot.queryParams["confirmationCode"];
        this.model.Code = code;
        this.appService.sendModel("/Account/ResetPassword", this.model).subscribe(function (callback) {
            if (callback[0] == "success") {
                _this.router.navigate(['/Account/Login']);
            }
            _this.appService.setEmittedValue(callback);
        });
    };
    return ResetPasswordComponent;
}());
ResetPasswordComponent = __decorate([
    core_1.Component({
        selector: 'login-reset',
        templateUrl: '/app/account/resetPassword.component.html',
        animations: [router_animations_1.slideToBottom()],
        host: { '[@slideToBottom]': '' }
    }),
    __metadata("design:paramtypes", [router_1.Router, auth_service_1.AuthService, app_service_1.AppService, router_1.ActivatedRoute])
], ResetPasswordComponent);
exports.ResetPasswordComponent = ResetPasswordComponent;
//# sourceMappingURL=resetPassword.component.js.map