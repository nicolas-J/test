"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var router_animations_1 = require("../tools/router.animations");
var auth_service_1 = require("../services/auth.service");
var app_service_1 = require("../services/app.service");
var AccountViewModel_1 = require("../models/AccountViewModel");
var LoginComponent = (function () {
    function LoginComponent(router, authService, appService, route) {
        this.router = router;
        this.authService = authService;
        this.appService = appService;
        this.route = route;
        this.model = new AccountViewModel_1.LoginViewModel();
    }
    LoginComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.queryParams.subscribe(function (params) {
            if (params["userId"] && params["confirmationCode"])
                _this.appService.sendData("/Account/ConfirmEmail", { userId: params['userId'], confirmationCode: params['confirmationCode'] })
                    .subscribe(function (result) {
                    _this.appService.setEmittedValue(result);
                });
        });
    };
    LoginComponent.prototype.login = function () {
        var _this = this;
        this.authService.login(this.model)
            .subscribe(function (result) {
            if (result.severity == "success") {
                _this.router.navigate(['/Home/Projects']);
            }
            _this.appService.setEmittedValue(result);
        });
    };
    return LoginComponent;
}());
LoginComponent = __decorate([
    core_1.Component({
        selector: 'login',
        templateUrl: '/app/account/login.component.html',
        animations: [router_animations_1.slideToBottom()],
        host: { '[@slideToBottom]': '' }
    }),
    __metadata("design:paramtypes", [router_1.Router, auth_service_1.AuthService, app_service_1.AppService, router_1.ActivatedRoute])
], LoginComponent);
exports.LoginComponent = LoginComponent;
//# sourceMappingURL=login.component.js.map