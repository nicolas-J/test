"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var app_service_1 = require("../services/app.service");
var EditPasswordComponent = (function () {
    function EditPasswordComponent() {
        this.displayEditPassword = false;
    }
    return EditPasswordComponent;
}());
EditPasswordComponent = __decorate([
    core_1.Component({
        selector: 'login-page',
        templateUrl: '/app/popup/editPassword.component.html',
        providers: [app_service_1.AppService]
    })
], EditPasswordComponent);
exports.EditPasswordComponent = EditPasswordComponent;
//# sourceMappingURL=editPassword.component.js.map