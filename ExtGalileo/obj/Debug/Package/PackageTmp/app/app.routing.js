"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var router_1 = require("@angular/router");
var auth_guard_service_1 = require("./services/auth-guard.service");
//Home 
var index_component_1 = require("./home/index.component");
//Account
var account_component_1 = require("./account/account.component");
var login_component_1 = require("./account/login.component");
var register_component_1 = require("./account/register.component");
var forgotPassword_component_1 = require("./account/forgotPassword.component");
var resetPassword_component_1 = require("./account/resetPassword.component");
var tarification_component_1 = require("./dashboard/tarification.component");
var projects_component_1 = require("./dashboard/projects.component");
var documents_component_1 = require("./dashboard/documents.component");
var portefeuille_component_1 = require("./dashboard/portefeuille.component");
//Products
var testModule_component_1 = require("./test/testModule.component");
var rc_pro_component_1 = require("./products/rc-pro/rc-pro.component");
var paymentCallback_component_1 = require("./onlinePayment/paymentCallback.component");
var onlinePayment_component_1 = require("./onlinePayment/onlinePayment.component");
var payment_list_component_1 = require("./onlinePayment/payment-list.component");
//Manage
var collaboratorDetail_component_1 = require("./manage/collaboratorDetail.component");
var appRoutes = [
    {
        path: 'Account',
        children: [
            { path: 'Login', component: login_component_1.LoginComponent },
            { path: 'Register', canActivate: [auth_guard_service_1.AuthGuardService], component: register_component_1.RegisterComponent },
            { path: 'ForgotPassword', component: forgotPassword_component_1.ForgotPasswordComponent },
            { path: 'ResetPassword', component: resetPassword_component_1.ResetPasswordComponent }
        ]
    },
    {
        path: '', pathMatch: 'full', redirectTo: 'Home/Index'
    },
    {
        path: 'Products',
        children: [
            { path: 'RcPro/:id', component: rc_pro_component_1.RcProComponent },
            { path: 'RcPro', component: rc_pro_component_1.RcProComponent }
        ]
    },
    {
        path: 'Home',
        children: [
            { path: 'Index', component: index_component_1.IndexComponent },
            { path: 'Account', canActivate: [auth_guard_service_1.AuthGuardService], component: account_component_1.AccountComponent },
            { path: 'NosProduits', component: tarification_component_1.TarificationComponent },
            { path: 'Portefeuille', component: portefeuille_component_1.PortefeuilleComponent },
            { path: 'Portefeuille/:id', component: portefeuille_component_1.PortefeuilleComponent },
            { path: 'Documents', component: documents_component_1.DocumentsComponent },
            { path: 'Projects', canActivate: [auth_guard_service_1.AuthGuardService], component: projects_component_1.ProjectsComponent },
            { path: 'TestModule', component: testModule_component_1.TestModuleComponent },
            { path: 'OnlinePayment', component: onlinePayment_component_1.OnlinePaymentComponent }
        ]
    },
    {
        path: 'Manage',
        canActivate: [auth_guard_service_1.AuthGuardService],
        children: [
            { path: 'CollaboratorDetail', component: collaboratorDetail_component_1.CollaboratorDetailComponent },
            { path: 'CollaboratorDetail/:id', component: collaboratorDetail_component_1.CollaboratorDetailComponent }
        ]
    },
    {
        path: 'Payment',
        canActivate: [auth_guard_service_1.AuthGuardService],
        children: [
            { path: 'PaymentCallback', component: paymentCallback_component_1.PaymentCallbackComponent },
            { path: 'PaymentCallback/:id', component: paymentCallback_component_1.PaymentCallbackComponent },
            { path: 'PaymentList', canActivate: [auth_guard_service_1.AuthGuardService], component: payment_list_component_1.PaymentListComponent }
        ]
    }
];
exports.routing = router_1.RouterModule.forRoot(appRoutes);
//# sourceMappingURL=app.routing.js.map