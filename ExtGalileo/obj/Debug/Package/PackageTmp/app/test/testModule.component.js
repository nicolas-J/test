"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_animations_1 = require("../tools/router.animations");
var app_service_1 = require("../services/app.service");
var Rx_1 = require("rxjs/Rx");
require("rxjs/add/operator/map");
var Product_1 = require("../models/Product");
var TestModuleComponent = (function () {
    function TestModuleComponent(http, appService) {
        this.http = http;
        this.appService = appService;
        this.products = [];
        this.dtOptions = {};
        this.dtTrigger = new Rx_1.Subject();
        this.status = 'loading';
    }
    TestModuleComponent.prototype.ngOnInit = function () {
        this.dtOptions = {
            pagingType: "full_numbers",
            autoWidth: true,
            language: {
                search: "Rechercher&nbsp;:",
                lengthMenu: "Afficher _MENU_ &eacute;l&eacute;ments",
                info: "Affichage de l'&eacute;l&eacute;ment _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                infoEmpty: "Affichage de l'&eacute;l&eacute;ment 0 &agrave; 0 sur 0 &eacute;l&eacute;ment",
                infoFiltered: "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                infoPostFix: "",
                loadingRecords: "Chargement en cours...",
                zeroRecords: "Aucun &eacute;l&eacute;ment &agrave; afficher",
                emptyTable: "Aucune donn&eacute;e disponible dans le tableau",
                paginate: {
                    first: "Premier",
                    previous: "Pr&eacute;c&eacute;dent",
                    next: "Suivant",
                    last: "Dernier"
                },
                aria: {
                    sortAscending: ": activer pour trier la colonne par ordre croissant",
                    sortDescending: ": activer pour trier la colonne par ordre d&eacute;croissant"
                }
            }
        };
        var del = new Product_1.Product();
        del.Target = "24/03/2017";
        del.Family = "Dupont";
        del.OriginalName = "RC PRO";
        del.BrokerCommission = 945.47;
        var del2 = new Product_1.Product();
        del.Target = "18/04/2017";
        del.Family = "Merlin";
        del.OriginalName = "RC PRO";
        del.BrokerCommission = 458.47;
        this.status = 'active';
        this.products = [del, del2];
        this.dtTrigger.next();
        //this.appService.getDatas('/Projects/AllProjects/').subscribe(callback => {
        //    this.status = 'active';
        //    console.log(callback);
        //    this.products = callback;
        //    this.dtTrigger.next();
        //});
    };
    return TestModuleComponent;
}());
TestModuleComponent = __decorate([
    core_1.Component({
        selector: 'santePart',
        templateUrl: '/app/test/testModule.component.html',
        providers: [app_service_1.AppService],
        animations: [router_animations_1.slideToLeft()],
        host: { '[@slideToLeft]': '' }
    }),
    __metadata("design:paramtypes", [http_1.Http, app_service_1.AppService])
], TestModuleComponent);
exports.TestModuleComponent = TestModuleComponent;
//# sourceMappingURL=testModule.component.js.map