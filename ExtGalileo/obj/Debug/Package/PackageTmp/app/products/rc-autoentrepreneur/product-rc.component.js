"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var router_animations_1 = require("../../tools/router.animations");
require("rxjs/add/operator/toPromise");
//Service
var product_rc_service_1 = require("../../services/productService/product-rc.service");
var auth_guard_service_1 = require("../../services/auth-guard.service");
var masks_service_1 = require("../../services/masks.service");
var products_service_1 = require("../../services/productService/products.service");
var rc_declaration_component_1 = require("../../products/rc-autoentrepreneur/rc-declaration.component");
var ProductRcComponent = (function () {
    ///Functions
    function ProductRcComponent(router, productRcService, http, route, authGuardService, maskService, productsService) {
        this.router = router;
        this.productRcService = productRcService;
        this.http = http;
        this.route = route;
        this.authGuardService = authGuardService;
        this.maskService = maskService;
        this.productsService = productsService;
        this.display = false;
        this.msgs = [];
        this.today = new Date();
        this.endDate = new Date(2017, 11, 31);
        this.startDate = new Date();
        this.startDate.setHours(0, 0, 0, 0);
    }
    ProductRcComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route.params.forEach(function (params) {
            var id = params['id'];
            if (id) {
                _this.productsService.getProductModel(id)
                    .then(function (data) {
                    _this.model = data;
                    _this.model.EffectDate = new Date(_this.model.EffectDate.toString());
                    _this.model.AxaServiceProModel.RecoveryOrCreationDate = new Date(_this.model.AxaServiceProModel.RecoveryOrCreationDate);
                    _this.calculateRcPrice();
                });
            }
        });
        this.rolesSubscription = this.authGuardService.roles.subscribe(function (data) { return _this.roles = data; });
        this.FamilyActivity = [];
        this.FamilyActivity.push({ label: "---", value: null });
        this.FamilyActivity.push({ label: "Education et formation", value: "EducationAndFormation" });
        this.FamilyActivity.push({ label: "Sociétés de conseil", value: "AdviceSociety" });
        this.FamilyActivity.push({ label: "Métiers de l'informatique", value: "ItTrade" });
        this.FamilyActivity.push({ label: "Services aux entreprises", value: "ServiceToSociety" });
        this.FamilyActivity.push({ label: "Marketing et communication", value: "MarketingAndCommunication" });
        this.FamilyActivity.push({ label: "Métiers du biens être", value: "SerenityTrade" });
        this.FamilyActivity.push({ label: "Service à la personne", value: "PeopleCare" });
        this.LegalStatutList = [];
        this.LegalStatutList.push({ label: "Personne morale", value: "PersonneMorale" });
        this.LegalStatutList.push({ label: "Personne physique", value: "PersonnePhysique" });
        this.commissionList = [];
        this.commissionList.push({ label: "0", value: 0 });
        this.commissionList.push({ label: "1", value: 1 });
        this.commissionList.push({ label: "2", value: 2 });
        this.commissionList.push({ label: "3", value: 3 });
        this.commissionList.push({ label: "4", value: 4 });
        this.commissionList.push({ label: "5", value: 5 });
        this.commissionList.push({ label: "6", value: 6 });
        this.commissionList.push({ label: "7", value: 7 });
        this.commissionList.push({ label: "8", value: 8 });
        this.commissionList.push({ label: "9", value: 9 });
        this.commissionList.push({ label: "10", value: 10 });
        this.commissionList.push({ label: "11", value: 11 });
        this.commissionList.push({ label: "12", value: 12 });
        this.commissionList.push({ label: "13", value: 13 });
        this.commissionList.push({ label: "14", value: 14 });
        this.commissionList.push({ label: "15", value: 15 });
        this.commissionList.push({ label: "16", value: 16 });
        this.commissionList.push({ label: "17", value: 17 });
        this.commissionList.push({ label: "18", value: 18 });
        this.commissionList.push({ label: "19", value: 19 });
        this.commissionList.push({ label: "20", value: 20 });
        this.commissionList.push({ label: "21", value: 21 });
        this.commissionList.push({ label: "22", value: 22 });
        this.commissionList.push({ label: "23", value: 23 });
        this.commissionList.push({ label: "24", value: 24 });
        this.commissionList.push({ label: "25", value: 25 });
        this.commissionList.push({ label: "26", value: 26 });
        this.commissionList.push({ label: "27", value: 27 });
        this.commissionList.push({ label: "28", value: 28 });
        this.commissionList.push({ label: "29", value: 29 });
        this.commissionList.push({ label: "30", value: 30 });
    };
    ProductRcComponent.prototype.pad = function (s) { return (s < 10) ? '0' + s : s; };
    ProductRcComponent.prototype.ngOnDestroy = function () {
        this.rolesSubscription.unsubscribe();
    };
    //DisplayList on HTML
    ProductRcComponent.prototype.listFamilyActivity = function () {
        this.model.AxaServiceProModel.ActivityList = this.productRcService.getFamilyActivity(this.model.AxaServiceProModel.FamilyActivity);
        this.model.AxaServiceProModel.ActivityList = this.transform(this.model.AxaServiceProModel.ActivityList);
    };
    //Handle Object
    ProductRcComponent.prototype.transform = function (value) {
        var keys = [];
        for (var key in value) {
            if (value.hasOwnProperty(key)) {
                keys.push({ Key: key, Value: value[key] });
            }
        }
        return keys;
    };
    //Display declaration pop up
    ProductRcComponent.prototype.showDialog = function () {
        this.modal.showDialog();
    };
    ProductRcComponent.prototype.calculateRcPrice = function () {
        var headers = new http_1.Headers();
        headers.append("Content-Type", "application/json");
        var model = JSON.stringify(this.model);
        this.http.post("/Calculate/AxaAutoEntrepreneurPrice/", model, { headers: headers })
            .toPromise()
            .then(function (res) { return console.log(res); });
    };
    ProductRcComponent.prototype.allocatePrices = function (response) {
        var prices = JSON.parse(response._body);
        if (prices.Error != null) {
            this.showMessage("error", "Attention!", prices.Error);
            this.killPrices();
        }
        else {
            this.model.AxaServiceProModel.AnnualRate = prices.Total;
            this.model.AxaServiceProModel.SixMonthRate = (prices.Total / 2).toFixed(2);
            this.model.Base.Price = (prices.Total / 12).toFixed(2);
            this.model.AxaServiceProModel.EstimatedCommission = prices.CommBroker;
            this.clearMessages();
        }
    };
    ProductRcComponent.prototype.showMessage = function (severity, summary, message) {
        this.msgs.push({ severity: severity, summary: summary, detail: message });
    };
    ProductRcComponent.prototype.clearMessages = function () {
        this.msgs = [];
    };
    ProductRcComponent.prototype.killPrices = function () {
        this.model.AxaServiceProModel.AnnualRate = 0;
        this.model.AxaServiceProModel.SixMonthRate = 0;
        this.model.Base.Price = 0;
        this.model.AxaServiceProModel.EstimatedCommission = 0;
    };
    ProductRcComponent.prototype.updatePayment = function (event) {
        this.model.PaymentMethod = event.PaymentMethod;
        this.model.Client.BankAccountData.Iban = event.Client.BankAccountData.Iban;
        this.model.Client.BankAccountData.Bic = event.Client.BankAccountData.Bic;
    };
    return ProductRcComponent;
}());
__decorate([
    core_1.ViewChild('rcForm'),
    __metadata("design:type", forms_1.NgForm)
], ProductRcComponent.prototype, "rcForm", void 0);
__decorate([
    core_1.ViewChild("rcModal"),
    __metadata("design:type", rc_declaration_component_1.RcDeclaration)
], ProductRcComponent.prototype, "modal", void 0);
ProductRcComponent = __decorate([
    core_1.Component({
        selector: "family-activity",
        templateUrl: "/app/products/rc-autoentrepreneur/product-rc.component.html",
        animations: [router_animations_1.slideToLeft()],
        host: { '[@slideToLeft]': '' }
    }),
    __metadata("design:paramtypes", [router_1.Router,
        product_rc_service_1.ProductRcService,
        http_1.Http,
        router_1.ActivatedRoute,
        auth_guard_service_1.AuthGuardService,
        masks_service_1.MasksService,
        products_service_1.ProductsService])
], ProductRcComponent);
exports.ProductRcComponent = ProductRcComponent;
//# sourceMappingURL=product-rc.component.js.map