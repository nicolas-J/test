"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var platform_browser_1 = require("@angular/platform-browser");
var router_animations_1 = require("../../tools/router.animations");
var app_service_1 = require("../../services/app.service");
var masks_service_1 = require("../../services/masks.service");
var auth_guard_service_1 = require("../../services/auth-guard.service");
var ExtranetSubscription_1 = require("../../models/ExtranetSubscription");
var SanteIndividuelComponent = (function () {
    function SanteIndividuelComponent(router, http, appService, route, titleService, authGuardService, maskService) {
        this.router = router;
        this.http = http;
        this.appService = appService;
        this.route = route;
        this.titleService = titleService;
        this.authGuardService = authGuardService;
        this.maskService = maskService;
        this.model = new ExtranetSubscription_1.ExtranetSubscription();
        this.santePrice = 0;
        this.subBanner = "Produit";
        this.banner = "Santé Individuel";
    }
    SanteIndividuelComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.titleService.setTitle("Luca | Produit Santé Individuel");
        this.rolesSubscription = this.authGuardService.roles.subscribe(function (data) { return _this.roles = data; });
        this.endDate = new Date(2017, 11, 31);
        this.startDate = new Date();
        this.startDate.setHours(0, 0, 0, 0);
        this.model.EffectDate = new Date();
        this.model.Commission = "10";
        this.levels = [];
        this.products = [];
        this.products.push({ label: '5820b0c150e47218800559f4', value: 'SPVIE-SANTE-Part-Resp' });
        this.products.push({ label: '56bb4cd458c2a0bd945924d0', value: 'SPVIE-SANTE-TNS-Resp' });
        this.products.push({ label: '5625050a9e8b3a38c810174b', value: 'SPVIE-SANTE-VITALITE-Resp' });
        this.products.push({ label: '5804f663a01bb7393ca35c4f', value: 'SPVIE-SANTE-SURCO-NON-RESP' });
        //this.appService.sendModel('/ProductsSante/GetIndividualHealthProduct/', this.model).subscribe(callback => {
        //    this.products = callback;
        //    console.log(callback);
        //});
    };
    SanteIndividuelComponent.prototype.submitForm = function () {
        //this.appService.sendModel('/CalculateSante/IndividualHealth/', this.model).subscribe(callback => {
        //    this.response = callback;
        //});
    };
    SanteIndividuelComponent.prototype.getChildValidation = function (event) {
        for (var key in event.control) {
            this.productForm.form.addControl(key, event.control[key]);
        }
    };
    SanteIndividuelComponent.prototype.calculatePrice = function () {
        var _this = this;
        console.log(this.productForm.form.valid);
        if (!this.productForm.form.valid) {
            this.santePrice = 0;
            if (this.model.Base.Level == null)
                this.appService.showMessage("Erreur", "Veuillez choisir un niveau de garantie");
            //this.appService.showMessage("Erreur", "Veuillez remplir les champs obligatoire pour avoir un tarif");
        }
        else {
            this.appService.sendModel('/CalculateSante/IndividualHealth/', this.model).subscribe(function (callback) {
                console.log(callback);
                console.log(typeof callback);
                if (typeof callback === "string") {
                    _this.santePrice = 0;
                    if (callback.match("Erreur"))
                        _this.appService.showMessage("Erreur", callback);
                    else
                        _this.appService.showMessage("Valid", callback);
                }
                else
                    _this.santePrice = callback;
                console.log(callback);
            });
        }
    };
    SanteIndividuelComponent.prototype.getLevelProduct = function (productId) {
        var _this = this;
        this.model.Base.Level == null;
        this.appService.sendDatas('/Products/GetLevels/', productId).subscribe(function (callback) {
            _this.levels = [];
            for (var _i = 0, callback_1 = callback; _i < callback_1.length; _i++) {
                var levels = callback_1[_i];
                _this.levels.push({ label: levels.Name, value: levels.Name });
            }
            if (_this.model.Base.Level != null)
                _this.calculatePrice();
        });
    };
    return SanteIndividuelComponent;
}());
__decorate([
    core_1.ViewChild('productForm'),
    __metadata("design:type", forms_1.NgForm)
], SanteIndividuelComponent.prototype, "productForm", void 0);
SanteIndividuelComponent = __decorate([
    core_1.Component({
        selector: 'account',
        templateUrl: '/app/products/sante-individuel/santeIndividuel.component.html',
        providers: [app_service_1.AppService],
        animations: [router_animations_1.slideToLeft()],
        host: { '[@slideToLeft]': '' }
    }),
    __metadata("design:paramtypes", [router_1.Router,
        http_1.Http,
        app_service_1.AppService,
        router_1.ActivatedRoute,
        platform_browser_1.Title,
        auth_guard_service_1.AuthGuardService,
        masks_service_1.MasksService])
], SanteIndividuelComponent);
exports.SanteIndividuelComponent = SanteIndividuelComponent;
//# sourceMappingURL=santeIndividuel.component.js.map