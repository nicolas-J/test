"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var MasksService = (function () {
    function MasksService() {
    }
    //Fonctions
    MasksService.prototype.getPhoneMask = function () {
        var mask = [/\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/];
        return (mask);
    };
    MasksService.prototype.getIbanMask = function () {
        var mask = [/[a-zA-Z]/, /[a-zA-Z]/, /\d/, /\d/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/,];
        return (mask);
    };
    MasksService.prototype.getBicMask = function () {
        var mask = [/[a-zA-Z]/, /[a-zA-Z]/, /[a-zA-Z]/, /[a-zA-Z]/, ' ', /[a-zA-Z]/, /[a-zA-Z]/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, ' ', /[a-zA-Z\d]/, /[a-zA-Z\d]/, /[a-zA-Z\d]/];
        return (mask);
    };
    MasksService.prototype.getPeriodeMask = function () {
        var mask = [/\d/, /\d/];
        return (mask);
    };
    MasksService.prototype.getSiretMask = function () {
        var mask = [/\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/, /\d/];
        return (mask);
    };
    MasksService.prototype.getSecuMask = function () {
        var mask = [/\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, ' ', /\d/, /\d/];
        return (mask);
    };
    MasksService.prototype.getDatetMask = function () {
        var mask = [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/];
        return (mask);
    };
    MasksService.prototype.getNafMask = function () {
        var mask = [/\d/, /\d/, /\d/, /\d/, /[a-zA-Z]/];
        return (mask);
    };
    MasksService.prototype.getPostCodeMask = function () {
        var mask = [/\d/, /\d/, /\d/, /\d/, /\d/];
        return (mask);
    };
    MasksService.prototype.getShortDateMask = function () {
        var mask = [/\d/, /\d/, '/', /\d/, /\d/];
        return (mask);
    };
    MasksService.prototype.getEmployeesMask = function () {
        var mask = [/\d/, /\d/, /\d/];
        return (mask);
    };
    MasksService.prototype.getBirthDepartmentMask = function () {
        var mask = [/\d/, /\d/, /\d/];
        return (mask);
    };
    MasksService.prototype.handleError = function (error) {
        console.error("An error occurred", error);
        return Promise.reject(error.message || error);
    };
    return MasksService;
}());
MasksService = __decorate([
    core_1.Injectable()
], MasksService);
exports.MasksService = MasksService;
//# sourceMappingURL=masks.service.js.map