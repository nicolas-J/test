"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var Observable_1 = require("rxjs/Observable");
var BehaviorSubject_1 = require("rxjs/BehaviorSubject");
require("rxjs/add/operator/share");
require("rxjs/add/operator/toPromise");
var app_service_1 = require("./app.service");
var FileService = (function () {
    function FileService(http, appService) {
        var _this = this;
        this.http = http;
        this.appService = appService;
        this.url = "";
        this.fileName = "";
        this.serverResponse = "";
        this.serverError = "";
        this.progressObserver = new BehaviorSubject_1.BehaviorSubject(0);
        this.progress$ = this.progressObserver.asObservable();
        this.progress$ = new Observable_1.Observable(function (observer) {
            _this.progressObserver = observer;
        });
    }
    FileService.prototype.changeProgress = function (number) {
        this.progressObserver.next(number);
    };
    FileService.prototype.getObserver = function () {
        return this.progress$;
    };
    FileService.prototype.resetService = function () {
        this.url = "";
        this.fileName = "";
        this.serverResponse = "";
        this.serverError = "";
    };
    FileService.prototype.b64ToBlob = function (b64Data, contentType, sliceSize) {
        if (contentType === void 0) { contentType = ''; }
        if (sliceSize === void 0) { sliceSize = 512; }
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    };
    FileService.prototype.uploadFile = function (file, fileName, model) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        setTimeout(function () {
                            this.changeProgress(0);
                        }.bind(_this), 2000);
                        resolve(JSON.parse(xhr.response));
                        var response = JSON.parse(xhr.response);
                        if (response[0] === "Error") {
                            _this.serverError = response[1];
                        }
                        else {
                            _this.serverResponse = response[1];
                        }
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.upload.onprogress = function (event) {
                _this.changeProgress(Math.round(event.loaded / event.total * 100));
            };
            xhr.open('POST', _this.url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            var formData = new FormData();
            formData.append("file", file);
            formData.append("fileName", fileName);
            formData.append("model", JSON.stringify(model));
            xhr.send(formData);
        });
    };
    FileService.prototype.uploadExcel = function (file) {
        var _this = this;
        $(".loading").show();
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        setTimeout(function () {
                            this.changeProgress(0);
                        }.bind(_this), 2000);
                        resolve(JSON.parse(xhr.response));
                        var response = JSON.parse(xhr.response);
                        $(".loading").hide();
                        if (response[0] === "Error") {
                            _this.serverError = response[1];
                        }
                        else {
                            _this.serverResponse = response[1];
                        }
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.upload.onprogress = function (event) {
                _this.changeProgress(Math.round(event.loaded / event.total * 100));
            };
            xhr.open('POST', _this.url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            var formData = new FormData();
            formData.append("file", file);
            xhr.send(formData);
        });
    };
    FileService.prototype.readFile = function (fileName, model) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                        var response = JSON.parse(xhr.response);
                        if (response[0] === "Error") {
                            _this.serverError = response[1];
                        }
                        else {
                            var blob = _this.b64ToBlob(response[0], response[1]);
                            var blobUrl = URL.createObjectURL(blob);
                            var link = document.createElement("a");
                            //Download
                            if (window.navigator.msSaveOrOpenBlob) {
                                document.body.appendChild(link);
                                link.addEventListener('click', function fileClickHandler() {
                                    window.navigator.msSaveOrOpenBlob(blob, response[3] + response[2]);
                                }, false);
                            }
                            else {
                                if (response[2] === ".pdf") {
                                    var win = window.open(blobUrl, '_blank');
                                }
                                link.setAttribute("download", response[3] + response[2]);
                                // Construct the uri
                                link.setAttribute("href", blobUrl);
                                document.body.appendChild(link);
                            }
                            link.click();
                            // Cleanup the DOM
                            document.body.removeChild(link);
                        }
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open('POST', _this.url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            var formData = new FormData();
            formData.append("fileName", fileName);
            formData.append("model", JSON.stringify(model));
            xhr.send(formData);
        });
    };
    FileService.prototype.getFile = function (fileName, model) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        var response = JSON.parse(xhr.response);
                        if (response[0] === "Error") {
                            _this.serverError = response[1];
                        }
                        else {
                            var blob = _this.b64ToBlob(response[0], response[1]);
                            var blobUrl = URL.createObjectURL(blob);
                            resolve(blobUrl);
                        }
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open('POST', _this.url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            var formData = new FormData();
            formData.append("fileName", fileName);
            formData.append("model", JSON.stringify(model));
            xhr.send(formData);
        });
    };
    FileService.prototype.getFileSample = function (url) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        var response = JSON.parse(xhr.response);
                        if (response.Error) {
                            _this.appService.setEmittedValue({ severity: "error", summary: "Erreur", detail: response.Error });
                        }
                        else {
                            var blob = _this.b64ToBlob(response[0], response[1]);
                            var blobUrl = URL.createObjectURL(blob);
                            var link = document.createElement("a");
                            //Download
                            if (window.navigator.msSaveOrOpenBlob) {
                                document.body.appendChild(link);
                                link.addEventListener('click', function fileClickHandler() {
                                    window.navigator.msSaveOrOpenBlob(blob, response[3] + response[2]);
                                }, false);
                            }
                            else {
                                if (response[2] === ".pdf") {
                                    var win = window.open(blobUrl, '_blank');
                                }
                                link.setAttribute("download", response[3] + response[2]);
                                // Construct the uri
                                link.setAttribute("href", blobUrl);
                                document.body.appendChild(link);
                            }
                            link.click();
                            // Cleanup the DOM
                            document.body.removeChild(link);
                        }
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open('POST', url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            var formData = new FormData();
            xhr.send(formData);
        });
    };
    FileService.prototype.deleteFile = function (fileName, model) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var xhr = new XMLHttpRequest();
            xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                    if (xhr.status === 200) {
                        resolve(JSON.parse(xhr.response));
                        var response = JSON.parse(xhr.response);
                        if (response[0] === "Error") {
                            _this.serverError = response[1];
                        }
                        else {
                            _this.serverResponse = response[1];
                            _this.model = response[2];
                        }
                    }
                    else {
                        reject(xhr.response);
                    }
                }
            };
            xhr.open('POST', _this.url, true);
            xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
            var formData = new FormData();
            formData.append("fileName", fileName);
            formData.append("model", JSON.stringify(model));
            xhr.send(formData);
        });
    };
    FileService.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    return FileService;
}());
FileService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, app_service_1.AppService])
], FileService);
exports.FileService = FileService;
//# sourceMappingURL=file.service.js.map