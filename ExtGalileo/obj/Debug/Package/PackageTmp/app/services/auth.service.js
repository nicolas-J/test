"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/map");
var AuthService = (function () {
    function AuthService(http) {
        this.http = http;
        // set token if saved in local storage
        var currentUser = localStorage.getItem('currentUser');
        this.token = currentUser;
    }
    AuthService.prototype.login = function (model) {
        var _this = this;
        return this.http.post('/Account/Login', model)
            .map(function (response) {
            // login successful if there's a jwt token in the response
            var res = JSON.parse(response["_body"]);
            if (res[0] == "success") {
                // set token property
                _this.token = res[1];
                //set success message
                var msg = { severity: "success", summary: "Succès", detail: "Bienvenue, sur Galiléo!" };
                // store username and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', model.Email);
                // return true to indicate successful login
                return msg;
            }
            else {
                _this.token = null;
                localStorage.removeItem('currentUser');
                return res;
            }
        });
    };
    AuthService.prototype.getToken = function () {
        return this.token;
    };
    AuthService.prototype.logout = function () {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    };
    AuthService.prototype.getUserRoles = function () {
        var _this = this;
        var headers = new http_1.Headers();
        headers.append('Content-Type', 'application/json');
        return this.http
            .get("/Api/authapi/getuserroles", { headers: headers })
            .toPromise()
            .then(function (response) { return JSON.parse(response['_body']); })
            .catch(function (resp) { return _this.handleError(resp); });
    };
    AuthService.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    return AuthService;
}());
AuthService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], AuthService);
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map