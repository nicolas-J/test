"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IPersonData = (function () {
    function IPersonData() {
    }
    return IPersonData;
}());
exports.IPersonData = IPersonData;
var FamilyStatus;
(function (FamilyStatus) {
    FamilyStatus[FamilyStatus["Single"] = 0] = "Single";
    FamilyStatus[FamilyStatus["Married"] = 1] = "Married";
    FamilyStatus[FamilyStatus["Widowed"] = 2] = "Widowed";
    FamilyStatus[FamilyStatus["Divorced"] = 3] = "Divorced";
    FamilyStatus[FamilyStatus["Separated"] = 4] = "Separated";
    FamilyStatus[FamilyStatus["Cohabitation"] = 5] = "Cohabitation";
    FamilyStatus[FamilyStatus["CivilUnion"] = 6] = "CivilUnion";
})(FamilyStatus = exports.FamilyStatus || (exports.FamilyStatus = {}));
var Title;
(function (Title) {
    Title[Title["Mr"] = 0] = "Mr";
    Title[Title["Mrs"] = 1] = "Mrs";
})(Title = exports.Title || (exports.Title = {}));
//# sourceMappingURL=IPersonData.js.map