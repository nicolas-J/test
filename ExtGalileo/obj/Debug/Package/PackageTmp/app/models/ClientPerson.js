"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PostalAddress_1 = require("./PostalAddress");
var Managers_1 = require("./Managers");
var BankAccountData_1 = require("./BankAccountData");
var PersonStatus_1 = require("./PersonStatus");
var MailPhoneFax_1 = require("./MailPhoneFax");
var ClientPerson = (function () {
    function ClientPerson() {
        this.TitleString = "MR";
        this.FirstName = "";
        this.MaidenName = "";
        this.PostalAddress = new PostalAddress_1.PostalAddress();
        this.Managers = new Managers_1.Managers();
        this.PersonStatus = new PersonStatus_1.PersonStatus();
        this.Spouse = null;
        this.Children = new Array();
        this.BankAccountData = new BankAccountData_1.BankAccountData();
        //this.ContractsId = new Array<string>();
        this.ClientPersonEvent = new Array();
        this.MailPhoneFax = new MailPhoneFax_1.MailPhoneFax();
    }
    return ClientPerson;
}());
exports.ClientPerson = ClientPerson;
//# sourceMappingURL=ClientPerson.js.map