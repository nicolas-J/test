"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var MailPhoneFax = (function () {
    function MailPhoneFax() {
    }
    MailPhoneFax.prototype.ToString = function () {
        return (this.EmailAddress != null ? this.EmailAddress : "") + "  " + (this.MobilePhoneNumber != null ? this.MobilePhoneNumber : "") + "  " + (this.FixPhoneNumber != null ? this.FixPhoneNumber : "") + "  " + (this.Fax != null ? this.Fax : "");
    };
    return MailPhoneFax;
}());
exports.MailPhoneFax = MailPhoneFax;
//# sourceMappingURL=MailPhoneFax.js.map