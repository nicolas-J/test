"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var ClientPerson_1 = require("./ClientPerson");
var ClientCompany_1 = require("./ClientCompany");
var Product_1 = require("./Product");
var Product_2 = require("./Product");
var repository_collection_1 = require("./repository-collection");
var ExtranetSubscription = (function (_super) {
    __extends(ExtranetSubscription, _super);
    function ExtranetSubscription() {
        var _this = _super.call(this) || this;
        _this.Client = new ClientCompany_1.ClientCompany();
        _this.ClientPerson = new ClientPerson_1.ClientPerson();
        _this.Product = new Product_2.Product();
        _this.Base = new Product_1.ProductLevel();
        return _this;
    }
    return ExtranetSubscription;
}(repository_collection_1.RepositoryCollection));
exports.ExtranetSubscription = ExtranetSubscription;
var ProductOptions = (function () {
    function ProductOptions() {
    }
    return ProductOptions;
}());
exports.ProductOptions = ProductOptions;
var PriceList = (function () {
    function PriceList() {
    }
    return PriceList;
}());
exports.PriceList = PriceList;
//# sourceMappingURL=ExtranetSubscription.js.map