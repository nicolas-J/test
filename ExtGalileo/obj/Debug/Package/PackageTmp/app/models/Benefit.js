"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Benefit = (function () {
    function Benefit() {
        this._children = new Array();
    }
    Benefit.prototype.Benefit = function () {
        this._parent = null;
    };
    Benefit.prototype.Benefit1 = function (text, value) {
        if (value === void 0) { value = ""; }
        this._parent = null;
        this.Text = text;
        this.Value = value;
    };
    Benefit.prototype.Benefit2 = function (parent, text, value) {
        if (value === void 0) { value = ""; }
        this._parent = parent;
        this.Text = text;
        this.Value = value;
    };
    return Benefit;
}());
exports.Benefit = Benefit;
//# sourceMappingURL=Benefit.js.map