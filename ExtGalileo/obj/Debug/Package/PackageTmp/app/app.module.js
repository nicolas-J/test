"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
//
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var animations_1 = require("@angular/platform-browser/animations");
var forms_1 = require("@angular/forms");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var app_routing_1 = require("./app.routing");
//Services
var auth_service_1 = require("./services/auth.service");
var masks_service_1 = require("./services/masks.service");
var auth_guard_service_1 = require("./services/auth-guard.service");
var app_service_1 = require("./services/app.service");
var sante_surco_service_1 = require("./services/productService/sante-surco.service");
var product_rc_service_1 = require("./services/productService/product-rc.service");
var products_service_1 = require("./services/productService/products.service");
//Main
var app_component_1 = require("./app.component");
var nav_component_1 = require("./items/nav.component");
var header_component_1 = require("./items/header.component");
var footer_component_1 = require("./items/footer.component");
var message_component_1 = require("./items/message.component");
//Home 
var index_component_1 = require("./home/index.component");
//Acccount
var account_component_1 = require("./account/account.component");
var login_component_1 = require("./account/login.component");
var register_component_1 = require("./account/register.component");
var forgotPassword_component_1 = require("./account/forgotPassword.component");
var resetPassword_component_1 = require("./account/resetPassword.component");
//Dashboard
var dashboard_component_1 = require("./dashboard/dashboard.component");
var tarification_component_1 = require("./dashboard/tarification.component");
var projects_component_1 = require("./dashboard/projects.component");
var documents_component_1 = require("./dashboard/documents.component");
var portefeuille_component_1 = require("./dashboard/portefeuille.component");
//Input
//import { CityPostCodeComponent } from './input/cityPostCode.component';
//Popups
var loginSurvey_component_1 = require("./popup/loginSurvey.component");
//Attribute Directives
var minValidator_directive_1 = require("./shared/minValidator.directive");
var postCodeValidator_directive_1 = require("./shared/postCodeValidator.directive");
var yesNoValidator_directive_1 = require("./shared/yesNoValidator.directive");
var maxRevenueValidator_directive_1 = require("./shared/maxRevenueValidator.directive");
//IMPORT EXTERNAL LIB
var primeng_1 = require("primeng/primeng");
var angular2_text_mask_1 = require("angular2-text-mask");
var ng2_bootstrap_1 = require("ng2-bootstrap");
var ng2_modal_1 = require("ng2-modal");
var ng2_select_1 = require("ng2-select");
var angular_datatables_1 = require("angular-datatables");
//Manage
var collaboratorDetail_component_1 = require("./manage/collaboratorDetail.component");
var onlinePayment_component_1 = require("./onlinePayment/onlinePayment.component");
var paymentCallback_component_1 = require("./onlinePayment/paymentCallback.component");
var payment_list_component_1 = require("./onlinePayment/payment-list.component");
//Products
var testModule_component_1 = require("./test/testModule.component");
var product_rc_component_1 = require("./products/rc-autoentrepreneur/product-rc.component");
var rc_pro_component_1 = require("./products/rc-pro/rc-pro.component");
var rc_declaration_component_1 = require("./products/rc-autoentrepreneur/rc-declaration.component");
var client_company_helper_component_1 = require("./inputs/client-company-helper.component");
var client_person_helper_component_1 = require("./inputs/client-person-helper.component");
var documents_helper_component_1 = require("./inputs/documents-helper.component");
var effect_date_helper_component_1 = require("./inputs/effect-date-helper.component");
var spouse_info_helper_component_1 = require("./inputs/spouse-info-helper.component");
var bridge_helper_component_1 = require("./inputs/bridge-helper.component");
var children_helper_component_1 = require("./inputs/children-helper.component");
var child_info_helper_component_1 = require("./inputs/child-info-helper.component");
var iban_component_1 = require("./inputs/iban.component");
var bic_component_1 = require("./inputs/bic.component");
var payment_component_1 = require("./inputs/payment.component");
var post_code_and_city_component_1 = require("./inputs/post-code-and-city.component");
var creation_date_helper_component_1 = require("./inputs/creation-date-helper.component");
var santeIndividuel_component_1 = require("./products/sante-individuel/santeIndividuel.component");
var file_service_1 = require("./services/file.service");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            angular_datatables_1.DataTablesModule,
            animations_1.BrowserAnimationsModule,
            app_routing_1.routing,
            forms_1.FormsModule,
            primeng_1.DataTableModule,
            primeng_1.SharedModule,
            primeng_1.DialogModule,
            primeng_1.SelectButtonModule,
            primeng_1.MultiSelectModule,
            primeng_1.ConfirmDialogModule,
            primeng_1.MessagesModule,
            primeng_1.FileUploadModule,
            primeng_1.ChartModule,
            angular2_text_mask_1.TextMaskModule,
            router_1.RouterModule,
            ng2_bootstrap_1.CarouselModule,
            primeng_1.GalleriaModule,
            ng2_select_1.SelectModule,
            ng2_modal_1.ModalModule,
            primeng_1.PasswordModule,
            primeng_1.ListboxModule,
            primeng_1.DropdownModule,
            primeng_1.RadioButtonModule,
            primeng_1.GrowlModule,
            primeng_1.CheckboxModule,
            primeng_1.SliderModule,
            primeng_1.InputSwitchModule,
            primeng_1.TooltipModule
        ],
        declarations: [
            app_component_1.AppComponent,
            header_component_1.HeaderComponent,
            nav_component_1.NavComponent,
            footer_component_1.FooterComponent,
            message_component_1.MessageComponent,
            account_component_1.AccountComponent,
            index_component_1.IndexComponent,
            register_component_1.RegisterComponent,
            login_component_1.LoginComponent,
            forgotPassword_component_1.ForgotPasswordComponent,
            resetPassword_component_1.ResetPasswordComponent,
            dashboard_component_1.DashboardComponent,
            projects_component_1.ProjectsComponent,
            loginSurvey_component_1.LoginSurveyComponent,
            documents_component_1.DocumentsComponent,
            portefeuille_component_1.PortefeuilleComponent,
            tarification_component_1.TarificationComponent,
            collaboratorDetail_component_1.CollaboratorDetailComponent,
            santeIndividuel_component_1.SanteIndividuelComponent,
            product_rc_component_1.ProductRcComponent,
            rc_pro_component_1.RcProComponent,
            documents_helper_component_1.DocumentsHelper,
            effect_date_helper_component_1.EffectDateHelper,
            spouse_info_helper_component_1.SpouseInfoHelper,
            bridge_helper_component_1.BridgeHelper,
            children_helper_component_1.ChildrenHeplerComponent,
            child_info_helper_component_1.ChildInfoComponent,
            client_company_helper_component_1.ClientCompanyHelper,
            client_person_helper_component_1.ClientPersonHelper,
            rc_declaration_component_1.RcDeclaration,
            iban_component_1.IbanComponent,
            bic_component_1.BicComponent,
            post_code_and_city_component_1.PostCodeAndCityComponent,
            onlinePayment_component_1.OnlinePaymentComponent,
            paymentCallback_component_1.PaymentCallbackComponent,
            payment_component_1.PaymentComponent,
            payment_list_component_1.PaymentListComponent,
            creation_date_helper_component_1.CreationDateHelper,
            testModule_component_1.TestModuleComponent,
            minValidator_directive_1.MinValidatorDirective,
            postCodeValidator_directive_1.PostCodeValidatorDirective,
            yesNoValidator_directive_1.YesNoValidatorDirective,
            maxRevenueValidator_directive_1.MaxRevenueValidatorDirective
        ],
        exports: [
            platform_browser_1.BrowserModule,
            http_1.HttpModule,
        ],
        providers: [
            platform_browser_1.Title,
            auth_service_1.AuthService,
            auth_guard_service_1.AuthGuardService,
            app_service_1.AppService,
            file_service_1.FileService,
            masks_service_1.MasksService,
            product_rc_service_1.ProductRcService,
            products_service_1.ProductsService,
            sante_surco_service_1.SanteSurcoService,
            primeng_1.ConfirmationService,
            {
                provide: core_1.LOCALE_ID,
                useValue: navigator.language
            }
        ],
        bootstrap: [app_component_1.AppComponent],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map