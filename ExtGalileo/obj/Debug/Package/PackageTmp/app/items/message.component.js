"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var router_1 = require("@angular/router");
var auth_service_1 = require("../services/auth.service");
var app_service_1 = require("../services/app.service");
var auth_guard_service_1 = require("../services/auth-guard.service");
var MessageComponent = (function () {
    function MessageComponent(titleService, router, appService, authService, authGuardService) {
        this.titleService = titleService;
        this.router = router;
        this.appService = appService;
        this.authService = authService;
        this.authGuardService = authGuardService;
        this.msgs = [];
    }
    MessageComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.appService.getEmittedValue().subscribe(function (msg) {
            _this.msgs.push(msg);
        });
    };
    return MessageComponent;
}());
MessageComponent = __decorate([
    core_1.Component({
        selector: 'message',
        templateUrl: '/app/items/message.component.html'
    }),
    __metadata("design:paramtypes", [platform_browser_1.Title, router_1.Router, app_service_1.AppService, auth_service_1.AuthService, auth_guard_service_1.AuthGuardService])
], MessageComponent);
exports.MessageComponent = MessageComponent;
//# sourceMappingURL=message.component.js.map