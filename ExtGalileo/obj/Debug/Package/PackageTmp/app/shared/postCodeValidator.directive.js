"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var forms_1 = require("@angular/forms");
var PostCodeValidatorDirective = PostCodeValidatorDirective_1 = (function () {
    function PostCodeValidatorDirective() {
    }
    PostCodeValidatorDirective.prototype.validate = function (control) {
        return postCodeValidator()(control);
    };
    return PostCodeValidatorDirective;
}());
PostCodeValidatorDirective = PostCodeValidatorDirective_1 = __decorate([
    core_1.Directive({
        selector: '[postCode]',
        providers: [{ provide: forms_1.NG_VALIDATORS, useExisting: PostCodeValidatorDirective_1, multi: true }]
    })
], PostCodeValidatorDirective);
exports.PostCodeValidatorDirective = PostCodeValidatorDirective;
function postCodeValidator() {
    return function (control) {
        var rgx = new RegExp("^[0-9]{5}$");
        var forbidden = rgx.test(control.value);
        return forbidden ? null : { 'postCode': true };
    };
}
exports.postCodeValidator = postCodeValidator;
var PostCodeValidatorDirective_1;
//# sourceMappingURL=postCodeValidator.directive.js.map