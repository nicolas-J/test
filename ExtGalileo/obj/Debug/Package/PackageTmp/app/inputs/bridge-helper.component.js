"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
//Models
var ExtranetSubscription_1 = require("../models/ExtranetSubscription");
var BridgeHelper = (function () {
    function BridgeHelper(http) {
        this.http = http;
        this.msgs = [];
        console.log(core_1.isDevMode());
    }
    BridgeHelper.prototype.ngOnInit = function () {
        this.model = this.documentsInfo;
    };
    BridgeHelper.prototype.bridgeProject = function () {
        console.log(this.model.Id);
        $(".loader").show();
        var sign = "Ordre66";
        window.location.href = "Sign/SignCallBack/" + this.model.Id + "?sign=" + sign;
    };
    BridgeHelper.prototype.showBridge = function (response) {
        var message = JSON.stringify(response._body).replace(/"/g, "").replace(/\\/g, "");
        if (message.match("Erreur")) {
            this.showMessage("error", "Attention!", message.replace("u0027", "'"));
        }
        else {
            window.location.href = (message);
        }
        this.clearMessages();
        $(".loader").hide();
    };
    BridgeHelper.prototype.showMessage = function (severity, summary, message) {
        this.msgs.push({ severity: severity, summary: summary, detail: message });
    };
    BridgeHelper.prototype.clearMessages = function () {
        this.msgs = [];
    };
    return BridgeHelper;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", ExtranetSubscription_1.ExtranetSubscription)
], BridgeHelper.prototype, "documentsInfo", void 0);
BridgeHelper = __decorate([
    core_1.Component({
        selector: "bridge-helper",
        templateUrl: "/app/inputs/bridge-helper.component.html"
    }),
    __metadata("design:paramtypes", [http_1.Http])
], BridgeHelper);
exports.BridgeHelper = BridgeHelper;
//# sourceMappingURL=bridge-helper.component.js.map