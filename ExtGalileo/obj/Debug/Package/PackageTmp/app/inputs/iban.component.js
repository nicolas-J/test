"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
//Services
var masks_service_1 = require("../services/masks.service");
var IbanComponent = (function () {
    function IbanComponent(masksService) {
        this.masksService = masksService;
        this.sharedIban = new core_1.EventEmitter();
        this.isGreen = false;
        this.isRed = false;
    }
    IbanComponent.prototype.ngOnInit = function () {
        this.iban = this.ibanInput;
        this.ibanMask = this.masksService.getIbanMask();
    };
    IbanComponent.prototype.ibanChanged = function (event) {
        event.target.value = event.target.value.toUpperCase();
        this.sharedIban.emit(event.target.value);
    };
    IbanComponent.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    return IbanComponent;
}());
IbanComponent = __decorate([
    core_1.Component({
        selector: 'iban',
        templateUrl: '/app/inputs/iban.component.html',
        inputs: ['ibanInput'],
        outputs: ['sharedIban']
    }),
    __metadata("design:paramtypes", [masks_service_1.MasksService])
], IbanComponent);
exports.IbanComponent = IbanComponent;
//# sourceMappingURL=iban.component.js.map