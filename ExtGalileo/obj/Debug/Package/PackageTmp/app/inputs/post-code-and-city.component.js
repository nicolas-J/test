"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var ng2_select_1 = require("ng2-select/ng2-select");
var select_item_1 = require("ng2-select/select/select-item");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var app_service_1 = require("../services/app.service");
var PostCodeAndCityComponent = (function () {
    function PostCodeAndCityComponent(http, appService) {
        this.http = http;
        this.appService = appService;
        this.sharedSelectedPostCode = new core_1.EventEmitter();
        this.sharedSelectedCity = new core_1.EventEmitter();
        this.postCodeItems = new Array();
        this.cityItems = new Array();
    }
    PostCodeAndCityComponent.prototype.ngOnInit = function () {
        this.postCodeItems[0] = this.postCode != null ? this.postCode : "";
        this.initPostCode = [this.postCode != null ? this.postCode : ""];
        this.cityItems[0] = this.city != null ? this.city : "";
        this.initCity = [this.city != null ? this.city : ""];
    };
    PostCodeAndCityComponent.prototype.ngAfterViewInit = function () {
        this.postCodeSelect = this.selects.first;
        this.citySelect = this.selects.last;
    };
    //Post code
    PostCodeAndCityComponent.prototype.selectedPostCode = function (value) {
        if (this.postCode != value.text) {
            this.postCode = value.text;
            this.sharedSelectedPostCode.emit(value.text);
            this.searchCityByCode(value.text);
        }
    };
    PostCodeAndCityComponent.prototype.removedPostCode = function (value) {
    };
    PostCodeAndCityComponent.prototype.typedPostCode = function (value) {
        if (value.length === 5) {
            this.searchCityByCode(value);
        }
    };
    PostCodeAndCityComponent.prototype.onChangePostCode = function (event) {
        if (this.postCodeItems.indexOf(event.target.value) > -1) {
            this.postCodeSelect.active = new Array(this.postCodeSelect.itemObjects[this.postCodeItems.indexOf(event.target.value)]);
            this.postCodeSelect.activeOption = this.postCodeSelect.itemObjects[this.postCodeItems.indexOf(event.target.value)];
            this.postCodeSelect.selected.emit(this.postCodeSelect.itemObjects[this.postCodeItems.indexOf(event.target.value)]);
        }
        else {
            this.postCodeItems.push(event.target.value);
            this.postCodeSelect.itemObjects.push(new select_item_1.SelectItem({ id: event.target.value, text: event.target.value }));
            this.postCodeSelect.active = new Array(this.postCodeSelect.itemObjects[this.postCodeSelect.itemObjects.length - 1]);
            this.postCodeSelect.activeOption = this.postCodeSelect.itemObjects[this.postCodeSelect.itemObjects.length - 1];
            this.postCodeSelect.selected.emit(this.postCodeSelect.itemObjects[this.postCodeSelect.itemObjects.length - 1]);
        }
    };
    //City
    PostCodeAndCityComponent.prototype.selectedCity = function (value) {
        if (this.city != value.text) {
            this.city = value.text;
            this.sharedSelectedCity.emit(value.text);
            this.searchPostCodeByCity(value.text);
        }
    };
    PostCodeAndCityComponent.prototype.removedCity = function (value) {
    };
    PostCodeAndCityComponent.prototype.typedCity = function (value) {
        if (value.length > 3) {
            this.searchPostCodeByCity(value);
        }
    };
    PostCodeAndCityComponent.prototype.onChangeCity = function (event) {
        if (this.cityItems.indexOf(event.target.value.toUpperCase()) > -1) {
            this.citySelect.active = new Array(this.citySelect.itemObjects[this.cityItems.indexOf(event.target.value.toUpperCase())]);
            this.citySelect.activeOption = this.citySelect.itemObjects[this.cityItems.indexOf(event.target.value.toUpperCase())];
            this.citySelect.selected.emit(this.citySelect.itemObjects[this.cityItems.indexOf(event.target.value.toUpperCase())]);
        }
        else {
            this.cityItems.push(event.target.value.toUpperCase());
            this.citySelect.itemObjects.push(new select_item_1.SelectItem({ id: event.target.value.toUpperCase(), text: event.target.value.toUpperCase() }));
            this.citySelect.active = new Array(this.citySelect.itemObjects[this.citySelect.itemObjects.length - 1]);
            this.citySelect.activeOption = this.citySelect.itemObjects[this.citySelect.itemObjects.length - 1];
            this.citySelect.selected.emit(this.citySelect.itemObjects[this.citySelect.itemObjects.length - 1]);
        }
    };
    //PostCode
    PostCodeAndCityComponent.prototype.searchPostCodeByCity = function (city) {
        var _this = this;
        if (city != "") {
            this.appService.sendDatas('/Tools/GetCity/', city).subscribe(function (callback) {
                _this.setCityValue(callback);
            });
        }
    };
    PostCodeAndCityComponent.prototype.setPostCode = function (response) {
        var postCodes = JSON.parse(response._body);
        this.postCodeItems = new Array();
        this.postCodeSelect.itemObjects = new Array();
        for (var i = 0; i < postCodes.length; i++) {
            this.postCodeItems.push(postCodes[i]);
            this.postCodeSelect.itemObjects.push(new select_item_1.SelectItem({ id: postCodes[i], text: postCodes[i] }));
        }
        if (postCodes.length == 1) {
            this.postCodeSelect.active = new Array(this.postCodeSelect.itemObjects[0]);
            this.postCodeSelect.activeOption = this.postCodeSelect.itemObjects[0];
            this.postCodeSelect.selected.emit(this.postCodeSelect.itemObjects[0]);
        }
        else if (postCodes.length > 1) {
            $(".postCodeCssClass .ui-select-toggle").click();
        }
    };
    //City
    PostCodeAndCityComponent.prototype.searchCityByCode = function (postcode) {
        var _this = this;
        if (postcode != "") {
            this.appService.sendDatas('/Tools/GetCity/', postcode).subscribe(function (callback) {
                _this.setCityValue(callback);
            });
        }
    };
    PostCodeAndCityComponent.prototype.setCityValue = function (response) {
        var citys = response;
        this.cityItems = new Array();
        this.citySelect.itemObjects = new Array();
        for (var i = 0; i < citys.length; i++) {
            this.cityItems.push(citys[i]);
            this.citySelect.itemObjects.push(new select_item_1.SelectItem({ id: citys[i], text: citys[i] }));
        }
        if (citys.length == 1) {
            this.citySelect.active = new Array(this.citySelect.itemObjects[0]);
            this.citySelect.activeOption = this.citySelect.itemObjects[0];
            this.citySelect.selected.emit(this.citySelect.itemObjects[0]);
        }
        else if (citys.length > 1) {
            $(".cityCssClass .ui-select-toggle").click();
        }
    };
    return PostCodeAndCityComponent;
}());
__decorate([
    core_1.ViewChildren(ng2_select_1.SelectComponent),
    __metadata("design:type", core_1.QueryList)
], PostCodeAndCityComponent.prototype, "selects", void 0);
__decorate([
    core_1.ViewChild(ng2_select_1.SelectComponent),
    __metadata("design:type", ng2_select_1.SelectComponent)
], PostCodeAndCityComponent.prototype, "postCodeSelect", void 0);
__decorate([
    core_1.ViewChild(ng2_select_1.SelectComponent),
    __metadata("design:type", ng2_select_1.SelectComponent)
], PostCodeAndCityComponent.prototype, "citySelect", void 0);
PostCodeAndCityComponent = __decorate([
    core_1.Component({
        selector: 'post-code-and-city',
        templateUrl: '/app/inputs/post-code-and-city.component.html',
        inputs: ['postCode', 'city'],
        outputs: ['sharedSelectedPostCode', 'sharedSelectedCity'],
        providers: [app_service_1.AppService]
    })
    //<post-code-and-city [postCode]="parentPostCodeVar" [city]="parentCityVar" (sharedSelectedPostCode)='parentPostCodeVar=$event' (sharedSelectedCity)='parentCityVar=$event'></post-code-and-city>
    ,
    __metadata("design:paramtypes", [http_1.Http, app_service_1.AppService])
], PostCodeAndCityComponent);
exports.PostCodeAndCityComponent = PostCodeAndCityComponent;
//# sourceMappingURL=post-code-and-city.component.js.map