"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
//Service
var masks_service_1 = require("../services/masks.service");
var ChildInfoComponent = (function () {
    function ChildInfoComponent(masksService) {
        this.masksService = masksService;
        this.modelChange = new core_1.EventEmitter();
        this.check = [];
    }
    ChildInfoComponent.prototype.ngOnInit = function () {
        var _this = this;
        for (var i = 0; i < 4; i++) {
            this.check.push(true);
        }
        this.model.forEach(function (child) {
            if (child.TitleString == "MR")
                _this.check[_this.model.indexOf(child)] = true;
            else {
                _this.check[_this.model.indexOf(child)] = false;
            }
        });
        this.data = this.model;
        this.secuMask = this.masksService.getSecuMask();
    };
    ChildInfoComponent.prototype.onDataChange = function () {
        this.modelChange.emit(this.model);
    };
    ChildInfoComponent.prototype.completeSecurityNumber = function (event, indexString) {
        var index = indexString.toString();
        this.model[index].SocialSecurityNumber = event.target.value;
        this.onDataChange();
    };
    ChildInfoComponent.prototype.checkTitle = function (event, indexString) {
        var index = indexString.toString();
        if (event.checked == true)
            this.model[index].TitleString = "MR";
        else {
            this.model[index].TitleString = "MME";
        }
        this.onDataChange();
    };
    return ChildInfoComponent;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ChildInfoComponent.prototype, "modelChange", void 0);
ChildInfoComponent = __decorate([
    core_1.Component({
        selector: "child-info-helper",
        templateUrl: "/app/inputs/child-info-helper.component.html",
        inputs: ['model', 'inputList']
    })
    //arguments possible de inputList:
    //Civility -> civilité + situation familiale
    //TItle -> titre
    //Name -> nom + prénom
    //Secu -> numéro de sécu
    //BirthDate -> date de naissance
    ,
    __metadata("design:paramtypes", [masks_service_1.MasksService])
], ChildInfoComponent);
exports.ChildInfoComponent = ChildInfoComponent;
//# sourceMappingURL=child-info-helper.component.js.map