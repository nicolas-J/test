"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
//Service
var masks_service_1 = require("../services/masks.service");
var ClientCompanyHelper = (function () {
    function ClientCompanyHelper(maskService, http) {
        this.http = http;
        this.modelChange = new core_1.EventEmitter();
        this.msgs = [];
        this.siretMask = maskService.getSiretMask();
        this.nafMask = maskService.getNafMask();
        this.phoneMask = maskService.getPhoneMask();
        this.employeesMask = maskService.getEmployeesMask();
    }
    ClientCompanyHelper.prototype.ngOnInit = function () {
        this.LegalFormList = [];
        this.LegalFormList.push({ label: "---", value: "" });
        this.LegalFormList.push({ label: "SAS", value: "SAS" });
        this.LegalFormList.push({ label: "SARL", value: "SARL" });
        this.LegalFormList.push({ label: "EURL", value: "EURL" });
        this.LegalFormList.push({ label: "SASU", value: "SASU" });
        this.LegalFormList.push({ label: "EI", value: "EI" });
        this.LegalFormList.push({ label: "SNC", value: "SNC" });
        this.LegalFormList.push({ label: "Particulier", value: "Particulier" });
    };
    //getNafCode(naf) {
    //    console.log(naf.target.value);
    //    if (naf.target.value != "") {
    //        let headers = new Headers();
    //        headers.append('Content-Type', 'application/json');
    //        this.http.post("/api/angular/GetNafCodeList/" + naf.target.value, "", { headers: headers })
    //            .toPromise()
    //            .then(res => this.allocateNafList(res));
    //    }
    //}
    //allocateNafList(response) {
    //    const nafCodes = JSON.parse(response._body);
    //    console.log(nafCodes);
    //    //if (nafCodes.Error != null) {
    //    //    this.showMessage("error", "Attention!", nafCodes.Error);
    //    //} else if (nafCodes.Data == "Aucun code Naf trouvé") {
    //    //    this.showMessage("error", "Attention!", nafCodes.Error);
    //    //} else {
    //    //    this.model.AxaServiceProModel.AnnualRate = nafCodes.Total;
    //    //    this.model.AxaServiceProModel.SixMonthRate = (nafCodes.Total / 2).toFixed(2);
    //    //    this.model.Base.Price = (nafCodes.Total / 12).toFixed(2);
    //    //    this.model.AxaServiceProModel.EstimatedCommission = nafCodes.CommBroker;
    //    //    this.clearMessages();
    //    //}
    //}
    ClientCompanyHelper.prototype.onDataChange = function () {
        this.modelChange.emit(this.model);
    };
    ClientCompanyHelper.prototype.changeEmployeesNumber = function (event) {
        var temp = event.target.value;
        if (temp > 300) {
            this.showWarn("Le nombre de salariés ne peut pas dépasser 300");
            event.target.value = 0;
            this.model.EmployeesNumber = 0;
        }
        else {
            this.model.EmployeesNumber = event.target.value;
            this.onDataChange();
        }
    };
    ClientCompanyHelper.prototype.changeEmployeesAverageAge = function (event) {
        var temp = event.target.value;
        if (temp > 65 || temp < 20) {
            this.showWarn("L'âge moyen des salariés doit être compris en 20 et 65 ans");
            event.target.value = 0;
            this.model.EmployeesAverageAge = 0;
        }
        else {
            this.model.EmployeesAverageAge = event.target.value;
            this.onDataChange();
        }
    };
    ClientCompanyHelper.prototype.showWarn = function (message) {
        this.msgs = [];
        this.msgs.push({ severity: 'error', summary: 'Attention', detail: message });
    };
    return ClientCompanyHelper;
}());
ClientCompanyHelper = __decorate([
    core_1.Component({
        selector: "client-company-helper",
        templateUrl: "/app/inputs/client-company-helper.component.html",
        inputs: ['model', 'inputList'],
        outputs: ['modelChange']
    })
    //arguments possibles de inputList:
    //LegalForm -> Forme legale
    //Name -> Raison Sociale
    //NafCode -> Code APE
    //ContactDetails -> Contact details
    //Address -> Adresse
    //CityPostCode -> Ville + code postal
    //Siret -> Siret number
    ,
    __metadata("design:paramtypes", [masks_service_1.MasksService, http_1.Http])
], ClientCompanyHelper);
exports.ClientCompanyHelper = ClientCompanyHelper;
//# sourceMappingURL=client-company-helper.component.js.map