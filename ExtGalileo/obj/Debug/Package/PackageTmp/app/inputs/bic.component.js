"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
require("rxjs/add/operator/toPromise");
//Services
var masks_service_1 = require("../services/masks.service");
var BicComponent = (function () {
    function BicComponent(masksService) {
        this.masksService = masksService;
        this.sharedBic = new core_1.EventEmitter();
        this.isGreen = false;
        this.isRed = false;
    }
    BicComponent.prototype.ngOnInit = function () {
        this.bicMask = this.masksService.getBicMask();
        this.bic = this.bicInput;
    };
    BicComponent.prototype.bicChanged = function (event) {
        event.target.value = event.target.value.toUpperCase();
        this.sharedBic.emit(event.target.value);
    };
    BicComponent.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    return BicComponent;
}());
BicComponent = __decorate([
    core_1.Component({
        selector: 'bic',
        templateUrl: '/app/inputs/bic.component.html',
        inputs: ['bicInput'],
        outputs: ['sharedBic']
    }),
    __metadata("design:paramtypes", [masks_service_1.MasksService])
], BicComponent);
exports.BicComponent = BicComponent;
//# sourceMappingURL=bic.component.js.map