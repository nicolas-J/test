"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var app_service_1 = require("../services/app.service");
var router_animations_1 = require("../tools/router.animations");
var PortefeuilleComponent = (function () {
    function PortefeuilleComponent(router, http, route, appService) {
        this.router = router;
        this.http = http;
        this.route = route;
        this.appService = appService;
        this.banner = "Mon Portefeuille";
        this.loadPage = false;
    }
    PortefeuilleComponent.prototype.ngOnInit = function () {
        var _this = this;
        if (this.router.url.match("BrokerName")) {
            this.appService.getDatas('/PortFolio/MyContracts?BrokerName').subscribe(function (callback) {
                _this.response = callback;
                _this.loadPage = true;
            });
        }
        else {
            this.appService.getDatas('/PortFolio/MyContracts/').subscribe(function (callback) {
                _this.response = callback[0];
                _this.chartValue = callback[1];
                _this.showGraph(_this.chartValue);
                _this.loadPage = true;
            });
        }
    };
    PortefeuilleComponent.prototype.showGraph = function (chartValue) {
        this.dataChart = {
            labels: ['ADP', 'IARD', 'PREV'],
            datasets: [
                {
                    data: [chartValue[0], chartValue[1], chartValue[2]],
                    backgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ],
                    hoverBackgroundColor: [
                        "#FF6384",
                        "#36A2EB",
                        "#FFCE56"
                    ]
                }
            ]
        };
        this.optionsChart = {
            title: {
                display: true,
                text: 'Mes affaires',
                fontSize: 16
            },
            legend: {
                position: 'bottom'
            },
            cutoutPercentage: 80,
        };
    };
    return PortefeuilleComponent;
}());
PortefeuilleComponent = __decorate([
    core_1.Component({
        selector: 'portefeuille',
        templateUrl: '/app/dashboard/portefeuille.component.html',
        providers: [app_service_1.AppService],
        animations: [router_animations_1.slideToLeft()],
        host: { '[@slideToLeft]': '' }
    }),
    __metadata("design:paramtypes", [router_1.Router, http_1.Http, router_1.ActivatedRoute, app_service_1.AppService])
], PortefeuilleComponent);
exports.PortefeuilleComponent = PortefeuilleComponent;
//# sourceMappingURL=portefeuille.component.js.map